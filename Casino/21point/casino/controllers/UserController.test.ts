// testing target
import { UserController } from "./UserController";

// mock target
import { UserService } from "../services/userService"

// types
import { Request, Response } from "express"
import { Knex } from "knex"

// mock socket.io testing
import SocketIO from 'socket.io';

// mock module
jest.mock("express")

describe("UserController Testing", () => {
    let controller: UserController
    let service: UserService

    let req: Request
    let res: Response

    let io: SocketIO.Server
    let emit: jest.Mock



    beforeEach(() => {
        service = new UserService({} as Knex)
        jest.spyOn(service, "login").mockImplementation( () => {
            return Promise.resolve(
                {
                    id: 40,
                    username: "byebye",
                    password: "$2a$10$QE.U4jstd2O1d8n0nz.wKOLV5h4yowFtQ495uqTLWEzeNxP.3tkl2",
                    nickname: "byebye",
                    email: "byebye@casino.com",
                    user_icon: "Charizard.webp",
                    created_at: "2021-07-12 12:57:55.970024+08",
                    updated_at: "2021-07-12 12:57:55.970024+08",
                    online: "",
                    balance: 100000,
                    online_boolean: true 
                }
            )
        })

        jest.spyOn(service, "display_all_online_users")

        jest.spyOn(service, "updating_user_socket_ID")
        
        io = {
            emit: jest.fn(() => ({ emit })),

          } as any as SocketIO.Server;
      

        controller = new UserController(service, io as SocketIO.Server)

        req = {
            params: {},
            body: { username: "byebye", password: "1234"},
            query: {},
            session: {},
            emit: {}
            
        } as any as Request

        res = {
            status: jest.fn(() => res),
            json: jest.fn()
        } as any as Response
    })

    it("test login success", async () => {
        const spyJSON = jest.spyOn(res, "json")
        await controller.login(req, res)
        expect(service.login).toBeCalled()
        expect(io.emit).toBeCalledWith("show_online_user", "byebye")
        expect(service.display_all_online_users).not.toBeCalled()
        expect(service.updating_user_socket_ID).not.toBeCalled()
        expect(spyJSON).toBeCalledWith({
            message: "success"
        })
    })
})
