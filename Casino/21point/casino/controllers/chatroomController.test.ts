// testing target
import { ChatroomController } from "./chatroomController"

// mock target
import { ChatroomService } from "../services/chatroomService"

// types
import { Request, Response } from "express"
import { Knex } from "knex"

// mock socket.io testing
import SocketIO from "socket.io"

// mock module
jest.mock("express")

describe("chatroomController Testing", () => {
    let controller: ChatroomController
    let service: ChatroomService

    let req: Request
    let res: Response

    let io: SocketIO.Server
    let emit: jest.Mock



    beforeEach(() => {
        service = new ChatroomService({} as Knex)
        jest.spyOn(service, "sending_chat").mockImplementation(() => {
            return Promise.resolve(
                3
            )
        })

        io = {
            emit: jest.fn(() => ({ emit }))
        } as any as SocketIO.Server

        controller = new ChatroomController(service, io as SocketIO.Server)

        req = {
            params: {},
            body: { text_content: "hi" },
            query: {},
            session: { user_id: 3, username: "edwin" },
            emit: {}

        } as any as Request

        res = {
            status: jest.fn(() => res),
            json: jest.fn()
        } as any as Response

    })

    it("test sending chat success", async () => {
        const spyJSON = jest.spyOn(res, "json")
        await controller.public_chat(req, res)
        expect(service.sending_chat).toBeCalled()
        expect(io.emit).toBeCalledWith("chatroom_text_box", ["edwin", "hi"])
        expect(spyJSON).toBeCalledWith({ message: "id:3,username:edwin,message:hi" })
    })
})
