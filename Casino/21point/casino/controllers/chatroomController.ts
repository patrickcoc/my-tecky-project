import { ChatroomService } from "../services/chatroomService"
import { Request, Response } from "express"
import { Server as SocketIO } from "socket.io"

export class ChatroomController {
    constructor(private chatroomService: ChatroomService, private io: SocketIO) { }

    public_chat = async (req: Request, res: Response) => {
        try{
            const username:string = req.session["username"]
            const user_id:number = req.session['user_id']
            const message = req.body['text_content']
            if(!message){
                res.status(400).json({message:"missing input"})
                return
            }
            const id = await this.chatroomService.sending_chat(message,user_id)
            this.io.emit("chatroom_text_box",[username,message])
            res.json({
                message:`id:${id},username:${username},message:${message}`
            })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({message:"internal server error"})
        }
    }
}