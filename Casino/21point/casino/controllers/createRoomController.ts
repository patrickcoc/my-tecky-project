import { CreateRoomService } from "../services/createRoomService"
import { Request, Response } from "express"
import { Server as SocketIO } from "socket.io"

// import { checkPassword } from "../hash"

export class CreateRoomController {
    constructor(
        private createRoomService: CreateRoomService,
        private io: SocketIO) { }

    create_room = async (req: Request, res: Response) => {
        try {
            const user_id = req.session["user_id"]
            // console.log(typeof (user_id))
            if (user_id === null || user_id === undefined) {
                res.status(400).json({ message: "not yet login in" })
                return
            } else {
                const { room_name, room_password } = req.body
                if (!room_name) {
                    res.status(400).json({ message: "missing username" })
                    return
                }

                const room_id = await this.createRoomService.create_room(room_name, room_password, user_id)

                this.io.socketsJoin(`room_id_${room_id}`)
                // this.io.to(room_id).emit('rev_join_room')
                this.io.emit(`room_id`, room_id, room_name, room_password)


                res.status(200).json({ "room_id": room_id, "user_id": user_id })
            }


        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "open_room_error" })
        }
    }

    show_room = async (req: Request, res: Response) => {
        try {
            const data = await this.createRoomService.show_room()
            res.status(200).json({ data })
        } catch (err) {
            console.log(err.message)
            console.log("show_room_error")
        }
    }

    show_latest_room = async (req: Request, res: Response) => {
        try {
            const data = await this.createRoomService.show_latest_room()
            res.status(200).json({ data })

        } catch (err) {
            console.log(err.message)
            console.log("show_latest_room_error")
        }
    }

    //遊戲大廳 加入
    join_room = async (req: Request, res: Response) => {
        try {
            const join_room_id = req.body
            const user_id = req.session["user_id"]
            const nickname = await this.createRoomService.join_room(user_id)
            // console.log(nickname)
            console.log(join_room_id)
            // console.log(`roomid: ${join_room_id.target_room_id}`)
            this.io.socketsJoin(`room_id_${join_room_id.target_room_id}`)
            this.io.to(`room_id_${join_room_id.target_room_id}`).emit('rev_join_room', nickname)
            // res.status(200).json( {data} )
        } catch (err) {
            console.log(err.message)
            console.log("join_room_error")
        }
    }



}