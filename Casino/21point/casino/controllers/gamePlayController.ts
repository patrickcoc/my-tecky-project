import { Server as SocketIO } from "socket.io";
import { Request, Response } from "express";
import { GamePlayService } from "../services/gamePlayService"
// import{newPlayer}from"../server"
export class GamePlayController {
    constructor(private gameplayService: GamePlayService, private io: SocketIO) { }

    getPlayer = async (req: Request, res: Response) => {
        try {
            if(req.session["user_id"]){
            const userID: any|void = req.session["user_id"]
            // const player_info = await this.gameplayService.getPlayer(userID)
            this.io.emit("receiveNew", userID)
            }
            res.status(200).json({ message: "get player info" });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "getplayer:internal server error" });
        }
    };

    createPlayerRecord = async (req: Request, res: Response) => {
        try {
            const { id, win, earn_coin } = req.body;
            await this.gameplayService.createPlayerRecord(id, win, earn_coin);
            res.status(200).json({ message: "create new playrecord" });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "createrecord: internal server error" });
        }
    };

    updataPlayerAmount = async (req: Request, res: Response) => {
        try {
            const { nickname, balance } = req.body;
            const latest_balance = await this.gameplayService.updataPlayerAmount(nickname, balance);
            req.session['balance'] = latest_balance
            console.log(`session['balance']:${req.session['balance']}`)
            res.status(200).json({ message: "updata Amount" });
        } catch (err) {
            console.error(err.message);
            res.status(500).json({ message: "updataamount; internal server error" });
        }
    };
}