import { GameRoomService } from "../services/gameRoomService"
import { Request, Response } from "express"

export class GameRoomController {
    constructor(private gameRoomService: GameRoomService) {}

    get_room_info = async ( req: Request, res: Response ) => {
        try{
            const room_id = parseInt(req.params.id)
            const room_info = await this.gameRoomService.get_room_info(room_id)
            console.log(room_info)
            res.status(200).json(room_info)


        } catch (err) {
            console.error(err.message)
            res.status(500).json({message: "get room info error"})
        }
    }



}