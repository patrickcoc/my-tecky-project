import { MemberService } from "../services/memberService";
import { Request, Response } from "express";

export class MemberController {
    constructor(private memberService: MemberService) { }

    get_member_info = async (req: Request, res: Response) => {
        try {
            const { username } = { username:req.session["username"] }
            const member_info = await this.memberService.get_member_information(username)
            res.json({member_info})

        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }
    get_member_item = async (req: Request, res: Response) => {
        try {
            const userID= req.session["user_id"]
            console.log('getid',userID)
            const get_item=await this.memberService.get_member_item(userID)
            res.json(get_item)
            console.log('getitem',get_item)

        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "getitem:internal server error" })
        }
    }
}