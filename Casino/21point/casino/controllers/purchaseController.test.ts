// testing target
import { PurchaseController } from "./purchaseController";

// mock target
import { PurchaseService } from "../services/purchaseServices";

// types
import { Request, Response } from "express"
import { Knex } from "knex"

//mock
jest.mock("express")

describe("PurchaseController success Testing", () => {
    let controller: PurchaseController
    let service: PurchaseService

    let req: Request
    let res: Response

    beforeEach(() => {
        service = new PurchaseService({} as Knex)
        jest.spyOn(service, "purchasing_personal_item").mockImplementation(
            () => {
            return Promise.resolve()
        }
        )

        controller = new PurchaseController(service)

        req = {
            params: {},
            body: { "item": "改名卷" },
            query: {},
            session: { user_id: 5 }
        } as any as Request;

        res = {
            status: jest.fn(() => res),
            json: jest.fn()
        } as any as Response
    })

    it("test purchasing_personal_item success", async () => {
        const spyJSON = jest.spyOn(res, "json")
        await controller.post(req, res)
        expect(service.purchasing_personal_item).toBeCalled()
        expect(spyJSON).toBeCalledWith(
            {
                "message": "1 of Item 改名卷 has bought by user_id:5 successfully"
            }
        )

    })

})