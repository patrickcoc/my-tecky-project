import { PurchaseService } from "../services/purchaseServices"
import { Request, Response } from "express"

export class PurchaseController {
    constructor(private purchaseService: PurchaseService) {}

    // get all purchased item

    // create purchased item
    post = async (req: Request, res: Response) => {
        try{
            const user_id:number = req.session['user_id']
            const item:string = req.body['item']
            // const quantity:number = req.body['quantity']
            const quantity:number = 1
            await this.purchaseService.purchasing_personal_item(user_id,item,quantity)
            res.status(200).json({message:`${quantity} of Item ${item} has bought by user_id:${user_id} successfully`})
        } catch(err) {
            console.error(err.message)
            res.status(500).json({message:"internal server error"})
        }
    }

    // update purchased item


    // delete purchased item
}