import { QuitRoomService } from "../services/quitRoomService"
import { Request, Response } from "express"
import { Server as SocketIO} from "socket.io"

export class QuitRoomController {
    constructor(
        private quitRoomService: QuitRoomService,
        private io: SocketIO
    ) {}

    quit_room = async (req: Request, res: Response) => {
        try{
            const quit_room_id = req.body
            const user_id = req.session["user_id"]
            const nickname = await this.quitRoomService.quit_room(user_id)
            // console.log(nickname)
            console.log(`QUIT ROOM ID: ${quit_room_id.quit_room_id}`)
            console.log(typeof(quit_room_id.quit_room_id))
            // this.io
            this.io.socketsLeave(`room_id_${quit_room_id.quit_room_id}`)
            // this.io.emit('leave_room', nickname)
            this.io.to(`room_id_${quit_room_id.quit_room_id}`).emit('leave_room', nickname)
            res.status(200).json({message: "success"})
            // this.io.emit(`leave_room`, nickname)
        } catch (err) {
            console.error(err.message)
            console.log("quit room error")
            res.status(500).json({message: "Internal Server Error"})
        }
    }

}