import { Request, Response } from "express";
import { ShopService } from "../services/shopServices";

export class ShopController {
    constructor(private shopService : ShopService) { }

        // showItem = async (req:Request, res: Response)=> {
        //     try{
        //         const { id,item_name,cost,game_coin_id,item_icon } = req.body;
        //         await this.shopService.showItem(id,item_name,cost,game_coin_id,item_icon );
        //         res.status(200).json({measage: "get items success"});
        //     }catch (err) {
        //         console.error(err.message);
        //         res.status(500).json({ message: "internal server error" });
        //     }
        // }

        get_shop_item = async (req:Request, res: Response) => {
            try{
                const item = await this.shopService.get_shop_item();
                res.status(200).json({ item });
            }catch (err) {
                console.error(err.message);
                res.status(500).json({ message: "internal server error" });
            }
        }



        
    }
