import { UserService } from "../services/userService"
import { Request, Response } from "express"
import { checkPassword } from "../hash"
import { Server as SocketIO } from 'socket.io'

export class UserController {
  constructor(private userService: UserService, private io: SocketIO,) { }

  login = async (req: Request, res: Response) => {
    try {
      const { username, password } = req.body
      if (!username || !password) {
        res.status(400).json({ message: "missing username/password" })
        return
      }
      const user = await this.userService.login(username)
      if (!user || !(await checkPassword(password, user.password))) {
        res.status(400).json({ message: "invalid username/password" })
        return
      }
      req.session["username"] = user.username;
      console.log(`[req.session["username"]:${req.session["username"]}`)
      req.session["user_id"] = user.id
      console.log(`[req.session["user_id"]:${req.session["user_id"]}`)
      req.session["balance"] = user.balance
      console.log(`[req.session["balance"]:${req.session["balance"]}`)

      this.io.emit("show_online_user", user.nickname)

      res.status(200).json({ message: "success" })
    } catch (err) {
      console.error(err.message)
      res.status(500).json({ message: "internal server error" })
    }
  }

  logout = async (req: Request, res: Response) => {
    try {
      if (req.session['user_id']) {
        // console.log(req.session['user_id'])
        await this.userService.logout(req.session['user_id'])

        this.io.emit("update_show_user")

        req.session.destroy(() => {
          console.log("logout")
          res.status(200).json({ message: "logout success" })
        })
      } else {
        console.log("no req.session")
      }

    } catch (err) {
      console.error(err.message)
      console.log("logout error")
    }
  }

  logged_in_function = async (req: Request, res: Response) => {
    try {
      if (req.session['user_id']) {
        res.status(200).json({ message: "reload get function success" })
      } else {
        console.log("Not yet Login")
      }
    } catch (err) {
      console.error(err.message)
      console.log("get logged in function error")
      res.status(400).json({ message: "not yet log in" })
    }
  }

  display_online_users = async (req: Request, res: Response) => {
    try {
      const online_users = await this.userService.display_all_online_users()
      // console.log("onlineeeeeeeeeeeeeeeeee")
      // console.log(online_users)
      const show_onlines = online_users.map(user => user.nickname)
      // const show_all_onlines = online_users.map(user => user.nickname)
      this.io.emit("online_user", show_onlines)
      // this.io.emit("online_user", show_all_onlines)
      // res.status(200).json({ "online_users": show_all_onlines })
      // res.status(200).json({ "online_users": online_users })
      res.status(200).json({ "online_users": show_onlines })
    } catch (err) {
      console.error(err.message)
      res.status(500).json({ message: "internal server error" })
    }
  }
  save_online_id = async (req: Request, res: Response) => {
    try {
      const socket_ID = req.query['socket_ID']

      if (!req.session['username']) {
        res.status(200).json({ message: `Received SocketID:${socket_ID}` })
        return
      }
      const username = req.session['username']
      console.log(`[username]:${username}`)
      const content = { socket_ID, username }
      console.log(`[socket_ID]:${socket_ID}`)
      await this.userService.updating_user_socket_ID(content)
      res.status(200).json({ message: `Received Username: ${username}, Received SocketID:${socket_ID}` })
    } catch (err) {
      console.error(err.message)
      res.status(500).json({ message: "internal server error" })
    }
  }

  add_user_balance = async (req: Request, res: Response) => {
    try {

      const user_id = req.session["user_id"]
      await this.userService.add_user_balance(user_id)

      res.status(200).json({ message: "success" })
      // this.io.emit(`leave_room`, nickname)
    } catch (err) {
      console.error(err.message)
      console.log("quit room error")
      res.status(500).json({ message: "Internal Server Error" })
    }
  }

  register_user = async (req: Request, res: Response) => {
    try {
      const { email, nickname, username, password } = req.body
      if (!username || !password || !email || !nickname) {
        res.status(400).json({ message: "missing information" })
        return
      }
      // await this.userService.login(username)
      const register_obj = await this.userService.register_user(email, nickname, username, password)
      console.log("this is object")
      console.log(register_obj.boolean)

      if (register_obj.boolean === true) {

        req.session["username"] = username;
        console.log(`[req.session["username"]:${req.session["username"]}`)
        req.session["user_id"] = register_obj.id
        console.log(`[req.session["user_id"]:${req.session["user_id"]}`)

        this.io.emit("show_online_user", nickname)

        res.status(200).json({ message: "success" })

        return
      }

      if (register_obj.boolean === false) {
        console.log(false)
        res.status(400).json({ message: "username/email has been used" })
        return
      }

    } catch (err) {
      console.error(err.message)
      console.log("register error")
      res.status(500).json({ message: "Internal Server Error" })
    }



  }

  change_nickname = async (req: Request, res: Response) => {
    try {
      const user_id = req.session["user_id"]
      const nickname = req.body['nickname']
      const item_name = `改名卷`
      console.log("Below is session")
      console.log(req.session)
      const new_nickname = await this.userService.change_user_nickname(user_id, nickname, item_name)
      console.log(new_nickname)
      if (new_nickname === undefined) {
        console.log("repeated current nickname")
        res.status(400).json({ message: "repeated current name" })
        return
      }

      res.status(200).json({ message: `new_nickname:${new_nickname}` })
    } catch (err) {
      console.error(err.message)
      res.status(500).json({ message: "internal server error" })
    }
  }

  check_login = async (req: Request, res: Response) => {
    try {
      const user_id = req.session["user_id"]

      if (user_id === null || user_id === undefined) {
        res.status(400).json({ message: "not yet login in" })

      } else {
        res.status(200).json({ message: "already log in" })
      }
    } catch (err) {
      console.error(err.message)
      res.status(500).json({ message: "internal server error" })
    }
  }


}
