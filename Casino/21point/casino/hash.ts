import bcrypt from 'bcryptjs';

const SALT_ROUNDS = 10;

export async function hashPassword(plainPassword:string) {
    const hash = await bcrypt.hash(plainPassword,SALT_ROUNDS);
    return hash;
};


export async function checkPassword(plainPassword:string,hashPassword:string){
    const match = await bcrypt.compare(plainPassword,hashPassword);
    return match;
}

// async function main(){
//     const hashed = await hashPassword("tecky");
//     console.log(hashed);


//     const check = await checkPassword("tecky","$2a$10$ecdGKXr0PeUeItiJKZGS5.O3jdIhVA.qTO5dqO31Osfv.Qo71X60G");
//     console.log(check);
// }

// main();