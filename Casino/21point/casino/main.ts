import express from "express";
import expressSession from "express-session";
import path from "path";
import multer from "multer";
// import { logger } from "./logger";
import { isLoggedIn } from "./guards";
import dotenv from "dotenv";
import pg from "pg"
import http from "http";
import { Server as SocketIO } from "socket.io";
import grant from "grant";
import Knex from "knex";
// import fetch from 'node-fetch'

// dotenv.config(); // load .env 嘅內容
dotenv.config();
export const client = new pg.Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD
})

try {
  client.connect();
  console.log("Connected to DB");
  client.query('UPDATE users SET online_boolean = false')
} catch (err) {
  console.log(err.message);
  console.log("Error : cannot connect to DB");
}

import * as knexConfig from "./knexfile";
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);

export const app = express();
export const server = new http.Server(app);
export const io = new SocketIO(server);

const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, path.resolve("uploads"));
  },
  filename: function (req, file, cb) {
    cb(null, `${file.fieldname}-${Date.now()}.${file.mimetype.split("/")[1]}`);
  },
});
export const upload = multer({ storage });

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

// Why use two Session ????????????
// app.use(
//   expressSession({
//     secret: "seven head",
//     resave: true,
//     saveUninitialized: true,
//   })
// );

const grantExpress = grant.express({
  defaults: {
    origin: "http://localhost:8080",
    transport: "session",
    state: true,
  },
  google: {
    key: process.env.GOOGLE_CLIENT_ID || "",
    secret: process.env.GOOGLE_CLIENT_SECRET || "",
    scope: ["profile", "email"],
    callback: "/login/google",
  },
});

app.use(grantExpress);

// app.use((req, res, next) => {
//   logger.debug(`ip: [${req.ip}], path: [${req.path}] method: [${req.method}]`);
//   next();
// });

const stripe = require('stripe')('sk_test_51JCR6CBzGZ0mNTpr8aV4LDqLWKlOCoNhmKS6m4oIBJba9RAJmi9orTYhpYNLVnKEpBhOdWAwRkyrAgbpPN7LLktD00IbMVqkrX');

app.post("/payment", async (req, res) => {
  const { product } = req.body;
  const session = await stripe.checkout.sessions.create({
      payment_method_types: ["card"],
      line_items: [
          {
              price_data: {
                  currency: "hkd",
                  product_data: {
                      name: product.name,
                      images: [product.image],
                  },
                  unit_amount: product.amount * 100,
              },
              quantity: product.quantity,
          },
      ],
      mode: "payment",
      success_url: `https://www.blackjacklion.com/success.html`,
      cancel_url: `https://www.blackjacklion.com/cancel.html`,
  });
  // console.log("See session")
  // console.log(session.payment_status)
  // console.log("See session id")
  // console.log(session.id)
  res.json({ id: session.id  });
  // res.status(200).json({ message: "success" });
});


const sessionMiddleware = expressSession({
  secret: 'Tecky Academy teaches typescript',
  resave: true,
  saveUninitialized: true,
  cookie: { secure: false }
});

app.use(sessionMiddleware);

io.use((socket, next) => {
  let req = socket.request as express.Request
  let res = req.res as express.Response
  sessionMiddleware(req, res, next as express.NextFunction)
})

import { UserService } from "./services/userService";
import { UserController } from "./controllers/userController";


const userService = new UserService(knex);
export const userController = new UserController(userService, io);

import { CreateRoomService } from "./services/createRoomService"
import { CreateRoomController } from "./controllers/createRoomController"

const createRoomService = new CreateRoomService(knex);
export const createRoomController = new CreateRoomController(createRoomService, io)

import { QuitRoomService } from "./services/quitRoomService"
import { QuitRoomController } from "./controllers/quitRoomController";

const quitRoomService = new QuitRoomService(knex);
export const quitRoomController = new QuitRoomController(quitRoomService, io)

import { MemberService } from "./services/memberService";
import { MemberController } from "./controllers/memberController";

const memberService = new MemberService(knex)
export const memberController = new MemberController(memberService)


import { ChatroomService } from "./services/chatroomService";
import { ChatroomController } from "./controllers/chatroomController";

const chatroomService = new ChatroomService(knex)
export const chatroomController = new ChatroomController(chatroomService,io)

import { GameRoomService } from "./services/gameRoomService";
import { GameRoomController } from "./controllers/gameRoomController";

const gameRoomService = new GameRoomService(knex)
export const gameRoomController = new GameRoomController(gameRoomService)

import { GamePlayService } from "./services/gamePlayService";
import { GamePlayController } from "./controllers/gamePlayController";

const gameplayService = new GamePlayService(knex)
export const gameplayController = new GamePlayController(gameplayService, io)

import { ShopService } from "./services/shopServices";
import { ShopController } from "./controllers/shopController";

const shopService = new ShopService(knex);
export const shopController = new ShopController(shopService)

import { PurchaseService } from "./services/purchaseServices";
import { PurchaseController } from "./controllers/purchaseController";

const purchaseService = new PurchaseService(knex)
export const purchaseController = new PurchaseController(purchaseService)

import { chatroomRoutes } from "./routers/chatroomRoutes";
import { userRoutes } from "./routers/userRoutes";
import { memberRoutes } from "./routers/memberRoutes";
import { createRoomRoutes } from "./routers/createRoomRoutes"
import { gameRoomRoutes } from "./routers/gameRoomRoutes"
import { gamePlayRoutes } from "./routers/gameplayRoutes"
import { quitRoomRoute } from "./routers/quitRoomRoute";
import { shopRoutes } from "./routers/shopRoutes"
import { purchaseRoutes } from "./routers/purchaseRoutes"

app.use(chatroomRoutes)
app.use(userRoutes);
app.use(memberRoutes)
app.use(createRoomRoutes)
app.use(gameRoomRoutes)
app.use(gamePlayRoutes)
app.use(quitRoomRoute)
app.use(shopRoutes)
app.use(purchaseRoutes)

app.use(express.static(path.join(__dirname, "public")));
app.use("/images", express.static(path.join(__dirname, "uploads")));

app.use(isLoggedIn, express.static(path.join(__dirname, "protected")));

app.use((req, res) => {
  res.sendFile(path.join(__dirname, "public/404.html"));
});

// const socketID_username_record:Record<string, string> = {};

let head_count = 0
let user_online_arrays: {}


import { leftGame,sendDisconnect,Chat,leave, playerIn, bet, welcomeNewPlayer, Stand, playerReady, hitCard,cheat } from "./server"




io.on('connection', function (socket: any) {
  // main connected
  const session = socket.request.session.user_id
  console.log('session', session,socket.request.session)
  head_count += 1
  console.log(`online${head_count}`)
  // io.emit("test", 123)

  socket.on('update_username', (msg: any) => {
    Object.assign(user_online_arrays, { ['socket.id']: msg })
  })

  socket.on('receive', async (userID: any) => {
    let player_info = await gameplayService.getPlayer(session)
    welcomeNewPlayer(player_info, socket)
    socket.emit('new user info', { info: player_info, id: socket.id })
  })

  console.log('new connection' + socket.id);
  // upDateUser(socket)
  playerReady(socket)
  bet(socket)
  playerIn(socket)
  hitCard(socket)
  Stand(socket)
  leave(socket)
  cheat(socket)
  sendDisconnect(socket)
  leftGame(socket)
  Chat(socket)
  socket.on('update database', async function (player: any) {
    let player_amount = await gameplayService.updataPlayerAmount(player.nickname, player.balance)
    console.log(player_amount)
  })
  socket.on("disconnect", async () => {
    head_count -= 1
    console.log(head_count)
    console.log(`${socket.id}:disconnected`)
    // console.log(`username:${socketID_username_record[socket.id]}`)

    await knex('users')
      .where('online', socket.id)
      .update("online", "")
  })



})


const PORT = 8080;
server.listen(PORT, () => {
  console.log(`listening to port: [${PORT}]`);
});
