import { Knex } from "knex";

const tableNameUser = "users";
const tableNameRoom = "room";
const tableNameFriendList = "friend_list";
const tableNameGameCoin = "game_coin";
const tableNameUserItem = "user_item";
const tableNamePersonalMatchHistoryList = "personal_match_history_list";
const tableNameGameMatchHistory = "game_match_history";
const tableNameChatRoom = "chat_room";
const tableNameNotes = "notes";

export async function up(knex: Knex): Promise<void> {
  await knex.schema.createTable(tableNameUser, (table) => {
    table.increments();
    table.string("username").unique().notNullable();
    table.string("password").notNullable();
    table.string("nickname").notNullable();
    table.string("email").notNullable();
    table.text("user_icon").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tableNameRoom, (table) => {
    table.increments();
    table.string("room_name").notNullable();
    table.string("room_password").notNullable();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references(`${tableNameUser}.id`)
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tableNameFriendList, (table) => {
    table.increments();
    
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references(`${tableNameUser}.id`)
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tableNameGameCoin, (table) => {
    table.increments();
    table.integer("amount").notNullable();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references(`${tableNameUser}.id`)
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tableNameNotes, (table) => {
    table.increments();
    table.integer("coin_id").unsigned().notNullable();
    table.foreign("coin_id").references(`${tableNameGameCoin}.id`)
    table.integer("input").notNullable();
    table.integer("output").notNullable();
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tableNameUserItem, (table) => {
    table.increments();
    table.integer("user_id").unsigned().notNullable();
    table.foreign("user_id").references(`${tableNameUser}.id`)
    table.string("item")
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tableNamePersonalMatchHistoryList, (table) => {
    table.increments();
    table.integer("user_id").notNullable();
    table.foreign("user_id").references(`${tableNameUser}.id`);
    table.boolean("win").notNullable();
    table.integer("earn_coin");
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tableNameGameMatchHistory, (table) => {
    table.increments();
    table.string("blackjack_record");
    table.time("created_time")
    table.integer("game_match_id").unsigned().notNullable();
    table.foreign("game_match_id").references(`${tableNamePersonalMatchHistoryList}.id`);
    table.timestamps(false, true);
  });

  await knex.schema.createTable(tableNameChatRoom, (table) => { 
    table.increments();
    table.integer("room_id").unsigned().nullable();
    table.foreign("room_id").references(`${tableNameRoom}.id`)
    table.text("content").notNullable();
    table.timestamps(false, true);
  });

}

export async function down(knex: Knex): Promise<void> {
  await knex.schema.dropTable(tableNameChatRoom); 
  await knex.schema.dropTable(tableNameGameMatchHistory); 
  await knex.schema.dropTable(tableNamePersonalMatchHistoryList);
  await knex.schema.dropTable(tableNameUserItem);
  await knex.schema.dropTable(tableNameNotes);
  await knex.schema.dropTable(tableNameGameCoin);
  await knex.schema.dropTable(tableNameFriendList);
  await knex.schema.dropTable(tableNameRoom);
  await knex.schema.dropTable(tableNameUser);

}

