import { Knex } from "knex";

const tableNameRoom = "room";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(tableNameRoom, (table) => {
        table.string("room_password").alter();
      });

}


export async function down(knex: Knex): Promise<void> {
    // await knex.schema.dropTable(tableNameRoom);
}

