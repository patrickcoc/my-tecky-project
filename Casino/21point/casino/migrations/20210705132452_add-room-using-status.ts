import { Knex } from "knex";

const tableNameRoom = "room";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(tableNameRoom,(table)=> {
        table.boolean("active").defaultTo("true");
    });
}


export async function down(knex: Knex): Promise<void> {
    // await knex.schema.dropTable(tableNameRoom);
}

