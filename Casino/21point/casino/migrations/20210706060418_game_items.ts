import { Knex } from "knex";
const tableNameItems = "game_shop_items"


export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tableNameItems, (table) => {
        table.increments();
        table.string("item_name").unique().notNullable();
        table.integer("cost").notNullable();
        table.integer("game_coin_id").unsigned().notNullable();
        table.foreign("game_coin_id").references('game_coin_id')
        table.text("item_icon").notNullable();
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tableNameItems); 
}

