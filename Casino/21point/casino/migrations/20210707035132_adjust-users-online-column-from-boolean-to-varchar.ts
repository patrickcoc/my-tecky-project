import { Knex } from "knex";

const tableNameUser = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(tableNameUser,(table) => {
        table.string("online").nullable().alter();
    })
}


export async function down(knex: Knex): Promise<void> {
    // await knex.schema.dropTable(tableNameUser)
}

