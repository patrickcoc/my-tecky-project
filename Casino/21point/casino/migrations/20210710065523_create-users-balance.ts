import { Knex } from "knex";

const tableNameUser = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(tableNameUser, (table) => {
        table.integer("balance").notNullable().defaultTo(0);
    })
   
}


export async function down(knex: Knex): Promise<void> {
}

