import { Knex } from "knex";

const tableNameUser = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(tableNameUser, (table) => {
        table.boolean("online_boolean").notNullable().defaultTo('false')
    })
}


export async function down(knex: Knex): Promise<void> {
}

