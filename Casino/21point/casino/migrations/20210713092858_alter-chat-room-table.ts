import { Knex } from "knex";

const tableNameChatRoom = "chat_room";
const tableNameUser = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(tableNameChatRoom,(table) => {
        table.integer("user_id").notNullable();
        table.foreign("user_id").references(`${tableNameUser}.id`)
    })
}


export async function down(knex: Knex): Promise<void> {
}

