import { Knex } from "knex";

const tableNameUserItem = "user_item";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(tableNameUserItem,(table) => {
        table.integer("quantity").nullable()
    })
}


export async function down(knex: Knex): Promise<void> {
}

