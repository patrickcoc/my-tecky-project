import { Knex } from "knex";

const tableNameUserItem = "user_item";
const tableNameItems = "game_shop_items"


export async function up(knex: Knex): Promise<void> {
    await knex.schema.alterTable(tableNameUserItem,(table) => {
        table.integer("item_id").unsigned().notNullable();
        table.foreign("item_id").references(`${tableNameItems}.id`)
    })
}


export async function down(knex: Knex): Promise<void> {
}

