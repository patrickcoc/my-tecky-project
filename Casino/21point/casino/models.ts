export interface User {
    id: number
    username: string
    password: string
    email: string
    online: string
    nickname: string
    user_icon: any
    online_boolean: boolean
    balance: number
}

export interface Member {
    username: string
    nickname: string
    email: string
    user_icon: string
}
export interface Record {
    id: number;
    win: boolean;
    earn_coin: number;
}

export interface Room {
    id: number;
    room_name: string;
    room_password: string | null
    user_id: number
}

export interface Items {
    id: number;
    item_name: string;
    cost: number;
    item_icon: string;
}

export interface ICard {
    Suit: string;
    Value: number;
    Rank: string;
    img: string
}

export interface IPlayer {
    id: string;
    player_id: string;
    nickname: string;
    balance: number;
    icon: string;
    bet: number;
    hand: ICard[];
    total: number;
    displayTotal: string;
    doubleDown: boolean;
    splitHand: null | ICard[];
}

export interface Chatroom{
    id: number
    user_id: number
    room_id: number
    content: string
}

export interface UserItem{
    id:number,
    user_id:number;
    quantity:number;
    item_id:number
}

