const socket = io.connect()

socket.on(`rev_join_room`, (nickname) => {
    console.log(`${nickname} is joined`)
})

socket.on(`leave_room`, (nickname) => {
    console.log(`${nickname} is quited`)
})


const room_name = document.querySelector('.room_name')

window.addEventListener('load', async function () {

    // const check_res = await fetch(`/check_playing`)

    const room_id = get_room_id()
    console.log(room_id)
    const res = await fetch(`/get_room_info/${room_id}`)
    const room_title = await res.json();
    room_name.innerHTML = room_title
})

// window.onbeforeunload = function () {
//     return "There are unsaved changes. Leave now?";
// };


//Leave Room Button
const quit_button = document.querySelector('.quit_button')
quit_button.addEventListener('click', async function () {
    const quit_room_id = get_room_id()
    console.log("click")
    const res = await fetch('/quit_room', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify({ quit_room_id }),
    })

    console.log(res.status)
    window.location = `/`
})

function get_room_id(){
    const searchStr = window.location.search
    const id = new URLSearchParams(searchStr).get('id')
    return id
}


// function get_room_info () {
//     const 
// }