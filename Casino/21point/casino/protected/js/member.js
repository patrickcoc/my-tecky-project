const gamers_info = document.querySelector('.gamers_info')
const profile_picture = document.querySelector('.profile_picture')

function main() {
    get_user_information()
    get_user_item()
}

async function get_user_information() {
    const res = await fetch("/membership")
    const data = await res.json()
    const user = data.member_info[0]
    gamers_info.innerHTML = `
    <div class="users_info">
    <div class="users_info_items">帳號名稱：${user.username}</div>
    <div class="users_info_items">別名：${user.nickname}</div>
    <div class="users_info_items">電郵 : ${user.email}</div>
    </div>
    `
    profile_picture.innerHTML = `
    <img class="user_profile" src="./images/${user.user_icon}" alt="7-head">
    `
}
async function get_user_item() {
    const res = await fetch("/membership_item")
    const data = await res.json()
    console.log('check data', data)
    for (let i = 0; i < data.length; i++) {
        console.log(data[i])
        item_inner = `
        <div class="item_info">
            <div class="item_icon"><img src="./images/${data[i].item_icon}"></div>
            <div class="item_name">${data[i].item}</div>
         </div>`
        //original
        // item_inner = `<div class="item_info">
        // <div class="item_name">${data[i].item}</div>
        //  <div class="item_icon"><img src="./images/${data[i].item_icon}"></div>
        //  <div class="item_price">價錢：${data[i].cost} / 1</div>
        //  <div class="item_quantity">已持有：${data[i].quantity}</div>
        //  </div>`
        document.querySelector('.shop_items_list').innerHTML += item_inner


        const item_icon_img = document.querySelectorAll('.item_icon>img')
        for (let i = 0; i < item_icon_img.length; i++) {
            item_icon_img[i].addEventListener('click', function (e) {
                console.log(e.target.parentNode.nextSibling.nextSibling.innerHTML)
                const item_name = e.target.parentNode.nextSibling.nextSibling.innerHTML
                console.log(e.target.innerHTML)
                const target_image = e.target.innerHTML
                console.log(e.target)
                const use_item_info = document.querySelector('.use_item_info')

                use_item_info.innerHTML =
                    `<div class="item_icon_">${e.target.parentNode.innerHTML}</div>
                 <div class="item_name">${item_name}</div>
                 <button onclick="open_change_name_form()" class="use_submit">使用</button>          
                `
                use_action()



            })

        }
    }
}


//close user_item_form
const use_item_form = document.querySelector('#use_item_form')
const close_use_item_form_button = document.querySelector('.close_use_item_form_button')
const use_item_form_container = document.querySelector('.use_item_form_container')

close_use_item_form_button.addEventListener('click', function () {
    use_item_form_container.classList.remove('show')
    use_item_form_container.classList.add('hide')
})


function use_action() {
        use_item_form.addEventListener('submit', async function (e) {
        e.preventDefault();

        const nickname = use_item_form['changename'].value
        const res = await fetch('/change_name', {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({ nickname }),
        })

        if (res.status === 200) {
            // console.log("200")
            use_item_form['changename'].value =''
            use_item_form_container.classList.remove('show')
            use_item_form_container.classList.add('hide')

            setTimeout(() => {
                location.reload()
            },5000)

        } else if(res.status === 400) {
            // console.log("repeated name")
            document.querySelector('.error_info').innerHTML =
            `*您現已使用此暱稱`
            setTimeout(() => {
                document.querySelector('.error_info').innerHTML =
                ``
            },5000)
        }

    })
}

function open_change_name_form() {
    use_item_form_container.classList.remove('hide')
    use_item_form_container.classList.add('show')
}



main()


