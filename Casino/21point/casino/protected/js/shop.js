//close confirm form
const confirm_form = document.querySelector('#confirm_form')
const close_confirm_form_button = document.querySelector('.close_confirm_form_button')
const confirm_form_container = document.querySelector('.confirm_form_container')

close_confirm_form_button.addEventListener('click', function() {
    confirm_form_container.classList.remove('show')
    confirm_form_container.classList.add('hide')
})

//Close Add Balance Form Container
const close_checkout_button = document.querySelector('.close_add_balance_form')
const checkout_container = document.querySelector('.checkout_container')
close_checkout_button.addEventListener("click", function () {
    if (checkout_container.classList.contains('show')) {
        checkout_container.classList.remove('show')
        checkout_container.classList.add('hide')
    }
})


let all_item_data;
let item;

window.addEventListener('load', async function () {
    create_shop_member_info()

    try {
        const res = await fetch('/get_shop_item')
        all_item_data = await res.json()
        console.log(all_item_data)


        for ( item of all_item_data.item) {

            const product_container_div = document.createElement('div')
            product_container_div.classList.add('product_container')
            product_container_div.innerHTML =
                `
        <div class="product_image">
                <img src="./images/${item.item_icon}">
                
                    <div class="product_info">
                        <div class="product_name">${item.item_name}</div>
                        <div class="product_price">遊戲金幣: $${item.cost}</div>

                    </div>
                
        </div>
            <button id='item_${item.id}' class="buy_button">購買</button>
        
        
        `
            document.querySelector('.shop_container').appendChild(product_container_div)
            
        }

        
        const buy_button = document.querySelectorAll('.buy_button')

        console.log(buy_button[0])
        for (let i = 0; i < buy_button.length; i++) {
            buy_button[i].addEventListener('click', async function (e) {
               
                // console.log(buy_button[i].parentNode)
                console.log(buy_button[i].parentNode.children[0].children[1].firstElementChild.innerText)
                const target_item_name = buy_button[i].parentNode.children[0].children[1].firstElementChild.innerText
                console.log(typeof(target_item_name))
                
                confirm_form_container.classList.remove('hide')
                confirm_form_container.classList.add('show')
                console.log(buy_button[i].id)
                
                
                confirm_form.innerHTML = 
                `<div class= "product_container">
                ${buy_button[i].parentNode.children[0].innerHTML}
                </div>
                `
                

                confirm_form.innerHTML += `<button onclick="confirm_action('${target_item_name}')" class="confirm_submit">確定</button>`


                
                
                
            })
        }


    } catch (err) {
        console.log(err.message)
    }

})

// console.log(confirm_action)
// confirm_action()
const confirm_action = async(target_item_name) => {
    confirm_form_container.classList.remove('show')
    confirm_form_container.classList.add('hide')
    const purchasing_item = { item: target_item_name }
    console.log(target_item_name)
    console.log(purchasing_item)
    console.log("confirm_action")
    const res = await fetch("/purchasing_item",{
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(purchasing_item)
    })

    if(res.status === 200){
        
        location.reload()

    } else {
        alert("唔夠錢阿,老細!")
    }
}

//create shop member info
async function create_shop_member_info() {
    try{
        const res = await fetch('/membership')
    const data = await res.json()
    const user = data.member_info[0]

    const add_balance_div = document.createElement('div')
    add_balance_div.classList.add('add_balance')
    add_balance_div.innerText = `増值`

    const shop_member_info = document.createElement('div')
    shop_member_info.classList.add('shop_show_member_info')
    shop_member_info.innerHTML = `
    
    <div class="game_balance">遊戲金幣: <i class="fas fa-coins"></i> $${user.balance}</div>
    
    `
    shop_member_info.appendChild(add_balance_div)

    add_balance_div.addEventListener('click', function () {
        const checkout_container = document.querySelector('.checkout_container')
        if(checkout_container.classList.contains('hide')){
            checkout_container.classList.remove('hide')
            checkout_container.classList.add('show')
        }
    })

    const shop_container = document.querySelector('.shop_container')

    shop_container.appendChild(shop_member_info)

    // console.log(user)
    } catch(err) {
        console.log(err.message)
    }
    
}