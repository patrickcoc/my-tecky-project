const member = document.querySelector('#member')
const table = document.querySelector('#table')
const game_shop = document.querySelector('#shop')
const member_info_box = document.querySelector('.member_info_box')
const table_interface_box = document.querySelector('.table_interface_box')
const game_shop_box = document.querySelector('.game_shop_box')



member.addEventListener("click", (event) => {
    if(member_info_box.style.display = "none") {
        member_info_box.style.display = "block"
        table_interface_box.style.display = "none"
        game_shop_box.style.display = "none"
        document.querySelector('.room_title_style').innerHTML = "會員管理"
    }
})
table.addEventListener("click", (event) => {
    if(table_interface_box.style.display = "none") {
        table_interface_box.style.display = "block"
        member_info_box.style.display = "none"
        game_shop_box.style.display = "none"
        document.querySelector('.room_title_style').innerHTML = "牌桌外觀"
    }
})

game_shop.addEventListener("click", (event) => {
    if(game_shop_box.style.display = "none") {
        game_shop_box.style.display = "block"
        member_info_box.style.display = "none"
        table_interface_box.style.display = "none"
        document.querySelector('.room_title_style').innerHTML = "遊戲背包"
    }
})


// const item_icon = document.querySelector('.item_icon')
// const item_name = document.querySelector('.item_name')
// const item_cost = document.querySelector('.item_cost') 
