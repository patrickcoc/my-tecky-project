const socket = io.connect()

let socket_id = ""

socket.on("connect",async()=>{
    socket_id = socket.id
    console.log(socket_id)
    await fetch(`/socketID?socket_ID=${socket_id}`,{
        method: "POST"
    })
})

var suits = ["Spades", "Hearts", "Diamonds", "Clubs"];
var ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
var deck = new Array();
var players = new Array();
var currentPlayer = 0;
var chip = 0;
let ready_mode = false;
let create_mode = true;
let hit_mode = true;

// 建立52張牌 
function createDeck() {
    deck = new Array();
    for (var i = 0; i < ranks.length; i++) {
        for (var x = 0; x < suits.length; x++) {
            var values = parseInt(ranks[i]);
            if (ranks[i] == "J" || ranks[i] == "Q" || ranks[i] == "K")
                values = 10;
            if (ranks[i] == "A")
                values = 1;
            var card = { Ranks: ranks[i], Suit: suits[x], Values: values };
            deck.push(card);
        }
    }
    return deck
}

// 洗牌
function shuffle(array) {
    array.sort(() => Math.random() - 0.5);
    console.log(deck)
}



// 增加玩家，NUM為人數
function createPlayers(num) {
    for (var i = 1; i <= num; i++) {
        var hand = new Array();
        var player = { Name: 'Player ' + i, ID: i, Points: 0, Hand: hand, Chip: 0 };
        players.push(player);
    }
}

//ready
function ready() {
    if (create_mode === true) {
        players = new Array();
        hit_mode=true;
        ready_mode = true;
        playerNum = playerNumber()
        createPlayers(playerNum);
        createPlayersUI();
        create_mode = false;
        document.getElementById('btn_ready').value = 'restart';
        document.getElementById("status").style.display = "none";
    }
    else if (create_mode === false) {
        document.getElementById('btn_ready').value = 'ready';
        document.getElementById("status").style.display = "none";
        create_mode = true;
        document.querySelectorAll(".player").forEach(e => e.remove(e));
    }
}

// 建立玩家介面
function createPlayersUI() {
    document.getElementById('players').innerHTML = '';
    for (var i = 0; i < players.length; i++) {
        var div_player = document.createElement('div');
        var div_playerid = document.createElement('div');
        var div_hand = document.createElement('div');
        var div_points = document.createElement('div');
        var div_chip = document.createElement('div');

        div_points.className = 'points';
        div_points.id = 'points_' + i;
        div_player.id = 'player_' + i;
        div_player.className = 'player';
        div_hand.id = 'hand_' + i;
        div_chip.id = 'chip_' + i;

        div_playerid.innerHTML = players[i].ID;
        div_player.appendChild(div_playerid);
        div_player.appendChild(div_hand);
        div_player.appendChild(div_points);
        div_player.appendChild(div_chip);
        document.getElementById('players').appendChild(div_player);
    }
}
//拎玩家人數
function playerNumber() {
    let nums = document.getElementById("numberOfPlayer")
    let playerNum = nums.options[nums.selectedIndex].text
    return playerNum
}
//檢查下注
function check_chip() {
    console.log(players.length)
    let i=players.length-1
    if(players[i].Chip===0){
        alert("limit bit $100,000")
        return false
    }else{
        return true
    }
    // for (let i = players.length - 1; i > 0; i--) {
    //     if (players[i].Chip === 0) {
    //         console.log("limit bit $10,000")
    //     } else if (players[i].Chip >= 10) {
    //         return true
    //     }
    // }
}


// 開波
function startblackjack() {
    if (ready_mode === false) {
        alert("plase ready or reset")
    }
    else {
        const confirm = check_chip()
        if (!confirm === false) {
            currentPlayer = 0;
            let deck = createDeck();
            shuffle(deck);
            dealHands();
            updatePoints();
            document.getElementById('player_' + currentPlayer).classList.add('active');
            ready_mode = false;
        }
    }

}

// 一開波每人抽2張牌
function dealHands() {
    // alternate handing cards to each player
    // 2 cards each
    for (var i = 0; i < 2; i++) {
        for (var x = 0; x < players.length; x++) {
            var card = deck.pop();
            players[x].Hand.push(card);
            renderCard(card, x);
            // updatePoints();
        }
    }
    updateDeck();
}

// 根據玩家加入手牌
function renderCard(card, player) {
    var hand = document.getElementById('hand_' + player);
    hand.appendChild(getCardUI(card));
}

// 於html展示卡牌
function getCardUI(card) {
    var el = document.createElement('div');
    var icon = '';
    if (card.Suit == 'Hearts')
        icon = '&hearts;';
    else if (card.Suit == 'Spades')
        icon = '&spades;';
    else if (card.Suit == 'Diamonds')
        icon = '&diams;';
    else
        icon = '&clubs;';

    el.className = 'card';
    // el.innerHTML = card.Ranks + '<br/>' + icon;

    // display_card_function by Edwin
    const flower = card.Suit
    const number = card.Ranks
    const show_card = { flower, number }
    console.log(show_card)
    const display_cards = display_playcards(show_card)
    el.innerHTML = `<img class="cards" src="./playing_card/${display_cards}.png" alt="7-head"></img>`



    // display_card_function by Edwin
    return el;
}

function display_playcards(show_card){
    let card = ''
    if (show_card.flower == 'Hearts')
    card += 'H';
    else if (show_card.flower == 'Spades')
    card += 'S';
    else if (show_card.flower == 'Diamonds')
    card += 'D';
    else
    card += 'C';
    card += show_card.number
    console.log(card)
    return card
}


// 更新deck剩下的卡數量
function updateDeck() {
    document.getElementById('deckcount').innerHTML = deck.length;
}


// 抽牌
function hit() {
    if(hit_mode){
    var card = deck.pop();
    players[currentPlayer].Hand.push(card);
    renderCard(card, currentPlayer);
    updatePoints();
    updateDeck();
    check()
    }
}
// 拎分
function getPoints(player) {
    var points = 0;
    for (var i = 0; i < players[player].Hand.length; i++) {
        points += players[player].Hand[i].Values;
    }
    players[player].Points = points;
    return points;
}

//更新分數 
function updatePoints() {
    for (var i = 0; i < players.length; i++) {
        getPoints(i);
        document.getElementById('points_' + i).innerHTML = players[i].Points;
    }
}
// 計是否爆分
function check() {
    if (players[currentPlayer].Points > 21 && (players[currentPlayer]).ID === players.length) {
        alert("game over")
        hit_mode=false
    }
    else if (players[currentPlayer].Points === 21 && (players[currentPlayer]).ID === players.length) {
        alert("game over")
        hit_mode=false
    }
    else if (players[currentPlayer].Points > 21) {
        document.getElementById('status').innerHTML = 'Player: ' + (players[currentPlayer]).ID + ' LOST';
        document.getElementById('status').style.display = "inline-block";
        end()
    }
    else if (players[currentPlayer].Points === 21 || players[currentPlayer].hand.length===4) {
        document.getElementById('status').innerHTML = 'Player: ' + (players[currentPlayer]).ID + ' WIN';
        document.getElementById('status').style.display = "inline-block";
        end()
    }
}

//html插入注碼
function coin_inner() {
    let nums = document.getElementById("chip_num").value
    chip = chip + parseInt(nums)
    players[currentPlayer].Chip = chip;
    document.getElementById('chip_' + currentPlayer).innerHTML = "$" + players[currentPlayer].Chip + "萬";
    document.getElementById('chip_' + currentPlayer).style.display = "inline-block";
}

// 是否pass，跟住換人
function stay() {
    chip = 0
    if((players[currentPlayer]).Chip===0){
        alert("limit bit $10,000")
    }
    // move on to next player, if any
    else if (currentPlayer != players.length - 1) {
        document.getElementById('player_' + currentPlayer).classList.remove('active');
        currentPlayer += 1;
        document.getElementById('player_' + currentPlayer).classList.add('active');
    }
}

//每回合end
function end() {
    stay()
}



//顯示投注金額
function updateTextInput(val) {
    document.getElementById('chip_bit').value = val;
}