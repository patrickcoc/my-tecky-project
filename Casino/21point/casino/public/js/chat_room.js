const socket = io.connect()

let socket_id = ""

socket.on("connect",async()=>{
    socket_id = socket.id
    console.log(socket_id)
    await fetch(`/socketID?socket_ID=${socket_id}`,{
        method: "POST"
    })
})

const chatroom_testing = document.querySelector('#testing_chatroom')

chatroom_entering_text = document.querySelector('.chat_texting_box')

chatroom_entering_text.addEventListener('submit',(event)=>{
    event.preventDefault()
    let text_content = chatroom_entering_text['entering_text'].value
    chatroom_entering_text['entering_text'].value = ''
    const text_content_items = { text_content }
    sending_text(text_content_items)
})

async function sending_text(text_content_items){
    const res = await fetch('/chatroom',{
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body:JSON.stringify(text_content_items),
    })
}

socket.on('chatroom_text_box',(msg)=>{
    add_text_into_chat_box(msg)
})

const chat_content = document.querySelector('.chat_content')

function add_text_into_chat_box(msg){
    chat_content.innerHTML += `<div>${msg[0]}：${msg[1]}</div>`
}