// Create an instance of the Stripe object with your publishable API key
let stripe = Stripe("pk_test_51JCR6CBzGZ0mNTprYWhrVFJLeFuyOlCSxIlLDIBHQLF63kA6VPPbxJl1v6NtniKCfeEM2pNYpOJx7esgfqRJGttZ00UK9RBIK7");
let checkoutButton = document.querySelector('#purchase_button');

checkoutButton.addEventListener("click", async function () {
    try {
        await fetch("/payment", {
            method: "POST",
            headers: {
                "Content-Type": "application/json; charset=utf-8"
            },
            body: JSON.stringify({
                "product": {
                    "name": "遊戲金幣: 10000",
                    "image": "https://cdn.pixabay.com/photo/2018/06/11/09/54/game-3468135_960_720.png",
                    "amount": 100,
                    "quantity": 1
                }
            })
        })
            .then(function (response) {
                return response.json();
            })
            .then(function (session) {
                return stripe.redirectToCheckout({ sessionId: session.id });
            })
            .then(function (result) {
                // If redirectToCheckout fails due to a browser or network
                // error, you should display the localized error message to your
                // customer using error.message.
                if (result.error) {
                    alert(result.error.message);
                }
            })
            .catch(function (error) {
                console.error("Error:", error);
            });
    } catch (err) {
        console.log(err.message)
    }

});