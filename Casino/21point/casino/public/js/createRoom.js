main();

async function main() {
    await show_room();
}

// open room button
const nav_open_room_btn = document.querySelector('.nav_open_room_btn')
const open_room_form_container = document.querySelector('.open_room_form_container')
nav_open_room_btn.addEventListener('click', async function () {
    const res = await fetch('/check_login')
    if (res.status === 200){
        if (open_room_form_container.classList.contains('hide')) {
            open_room_form_container.classList.remove('hide')
            open_room_form_container.classList.add('show')
        }
    
        if(login_form_container.classList.contains('show')){
            login_form_container.classList.remove('show')
            login_form_container.classList.add('hide')
        }
    
        if (register_form_container.classList.contains('show')) {
            register_form_container.classList.remove('show')
            register_form_container.classList.add('hide')
        }
        
    } else if( res.status === 400){
        please_login_mesaage()
    }
    

    console.log('clicked')
})




const open_room_btn = document.querySelector('.open_room_btn')
// const open_room_form_container = document.querySelector('.open_room_form_container')
open_room_btn.addEventListener('click', async function () {
    const res = await fetch('/check_login')
    if (res.status === 200){
        if (open_room_form_container.classList.contains('hide')) {
            open_room_form_container.classList.remove('hide')
            open_room_form_container.classList.add('show')
        }
    
        if(login_form_container.classList.contains('show')){
            login_form_container.classList.remove('show')
            login_form_container.classList.add('hide')
        }
    
        if (register_form_container.classList.contains('show')) {
            register_form_container.classList.remove('show')
            register_form_container.classList.add('hide')
        }
        
    } else if( res.status === 400){
        please_login_mesaage()
    }
})

//close (open room form) button
const close_open_room_form = document.querySelector('.close_open_room_form')
close_open_room_form.addEventListener("click", function () {
    if (open_room_form_container.classList.contains('show')) {
        open_room_form_container.classList.remove('show')
        open_room_form_container.classList.add('hide')
    }
})


//Submit Open Room
const create_room_form = document.querySelector('#create_room_form')

create_room_form.addEventListener('submit', async function (e) {
    e.preventDefault()

    const room_name = create_room_form['room_name'].value
    const room_password = create_room_form['room_password'].value
    const create_room_object = { room_name, room_password }
    console.log(`[room_name]:${room_name}`)
    console.log(create_room_object)
    console.log(`[create_room_object]:${create_room_object.room_name, create_room_object.room_password}`)

    const res = await fetch('/create_room', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(create_room_object),
    })

    const data = await res.json()
    // console.log(data.room_id)
    const create_room_form_textarea = document.querySelector('.room_name_textarea')

    create_room_form_textarea.value = ""
    // create_room_form_textarea.focus()

    console.log(res.status)
    console.log('create_room_form function')
    if (res.status === 200) {
        open_room_form_container.classList.remove('show')
        open_room_form_container.classList.add('hide')

        console.log("create room success")
        window.location.href = `/blackjack.html?id=${data.room_id}`
        console.log("end")

    } else if (res.status === 500 || res.status === 400) {
        please_login_mesaage()

    }
})

socket.on(`room_id`, (room_id) => {
    console.log(`room: ${room_id} created`)
    show_latest_room()
})



function please_login_mesaage() {
    const please_login_info = document.querySelector('.please_login_info')
    open_room_form_container.classList.remove('show')
    open_room_form_container.classList.add('hide')
    login_form_container.classList.remove('hide')
    login_form_container.classList.add('show')
    please_login_info.innerText = "*請先登入以使用更多功能*"
    setTimeout(() => {
        please_login_info.innerText = ""
    }, 5000)
}


async function show_latest_room() {
    const res = await fetch('/show_latest_room')
    const room_data = await res.json()

    const htmlStr = ``
    for (const data of room_data.data) {
        // console.log(data)
        const room_info = document.createElement('div')
        room_info.classList.add('room_info')
        room_info.classList.add('room_all_title_container')
        // room_info.innerHTML += `
        // <div class="opened_room_id opened_room_title_style">${data.id}</div>
        // <div class="opened_room_name opened_room_title_style">${data.room_name}</div>
        // <div class="opened_room_amount opened_room_title_style">1/4</div>
        // <div class="opened_join_room opened_room_title_style">

        //         <div class="join_room_btn">加入</div> 

        // </div>`
        room_info.innerHTML += `
        <div class="opened_room_id opened_room_title_style">${data.id}</div>
        <div class="opened_room_name opened_room_title_style">${data.room_name}</div>
        <div class="opened_room_amount opened_room_title_style">1/4</div>
        <div class="opened_join_room opened_room_title_style">
            <a href="/blackjack.html?id=${data.id}" class="join_btn_style"> 
                <div class="join_room_btn">加入</div> 
            </a>
        </div>`
        const room_info_container = document.querySelector('.room_info_container')
        const room_info_created = document.querySelector('.room_info')

        const first_room = document.querySelectorAll('.room_info')[0]
        if (room_info_created === null) {
            room_info_container.appendChild(room_info)
            // document.querySelector('.no_room_notice').remove();
        } else {
            // console.log(first_room)
            // console.log(room_info_created.parentNode)
            // document.querySelector('.no_room_notice').remove();
            room_info_created.parentNode.insertBefore(room_info, first_room)

        }
        join_room_btn_listener();
    }

    // document.querySelector('.room_info_container').appendChild(room_info)

    // console.log(room_data)
}



const room_background = document.querySelector('.room_background')
//Show all opened room in lobby
async function show_room() {
    const res = await fetch('/show_room')
    const room_data = await res.json()
    // console.log(typeof (room_data))
    // console.log(room_data.data)

    // if (res.status === 200) {
    for (const display_room of room_data.data) {

        const room_info = document.createElement('div')
        room_info.classList.add('room_info')
        room_info.classList.add('room_all_title_container')
        // room_info.innerHTML += `
        // <div class="opened_room_id opened_room_title_style">${display_room.id}</div>
        // <div class="opened_room_name opened_room_title_style">${display_room.room_name}</div>
        // <div class="opened_room_amount opened_room_title_style">1/4</div>
        // <div class="opened_join_room opened_room_title_style">

        //     <div class="join_room_btn">加入</div>

        // </div>`
        room_info.innerHTML += `
        <div class="opened_room_id opened_room_title_style">${display_room.id}</div>
        <div class="opened_room_name opened_room_title_style">${display_room.room_name}</div>
        <div class="opened_room_amount opened_room_title_style">1/4</div>
        <div class="opened_join_room opened_room_title_style">
        <a href="/blackjack.html?id=${display_room.id}" class="join_btn_style"> 
            <div class="join_room_btn">加入</div>
            </a>
        </div>`
        document.querySelector('.room_info_container').appendChild(room_info)


    }

    join_room_btn_listener();
    // }
}


function join_room_btn_listener() {
    const join_room_btn = document.querySelectorAll('.join_room_btn')
    for (let i = 0; i < join_room_btn.length; i++) {
        join_room_btn[i].addEventListener('click', async function (e) {
            
            const target_room_id = e.target.parentNode.parentNode.parentNode.children[0].innerText
            
            const res = await fetch(`/join_room`, {
                method: "POST",
                headers: {
                    "Content-Type": "application/json; charset=utf-8"
                },
                body: JSON.stringify({ target_room_id }),
            })
        })
    }

}



// function no_room_message() {
//     const room_info_container = document.querySelector('.room_info_container')
//     if (document.querySelector('.room_info') === null) {
//         const no_room = document.createElement('div')
//         no_room.classList.add("no_room_notice")
//         no_room.innerText = "暫沒有房間"
//         room_info_container.appendChild(no_room)
//     }
// }
// no_room_message();
// const opened_room_container = document.querySelectorAll('.room_info')
// //雙數行 灰色
// for (let i = 0; i < opened_room_container.length; i++) {
//     if (i % 2 !== 0) {
//         opened_room_container[i].style.backgroundColor =
//             'rgba(128, 128, 128, 0.966)'
//     }
// }

const quick_start_a_link = document.querySelector('.quick_start_a_link')

quick_start_a_link.addEventListener('click', async function () {
    const res = await fetch('/check_login')
    if (res.status === 200){
        window.location = `/member.html`
        
    } else if( res.status === 400){
        please_login_mesaage()
    }
    
})

const game_shop = document.querySelector('.game_shop')

game_shop.addEventListener('click', async function() {
    const res = await fetch('/check_login')
    if (res.status === 200){
        window.location = `/newshop.html`
        
    } else if( res.status === 400){
        please_login_mesaage()
    }

})

