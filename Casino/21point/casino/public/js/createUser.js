//Submit Register Form Handler
const register_info = document.querySelector('.register_info')

register_form.addEventListener('submit', async function (e) {
    e.preventDefault()

    const email = register_form['email'].value
    const nickname = register_form['nickname'].value
    const username = register_form['username'].value
    const password = register_form['password'].value
    const registerFormObject = { 
        email,
        nickname,
        username, 
        password 
    }

    const res = await fetch('/register', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(registerFormObject),
    })
    // console.log(res.status)

    if (res.status === 200) {
        
        await fetch(`/socketID?socket_ID=${socket_id}`, {
            method: "POST"
        })
        login_button.remove()
        setTimeout(() => {
            register_info.style.top = `67px`
            register_info.style.left = `163px`
            register_info.style.color = `blue`
            register_info.innerText = "*註冊成功"
            register_form_container.remove('show');
        }, 3000)
        
    

        create_logout()
        create_lobby_member_info()

        // login_form_container.add('hide')
        // window.location="/member.html"
        console.log("login success")
    } else if (res.status === 400) {
        register_info.innerText = "*電郵地址/用戶名稱已被使用"
        setTimeout(() => {
            register_info.innerText = ""
        }, 5000)
        console.log("login fail message")
    } else {
        console.log("login failed")
    }
})