

window.onload = async () => {
    getPlayer()
    socket.emit("receive")

    let dealer = { name: 'dealer', hand: [] };
    let users = [];
    let players = [];
    let cheatCard = { Value: 21, img: `image/exodia.jpeg` }
}

const socket = io.connect()

let socket_id = ""

// Player_chat
socket.on('chatroom_text_box', function (message) {
    document.querySelector('#chat-messages').innerHTML += `<div>${message[0]}：${message[1]}</div>`
});

socket.on('reset board', function () {
    resetBoard();
});


function findById(array, id) {
    for (let i = 0; i < array.length; i++) {
        if (array[i]["id"] == id) {
            return i;
        } else {
            console.log("cant find index")
            return undefined
        }
    }
}
socket.on('welcome', function (data) {
    console.log("weclome socket", data)
    users = data.users;
    players = data.currentPlayers;
    dealer.hand = data.dealerHand;
    document.getElementById('message').innerHTML = `<p>${data.greeting}!</p>`;
    setTimeout(function () {
        document.querySelector('#message p').remove()
    }, 750);

});

socket.on('new player', function (data) {
    index = data.newPlayer.length - 1
    players.push(data.newPlayer[index])
    console.log('newplay', data, players)
    assignNewPlayer(data.newPlayer);
   
});

socket.on('new user info', function (player_info) {
    console.log("new user info", player_info.info)
    socket.emit('new user', { nickname: player_info.nickname });
});


socket.on('new user', function (message) {
    document.querySelector('#chat-messages').innerHTML += `<p>${message}</p>`
});

socket.on('whose turn', function (data) {
    if (data.player.id === socket.id) {
        document.querySelector('#btnHit').disabled = false;
        document.querySelector('#btnStand').disabled = false;
        document.querySelector('#message').innerHTML = `<p>${data.player.nickname} turn!</p>`
        setTimeout(function () {
            document.querySelector('#message p').innerHTML = ''
        }, 5000)
        playerTurn(socket);
    } else if (data.player.id !== socket.id) {
        document.querySelector('#btnHit').disabled = true;
        document.querySelector('#btnStand').disabled = true;
        console.log(data.player, socket.id, "next player")
    }
});

socket.on('turn over', function (player) {
    console.log('turn over', player)
    let hitButton = document.querySelector('#btnHit');
    let standButton = document.querySelector('#btnStand');
    hitButton.disabled = true;
    standButton.disabled = true;

    if (player.total > 21) {
        console.log("busted21")
        document.querySelector('#message').innerHTML = '<p>BUST!</p>'
        setTimeout(function () {
            document.querySelector('#message p').innerHTML = ''
        }, 3000)
    } else if (player.total === 21 && player.hand.length === 2) {
        console.log("=21")
        player.money += (player.bet);
        document.querySelector('#message').innerHTML = `<p>BLACKJACK! YOU WIN $${player.bet * 1.5}!</p>`;
        setTimeout(function () {
            document.querySelector('#message p').innerHTML = ''
        }, 5000);
    }
});
function addSit() {
    let sitButton = document.getElementById('btnSit')
    socket.emit('deal me in', users);
    document.querySelector('#submitBet').disabled = false
    document.querySelector(".btnlist").classList.remove("hidden")
    sitButton.disabled = true;
    console.log("inside sit", users)
    placeBet();
    document.querySelector('#btnSit').removeEventListener('click', addSit);
}
socket.on('sit invite', function () {
    let sitButton = document.getElementById('btnSit')
    console.log("open btnsit")
    // if (users == undefined) {
    //     console.log("users is undefind")
    //     return
    // }
    // else if (users) {
    sitButton.addEventListener('click', addSit);
    // }
});

socket.on('sit uninvite', function () {
    document.querySelector('#btnSit').addClass('hidden');
});


socket.on('fill me in', function (data) {
    console.log(`fill me in data:${data}`)
    players = data.currentPlayers;
    dealer.hand = data.dealerHand;
    dealCards(data.currentPlayers, data.dealerHand);
});

socket.on('deal cards', function (data) {
    console.log("dealU", data.players)

    dealCards(data.players, data.dealer);
});

socket.on('new card', function (data) {
    hitMe(data.player, data.card, data.total, data.item);
});

socket.on('player bet', function (data) {
    console.log('other player bet', data)
    document.querySelector(`#playerMoney_${data.otherPlayer.nickname}`).innerHTML = `<p>$${centify(data.otherPlayer.balance)}</p>`
    document.querySelector(`.betFor_${data.otherPlayer.nickname}`).innerHTML = `<p>bet:$${data.otherPlayer.bet}</p>`
});

//投注
function setBet(betAmount) {
    let messageBox = document.querySelector('#message');

    console.log("setbet", betAmount, players)
    players.forEach((player) => {
        if (socket.id === player.id) {
            console.log("ddddd", players, player)
            parseInt(betAmount)
            if (betAmount > 0 && betAmount <= player.money) {
                player.bet = betAmount;
                player.money -= player.bet;
                console.log("bet your mother", player)
                document.querySelector(`#playerMoney_${player.nickname}`).innerHTML = `<p>$${centify(player.money)}</p>`
                document.querySelector(`.betFor_${player.nickname}`).innerHTML = `<p>bet:$${player.bet}</p>`
                console.log("placebet", players)
                socket.emit('place bet', { nickname: player.nickname, bet: player.bet });
            }
        }
    });
    // } else {
    //     let $messageText = $('<p>');
    //     $messageText.text('Not a valid bet!');
    //     $messageText.css({ 'color': 'orange', 'font-size': '14px' });
    //     $messageBox.append($messageText);
    //     $messageText.delay(1000).fadeOut('fast');
    //     setTimeout(function () {
    //         $messageText.remove();
    //     }, 2000);
    // };
};


//更新money
socket.on('update money', function (data) {
    console.log('update$$', data)
    socket.emit('update users', data)
    data.players.forEach((otherPlayer) => {
        if (otherPlayer.id !== socket.id) {
            console.log('$$$$$$$', otherPlayer)
            document.querySelector(`#playerMoney_${otherPlayer.nickname}`).innerHTML = `<p>$${otherPlayer.balance}</p>`
            document.querySelector(`.betFor_${otherPlayer.nickname}`).innerHTML = `$0`
            socket.emit('update database', otherPlayer);
        }
        else {
            console.log('$88888888')
            socket.emit('update database', otherPlayer);
        }
    })
});

function hitMe(playerhit, card, total, item) {
    console.log("hitme", playerhit, card, total, item)
    if (playerhit.id === socket.id && item) {
        document.querySelector(`.myhand_${playerhit.nickname}`).innerHTML = `<img src=${card.img}>`
        document.querySelector(`.sroce_${playerhit.nickname}`).innerHTML = `<div>sroce:${total}</div>`;
    }
    else if (playerhit.id !== socket.id && item) {
        document.querySelector(`.myhand_${playerhit.nickname}`).innerHTML = `<img src=${card.img}>`
        document.querySelector(`.sroce_${playerhit.nickname}`).innerHTML = `<div>sroce:${total}</div>`;
    }
    else if (playerhit.id === socket.id) {
        document.querySelector(`.myhand_${playerhit.nickname}`).innerHTML += `<img src=${card.img}>`
        document.querySelector(`.sroce_${playerhit.nickname}`).innerHTML = `<div>sroce:${total}</div>`;
    } else {
        document.querySelector(`.myhand_${playerhit.nickname}`).innerHTML += `<img src=${card.img}>`
        document.querySelector(`.sroce_${playerhit.nickname}`).innerHTML = `<div>sroce:${total}</div>`;
        console.log("inner other player card")
    };
}
function resetHitBtn() {
    socket.emit('hit');
}
function cheat() {
    socket.emit('cheat')
    console.log('go cheat')
    document.querySelector('#btnCheat').removeEventListener('click', cheat)
}
function playerTurn() {
    let hitButton = document.querySelector('#btnHit');
    let standButton = document.querySelector('#btnStand');

    function addStand() {
        stand();
        standButton.removeEventListener('click', addStand)
    };
    hitButton.addEventListener('click', resetHitBtn);
    standButton.addEventListener('click', addStand);
}

function stand() {
    socket.emit('stand');
    document.querySelector('#btnHit').disabled = true;
    document.querySelector('#btnStand').disabled = true;
}


function centify(amount) {
    if (amount % 1) {
        return amount.toFixed(2);
    } else {
        return amount;
    }
};
function addBet() {
    document.querySelector('#submitBet').disabled = true
    let bet = document.querySelector('#inputBet').value
    setBet(parseInt(bet));
};
function placeBet() {
    let submitBet = document.querySelector('#submitBet');
    let messageBox = document.querySelector('#message');
    console.log("hihi", players)
    messageBox.innerHTML = ``;
    function handleSubmitBet() {
        submitBet.removeEventListener('click', handleSubmitBet);
        addBet()
    }
    submitBet.addEventListener('click', handleSubmitBet);
};

function assignNewPlayer(newPlayer) {
    index = newPlayer.length - 1
    if (newPlayer[index].id == socket.id) {
        if (newPlayer[index].player_id === 13) {
            console.log('123')
            document.querySelector('#cheatBtn').innerHTML = `<div id='btnCheat'><img id='cheat' src="image/yugioh1.png"></div>`
            document.querySelector('#btnCheat').addEventListener('click', cheat)
        }
    }
    if (newPlayer[index].id == socket.id) {
        console.log("print Player Status", newPlayer)
        index = newPlayer.length - 1
        let playerStatus =
            `<div class=playerinfo ${newPlayer[index].nickname}>
            <div class="status_${newPlayer[index].id}">
            <div id="playerName_${newPlayer[index].id}">${newPlayer[index].nickname}</div>
            <img id=playericon_${newPlayer[index].id} src="image/7_head.jpg">
            <div id="playerMoney_${newPlayer[index].nickname}" >$${newPlayer[index].money}</div>
            </div>
            <div class="myhand_${newPlayer[index].nickname}"></div>
            <div class="sroce_${newPlayer[index].nickname}"></div>
            <div class="betFor_${newPlayer[index].nickname}"></div>
            </div>`
        const player_position = document.querySelector('.player')
        player_position.innerHTML += playerStatus
    } else if (newPlayer[index].id !== socket.id) {
        console.log("print Player Status", newPlayer)
        index = newPlayer.length - 1
        let playerStatus =
            `<div class=playerinfo ${newPlayer[index].nickname}>
            <div class="status_${newPlayer[index].id}">
            <div id="playerName_${newPlayer[index].id}">${newPlayer[index].nickname}</div>
            <img id=playericon_${newPlayer[index].id} src="image/7_head.jpg">
            <div id="playerMoney_${newPlayer[index].nickname}" >$${newPlayer[index].money}</div>
            </div>
            <div class="myhand_${newPlayer[index].nickname}"></div>
            <div class="sroce_${newPlayer[index].nickname}"></div>
            <div class="betFor_${newPlayer[index].nickname}"></div>
            </div>`
        const player_position = document.querySelector('.player')
        player_position.innerHTML += playerStatus
    }
};

function dealCards(players, dealer) {
    let player = []
    players.forEach((serverPlayer) => {
        if (serverPlayer.id === socket.id) {
            player.hand = serverPlayer.hand
            player.total = serverPlayer.total;
            player.displayTotal = serverPlayer.displayTotal;
            player.hand.forEach((card) => {
                document.querySelector(`.myhand_${serverPlayer.nickname}`).innerHTML += `<img src=${card.img}>`
            });
            document.querySelector(`.sroce_${serverPlayer.nickname}`).innerHTML = `<div id="${serverPlayer.nickname}_sroce" >sroce:${player.displayTotal}</div>`;
        } else {
            serverPlayer.hand.forEach((card) => {
                document.querySelector(`.myhand_${serverPlayer.nickname}`).innerHTML += `<img src=${card.img}>`
                document.querySelector(`.sroce_${serverPlayer.nickname}`).innerHTML = `<div id="${serverPlayer.nickname}_sroce" >sroce:${serverPlayer.displayTotal}</div>`;
            });

        }
    })
    dealer.forEach((dealerCard) => {
        document.querySelector('.dealerCard').innerHTML += `<img src=${dealerCard.img}>`
    });
    document.querySelector("#submitBet").disabled = true
    socket.emit('player ready');
}

socket.on('gameover', function (data) {
    console.log("now over", data)
    endGame(data.player, data.dealerHand, data.dealerTotal, data.status, data.message, data.players);
});


function endGame(player, dealerHand, dealerTotal, winStatus, message, players) {
    document.querySelector('#btnHit').removeEventListener('click', resetHitBtn);
    console.log("dealer end", dealerHand, dealerTotal, status, "msm", message, player, players)
    document.querySelector('.dealerCard').innerHTML = ""
    dealerHand.forEach((dealerCard) => {
        document.querySelector('.dealerCard').innerHTML += `<img src=${dealerCard.img}>`
        document.querySelector('.dealer_total').innerHTML = `<div class="dealer_sroce">sroce:${dealerTotal}</div>`
    });

    setTimeout(function () {
        if (winStatus) {
            console.log("win end", players, users, player)

            if (winStatus === 'win') {
                if (player.total === 21 && player.hand.length === 2) {
                    player.balance = parseInt(player.balance);
                } else {
                    // player.balance += parseInt(player.bet * 2);
                    document.querySelector(`#playerMoney_${player.nickname}`).innerHTML = `<p>$${player.balance}</p>`
                    document.querySelector(`.betFor_${player.nickname}`).innerHTML = `$0`

                }
            } else if (winStatus === 'push') {
                player.balance += parseInt(player.bet);

                document.querySelector(`#playerMoney_${player.nickname}`).innerHTML = `<p>$${player.balance}</p>`
                document.querySelector(`.betFor_${player.nickname}`).innerHTML = `<p>$0</p>`
            } else if (winStatus === 'lose') {
                document.querySelector(`#playerMoney_${player.nickname}`).innerHTML = `<p>$${player.balance}</p>`
                document.querySelector(`.betFor_${player.nickname}`).innerHTML = `<p>$0</p>`
            }
            users.forEach((user) => {
                players.forEach((player) => {
                    if (user.nickname === player.nickname) {
                        user.money = player.balance
                    }
                })
            })
            document.querySelector('#btnHit').disabled = true;
            document.querySelector('#btnStand').disabled = true;
            document.querySelector('#btnQuit').disabled = false;
            document.querySelector('#btnReset').disabled = false;
            function resetQuitBtn() {
                console.log('quit', players)
                leaveGame()
                document.querySelector('#btnQuit').disabled = true;
                document.querySelector('#btnQuit').removeEventListener('click', resetQuitBtn)

            }
            document.querySelector('#btnReset').addEventListener('click', resetGame)
            document.querySelector('#btnQuit').addEventListener('click', resetQuitBtn)
            console.log("last end", players, users, player)
        }
        else {
            // for everyone who's not a player, give them the option to join as a player...

            // if($('.sit-button').length === 0 && !$('.primary').attr('id')) {
            //   let $sitButton = $('<button>', {id: 'sit-button', style: 'align-self: flex-start; margin-top: 10px;'}).text('SIT');

            //   $('.primary').append($sitButton);
            //   $('.sit-button').on('click', function() {
            //     socket.emit('deal me in', {name: player.name, money: player.money});

            //     $('.hand').children().remove();
            //     $('#message').text(' ');
            //     $('#player-box').addClass('hidden');
            //     $('#dealer-box').addClass('hidden');
            //     $('#money-box').children().removeClass('hidden');
            //     $('.primary').attr({'id': 'player-hand'});

            //     $sitButton.remove();

            //     placeBet();
            //   })
            // }

        }

        // if (message) {
        //     if (typeof message === 'object') {
        //         message = message.filter((msg) => {
        //             return msg !== '';
        //         }).join('<br/>');
        //     }
        //     let messageBox = document.querySelector('#message')
        //     let message = `<p>${message}</p>`
        //     messageBox.innerHTML = message
        //     setTimeout(function () {
        //         messageBox.innerHTML = ''
        //     }, 2000);
        // }
    }, 2000);


};



function updateInputBet(val) {
    document.getElementById('chip_bet').value = val;
}

async function getPlayer() {
    const res = await fetch('gameplay')
    if (res.status === 200) {
    } else {
    }
}

function resetBoard() {
    document.getElementById('btnSit').disabled = false
    document.querySelector('#submitBet').disabled = true
    document.querySelectorAll('.playerinfo').forEach(e => e.parentNode.removeChild(e));
    document.querySelector(".btnlist").className = "btnlist hidden"
    document.querySelector('.dealerCard').innerHTML = ''
    document.querySelector('.dealer_total').innerHTML = ''

}
function leaveGame() {
    console.log(players, "leaveplay", users)
    socket.emit('leave game');
    let myIndex = findById(players, socket.id);
    players.splice(myIndex, 1);
    players = [];
    dealer = [];

}
function resetGame() {
    resetBoard();
    document.querySelector('#btnReset').disabled = true;
    document.querySelector('#btnReset').removeEventListener('click', resetGame)
    // placeBet(players);
    leaveGame();
}

