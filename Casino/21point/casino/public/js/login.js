const socket = io.connect()

let socket_id = ""

socket.on("connect", async () => {
    socket_id = socket.id
    console.log(socket_id)
    await fetch(`/socketID?socket_ID=${socket_id}`, {
        method: "POST"
    })
    // display_all_online_players()
})

socket.on("show_online_user", async () => {
    const online_user_container = document.querySelector('.online_user_container')
    online_user_container.innerHTML = 
        ``
    display_all_online_players()
    console.log("received")
})

socket.on("update_show_user", () => {
    display_all_online_players()
})


//login form selector
const login_button = document.querySelector('.login_button')
const close_login_button = document.querySelector('.close_login_form')
const login_form_container = document.querySelector('.login_form_container')
const login_form = document.querySelector('#login_form')

//Register form selector
const close_register_form = document.querySelector('.close_register_form')
const register_form_button = document.querySelector('.register_form_button')
const register_form_container = document.querySelector('.register_form_container')
const register_form = document.querySelector('#register_form')
const back_to_login_form_button = document.querySelector('.back_to_login_form_button')

//返回登入
back_to_login_form_button.addEventListener('click', function () {
    register_form_container.classList.remove('show')
    register_form_container.classList.add('hide')

    login_form_container.classList.remove('hide')
    login_form_container.classList.add('show')
})

//Open Register Form Container & Hide Login Container
register_form_button.addEventListener('click', function () {
    login_form_container.classList.remove('show')
    login_form_container.classList.add('hide')
    register_form_container.classList.remove('hide')
    register_form_container.classList.add('show')

    if (open_room_form_container.classList.contains('show')) {
        open_room_form_container.classList.remove('show')
        open_room_form_container.classList.add('hide')
    }
})

//Close Register Form Container
close_register_form.addEventListener('click', function () {
    if (register_form_container.classList.contains('show')) {
        register_form_container.classList.remove('show')
        register_form_container.classList.add('hide')
    }
})


//Open Login Form Container  
login_button.addEventListener("click", function () {
    if (login_form_container.classList.contains('hide')) {
        login_form_container.classList.remove('hide')
        login_form_container.classList.add('show')
    } else if (login_form_container.classList.contains('show')) {
        login_form_container.classList.remove('show')
        login_form_container.classList.add('hide')
    }

    if (open_room_form_container.classList.contains('show')) {
        open_room_form_container.classList.remove('show')
        open_room_form_container.classList.add('hide')
    }

    if (register_form_container.classList.contains('show')) {
        register_form_container.classList.remove('show')
        register_form_container.classList.add('hide')
    }



})
//Close Button of Login Form Container
close_login_button.addEventListener("click", function () {
    if (login_form_container.classList.contains('show')) {
        login_form_container.classList.remove('show')
        login_form_container.classList.add('hide')
    }
})

//Close Add Balance Form Container
const close_checkout_button = document.querySelector('.close_add_balance_form')
const checkout_container = document.querySelector('.checkout_container')
close_checkout_button.addEventListener("click", function () {
    if (checkout_container.classList.contains('show')) {
        checkout_container.classList.remove('show')
        checkout_container.classList.add('hide')
    }
})

const vid = document.querySelector('#bg_music')
vid.volume = 0.1;

const mute = document.querySelector('#music_mute')
mute.addEventListener('click', function(){
    vid.muted = !vid.muted;
})

window.addEventListener('click', function(){
    document.querySelector('#bg_music').play()
})


//Submit Login Form Handler
const wrong_info = document.querySelector('.wrong_info')

login_form.addEventListener('submit', async function (e) {
    e.preventDefault()

    const username = login_form['username'].value
    const password = login_form['password'].value
    const loginFormObject = { username, password }

    const res = await fetch('/login', {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: JSON.stringify(loginFormObject),
    })
    // console.log(res.status)

    if (res.status === 200) {
        // login_button.classList.remove('show')
        // login_button.classList.add('hide')
        // login_form_container.remove('show')
        await fetch(`/socketID?socket_ID=${socket_id}`, {
            method: "POST"
        })
        login_button.remove()
        login_form_container.remove('show');


        create_logout()
        create_lobby_member_info()

        // login_form_container.add('hide')
        // window.location="/member.html"
        console.log("login success")
    } else if (res.status === 400) {
        wrong_info.innerText = "*wrong username or password"
        setTimeout(() => {
            wrong_info.innerText = ""
        }, 5000)
        console.log("login fail message")
    } else {
        console.log("login failed")
    }
})

//load logout button 
window.addEventListener('load', async function () {
    const res = await fetch("/logged_in_function", {
        method: "POST"
    })
    if (res.status === 200) {
        const res = await fetch(`/socketID?socket_ID=${socket_id}`, {
            method: "POST"
        })
        
        login_button.remove()
        create_logout()
        create_lobby_member_info()
        display_all_online_players()
    }

})

//Esc button quit container
document.body.addEventListener('keydown', function (e) {
    if (e.keyCode === 27) {
        if (open_room_form_container.classList.contains('show')) {
            open_room_form_container.classList.remove('show')
            open_room_form_container.classList.add('hide')
        } else if (login_form_container.classList.contains('show')) {
            login_form_container.classList.remove('show')
            login_form_container.classList.add('hide')
        } else if (register_form_container.classList.contains('show')) {
            register_form_container.classList.remove('show')
            register_form_container.classList.add('hide')
        }




    }
})


//create logout button
function create_logout() {
    const create_logout = document.createElement('div')
    create_logout.classList.add('logout_button')
    create_logout.innerHTML = "登出"
    document.querySelector('header').appendChild(create_logout)

    //Log out
    const logout_Button = document.querySelector('.logout_button')

    logout_Button.addEventListener('click', async function (e) {
        console.log("clicked logout")
        await fetch("/logout", {
            method: "POST"
        })
        console.log('logout success')
        // window.location.href = "localhost:8080"
        window.location = "/"
        // display_all_online_players()

    })
}

//create lobby member info
async function create_lobby_member_info() {
    const res = await fetch('/membership')
    const data = await res.json()
    const user = data.member_info[0]

    const add_balance_div = document.createElement('div')
    add_balance_div.classList.add('add_balance')
    add_balance_div.innerText = `増值`

    const lobby_member_info = document.createElement('div')
    lobby_member_info.classList.add('lobby_show_member_info')
    lobby_member_info.innerHTML = `
    <div class="welcome_back">歡迎回來 ${user.nickname}!</div>
    <div class="game_balance">遊戲金幣: <i class="fas fa-coins"></i> $${user.balance}</div>
    
    `
    lobby_member_info.appendChild(add_balance_div)

    add_balance_div.addEventListener('click', function () {
        const checkout_container = document.querySelector('.checkout_container')
        if(checkout_container.classList.contains('hide')){
            checkout_container.classList.remove('hide')
            checkout_container.classList.add('show')
        }
    })

    const left_box = document.querySelector('.left_box')
    const quick_entry_dav_bar = document.querySelector('.quick_entry_dav_bar')

    left_box.insertBefore(lobby_member_info, quick_entry_dav_bar)

    // console.log(user)
}





// const online_user_container = document.querySelector('.online_user_container')

// display_all_online_players()

// async function display_all_online_players() {
//     const res = await fetch('/all_online_user')
//     const data = await res.json()
//     console.log(data)
//     const online_players = data.online_users
//     online_user_container.innerHTML = ""
//     for (const user of online_players) {
//         online_user_container.innerHTML += `<div class="onlineuser"><i class="far fa-user-circle"></i>${user}</div>`
//     }
// }

// socket.on('online_user', (msg) => {
//     online_user_container.innerHTML = ""
//     for (const user of msg) {
//         online_user_container.innerHTML += `<div class="onlineuser"><i class="far fa-user-circle"></i>${user}</div>`
//     }
// })




//Game Lobby Clock
function showTime(){
    let date = new Date();
    let h = date.getHours(); // 0 - 23
    let m = date.getMinutes(); // 0 - 59
    let s = date.getSeconds(); // 0 - 59
    let session = "AM";
    
    if(h == 0){
        h = 12;
    }
    
    if(h > 12){
        h = h - 12;
        session = "PM";
    }
    
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    
    let time = h + ":" + m + ":" + s + " " + session;
    document.querySelector("#clock_display").innerText = time;
    document.querySelector("#clock_display").textContent = time;
    
    setTimeout(showTime, 1000);
    
}

showTime();