async function display_all_online_players() {
    const online_user_container = document.querySelector('.online_user_container')
    online_user_container.innerHTML =``
    const res = await fetch('/all_online_user')
    const show_onlines = await res.json()
 
    for (let show_online of show_onlines.online_users) {
        // console.log(show_online)
        online_user_container.innerHTML += 
        `
        <div class="onlineuser">
            <i class="far fa-user-circle"></i>
                <div class="show_online">
                    ${show_online}
                </div>
        </div>`
    }

    
}
display_all_online_players()