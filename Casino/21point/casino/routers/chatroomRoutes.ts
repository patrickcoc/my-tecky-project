import express from 'express'
import { chatroomController } from "../main"

export const chatroomRoutes = express.Router()

chatroomRoutes.post('/chatroom',chatroomController.public_chat)