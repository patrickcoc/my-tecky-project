import express from "express"

import { createRoomController } from "../main"

export const createRoomRoutes = express.Router()

createRoomRoutes.post('/create_room', createRoomController.create_room)

createRoomRoutes.get('/show_room', createRoomController.show_room)

createRoomRoutes.get('/show_latest_room', createRoomController.show_latest_room)

createRoomRoutes.post('/join_room', createRoomController.join_room)
