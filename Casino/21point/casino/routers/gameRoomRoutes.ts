import express from "express"

import { gameRoomController } from "../main"

export const gameRoomRoutes = express.Router()

gameRoomRoutes.get('/get_room_info/:id', gameRoomController.get_room_info)