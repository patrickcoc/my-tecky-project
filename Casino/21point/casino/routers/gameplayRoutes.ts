import express from 'express'
import { gameplayController }  from "../main"

export const gamePlayRoutes = express.Router()

gamePlayRoutes.get('/gameplay', gameplayController.getPlayer)

gamePlayRoutes.post('/gameplay_start', gameplayController.createPlayerRecord)

gamePlayRoutes.put('/gameplay_end', gameplayController.updataPlayerAmount)