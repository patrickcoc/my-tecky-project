import express from "express"
import { memberController } from "../main"

export const memberRoutes = express.Router()

memberRoutes.get('/membership', memberController.get_member_info)

memberRoutes.get('/membership_item', memberController.get_member_item)