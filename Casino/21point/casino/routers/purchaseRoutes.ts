import express from "express"
import { purchaseController } from "../main"

export const purchaseRoutes = express.Router()

purchaseRoutes.post('/purchasing_item', purchaseController.post)