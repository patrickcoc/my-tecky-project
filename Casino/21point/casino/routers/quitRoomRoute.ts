import express from "express"
import { quitRoomController } from "../main"


export const quitRoomRoute = express.Router()

quitRoomRoute.post('/quit_room', quitRoomController.quit_room)