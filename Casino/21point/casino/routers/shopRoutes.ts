import express from "express"
import { shopController } from "../main"


export const shopRoutes = express.Router()

// shopRoutes.get('/shop', shopController.showItem)

shopRoutes.get('/get_shop_item', shopController.get_shop_item)