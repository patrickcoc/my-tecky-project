import express from "express"
import { userController } from "../main"

export const userRoutes = express.Router()

userRoutes.post('/login', userController.login)

userRoutes.post('/logout', userController.logout)

userRoutes.post('/logged_in_function', userController.logged_in_function)

userRoutes.get('/all_online_user', userController.display_online_users)

// userRoutes.get('/user_online',userController.turn_user_to_online)

userRoutes.post('/socketID',userController.save_online_id)

userRoutes.post('/confirm_add_money', userController.add_user_balance)

userRoutes.post('/register', userController.register_user)

// change nickname
userRoutes.post('/nickname',userController.change_nickname)

userRoutes.get('/check_login',userController.check_login)

userRoutes.post('/change_name',userController.change_nickname)