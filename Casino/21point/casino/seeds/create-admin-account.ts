import { Knex } from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    try{
    await knex("user_item").delete()
    await knex("chat_room").delete()
    await knex("room").delete();
    await knex("users").delete();

    const hashedPassword = await hashPassword('12345678')

    const testingPassword = await hashPassword('1234')

    await knex("users").insert([
        {username: "admin", password: hashedPassword, nickname: "admin", email: "admin@casino.com", user_icon: "Charizard.webp", balance: 100000},
        {username: "edwin", password: hashedPassword, nickname: "Jason好靚仔", email: "edwin@casino.com", user_icon: "Charizard.webp", balance: 100000},
        {username: "patrick", password: hashedPassword, nickname: "公海賭神", email: "689@casino.com", user_icon: "高進.jpeg", balance: 30000000},
        {username: "john", password: hashedPassword, nickname: "一拳打爆你", email: "john@casino.com", user_icon: "Charizard.webp", balance: 100000},
        {username: "jay", password: hashedPassword, nickname: "大軍", email: "jay@casino.com", user_icon: "大軍.jpeg", balance: 100000},
        {username: "byebye", password: testingPassword, nickname: "byebye", email: "byebye@casino.com", user_icon: "Charizard.webp", balance: 100000}
    ])
}catch(err){
    console.log(err);
}
}
