import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('user_item').del()
    await knex("game_shop_items").del();

    // Inserts seed entries
    await knex("game_shop_items").insert([
        { item_name: "改名卷" , cost: 10000 , game_coin_id : 1 , item_icon: "change_name.jpg" },
        // { item_name: "PePe" , cost: "200" , game_coin_id : 1 , item_icon: "pepe.webp"},
         { item_name: "黑暗遊戲" , cost: 999999999 , game_coin_id : 1 , item_icon: "goodgame.jpeg"},
         { item_name: "遊戲王卡背" , cost: 999999999 , game_coin_id : 1 , item_icon: "遊戲王.gif"},
         { item_name: "手牌抹殺" , cost: 999999999 , game_coin_id : 1 , item_icon: "手牌抹殺.png"},
    ]);
};
