import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    // Deletes ALL existing entries
    await knex('user_item').del()

    // let users: Array<{ id:number }> = await knex.select(“id”).from(“users”)
    // userID = users[0].id

    let userID = await knex("users")
        .select('*')
        .from('users')
        .where('username', '=', 'patrick')
        .first()
    console.log('sadsad', userID.id)
    // Inserts seed entries
    let itemID = await knex("game_shop_items")
        .select('*')
        .from('game_shop_items')
        .where('item_name', '=', '黑暗遊戲')
        .first()
    let itemID2 = await knex("game_shop_items")
        .select('*')
        .from('game_shop_items')
        .where('item_name', '=', '遊戲王卡背')
        .first()
        let itemID3 = await knex("game_shop_items")
        .select('*')
        .from('game_shop_items')
        .where('item_name', '=', '手牌抹殺')
        .first()

    await knex("user_item").insert([
        // { user_id: `${userID.id}`, item: "改名卷", quantity: 1, item_id: 11 },
        { user_id: `${userID.id}`, item: `黑暗遊戲`, quantity: 1, item_id: `${itemID.id}` },
        { user_id: `${userID.id}`, item: `遊戲王卡背`, quantity: 1, item_id: `${itemID2.id}` },
        { user_id: `${userID.id}`, item: `手牌抹殺`, quantity: 1, item_id: `${itemID3.id}` },

    ]);
};
