import { io } from "./main";
import { ICard, IPlayer } from './models'

var suits = ["spades", "hearts", "diamonds", "clubs"];
var ranks = ["2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A"];
let players: IPlayer[] = [];
let activePlayers: IPlayer[] = [];
let users: any[] = [];
let messages: any[] = [];
let dealer: { hand: ICard[], total: number, displayTotal: string } = { hand: [], total: 0, displayTotal: "" };
let playersReadyCount: number = 0;
let betCount: number = 0;
let gameInProgress = false;

export class PlayerClass {
    public bet: number;
    public hand: String[];
    public total: number;
    public displayTotal: string;
    public doubleDown: boolean;
    public splitHand: null | string;
    constructor(public id: string, public player_id: any, public nickname: string, public balance: number, public icon: string) {
        this.id = id;
        this.player_id = player_id;
        this.nickname = nickname;
        this.balance = balance;
        this.icon = icon
        this.bet = 0;
        this.hand = [];
        this.total = 0;
        this.displayTotal = '';
        this.doubleDown = false;
        this.splitHand = null;
    }
}
let deck: ICard[] = new Array();
deck = createDeck()
let cheatCard: any = { Value: 21, img: `image/exodia.jpeg` }

export function shuffle(array: any[]) {
    array.sort(() => Math.random() - 0.5);
}
export function createDeck() {

    for (var i = 0; i < ranks.length; i++) {
        for (var x = 0; x < suits.length; x++) {
            var value = parseInt(ranks[i]);
            if (ranks[i] == "J" || ranks[i] == "Q" || ranks[i] == "K")
                value = 10;
            if (ranks[i] == "A")
                value = 1;
            var card = { Rank: ranks[i], Suit: suits[x], Value: value, img: `image/Playing-Cards/${ranks[i]}_of_${suits[x]}.png` };
            deck.push(card);
        }
    }

    shuffle(deck)
    return deck
}

async function dealCards(deck: any) {
console.log('dealdeal')
    for (const player of players) {
        player.hand = []
        let first_card: ICard = deck.pop();
        let second_card: ICard = deck.pop();
        player.hand.push(first_card);
        player.hand.push(second_card);
        console.log(`${player.nickname} receives ${first_card.Rank} of ${first_card.Suit}`);
        console.log(`${player.nickname} receives ${second_card.Rank} of ${second_card.Suit}`);
        player.total = getPoints(player).total;
        player.displayTotal = getPoints(player).displayTotal;
        console.log("deal card", players)
    }
    // let dealer: { hand: ICard[], total: number, displayTotal: string } = { hand: [], total: 0, displayTotal: "" };
    dealer.hand = [];
    let first_card: ICard = deck.pop();
    let second_card: ICard = deck.pop();
    dealer.hand.push(first_card);
    dealer.hand.push(second_card);
    console.log("deal hand for dealer", dealer.hand,players)
    console.log(`Dealer receives first ${first_card.Rank} of ${first_card.Suit}`)
    console.log(`Dealer receives second ${second_card.Rank} of ${second_card.Suit}`)
    dealer.total = getPoints(dealer).total;
    dealer.displayTotal = getPoints(dealer).displayTotal;
    io.sockets.emit('deal cards', {
        players: players,
        dealer: [
            { img: 'image/card-back.png' },
            { img: dealer.hand[1].img }
        ]
    });
    io.sockets.emit('dealer turn', dealer)
}
//計分
function getPoints(player: { hand: ICard[], total: number, displayTotal: string }) {
    var points = 0;
    for (var i = 0; i < player.hand.length; i++) {
        points += player.hand[i].Value;
    }
    player.total = points;
    let total = player.total
    let displayTotal = `${total}`;
    return {
        total: total,
        displayTotal: displayTotal
    };
}

export function startGame() {
    gameInProgress = true;
    betCount = 0;
    dealCards(deck);
}


export function welcomeNewPlayer(data: any, socket: any) {
    socket.on('new user', function () {
        console.log(`welcome ${data.nickname}!`);
        console.log('newuser', data, users, players)
        users.push({
            id: socket.id,
            player_id: data.id,
            nickname: data.nickname,
            money: data.balance,
            icon: data.user_icon
        });

        socket.emit('welcome', {
            users: users,
            currentPlayers: players,
            chatMessages: messages,
            greeting: `Welcome, ${data.nickname}`,
            gameInProgress: gameInProgress,
            dealerHand: dealer.hand,
        });

        if (gameInProgress) {
            socket.emit('fill me in', {
                currentPlayers: players,
                dealerHand: [
                    { img: '../image/card-back.jpg' },
                    { img: dealer.hand[1] }
                ],
            })
            if (activePlayers[0]) {
                io.sockets.emit('whose turn', { player: activePlayers[0] });
            }
        }

        console.log(`welcoming ${socket.id} (${data.nickname})`);
        let text = `${data.nickname} has joined`
        messages.push(text);

        io.sockets.emit('new user', `${data.nickname} has joined`);

        if (players.length < 5 && !gameInProgress) {
            console.log("sit~~~~~~", users, players)
            socket.emit('sit invite', users);
        }
    });
}

export function playerIn(socket: any) {
    socket.on('deal me in', function (data: any) {
        let index = data.length - 1
        let newPlayer: any = new PlayerClass(data[index].id, data[index].player_id, data[index].nickname, data[index].money, data[index].icon)
        if (players.length) {
            for (let i = 0; i < players.length; i++) {
                if (players[i].player_id === newPlayer.player_id) {
                    players.splice(i, 1)
                }
            }
        }
        players.push(newPlayer);
        console.log("fuck", newPlayer, "on99", data)
        console.log('playerpush', players, 'check', data)
        console.log(`new player! ${data[index].nickname} / ${players.length} players now`);
        io.sockets.emit('new player', { newPlayer: data });
        // return players
        if (players.length >= 5) {
            io.sockets.emit('sit uninvite');
          }
    });
}

export function bet(socket: any) {
    socket.on('place bet', function (data: IPlayer) {
        console.log("player bettttt", data, players, "gg", players.length)
        players.forEach((player) => {
            if (player.id === socket.id) {
                player.bet = data.bet;
                player.balance -= player.bet;
                console.log(`${player.nickname} bet ${data.bet}`);
                betCount++;
                console.log(`${betCount} out of ${players.length} have bet so far`);
                socket.broadcast.emit('player bet', { otherPlayer: player });
            }
        });
        if (betCount === players.length) {
            setTimeout(startGame,1000)
            console.log("game start", players)
        }
    })
}

export function Stand(socket: any) {
    socket.on('stand', function () {
        let playerIndex: any = findById(activePlayers, socket.id);
        console.log(`STAND: searching for ${socket.id} in activePlayers (length of ${activePlayers.length}), playerIndex is ${playerIndex}`);
        let player = activePlayers[playerIndex];
        if (playerIndex === 0) {
            console.log(`${player.nickname} is standing`)
            endPlayerTurn(deck, player.id, socket);
        }
    });
}


export function findById(array: any, id: any): number | void {
    for (let i = 0; i < array.length; i++) {
        if (array[i].id == id) {
            return i;
        } else {
            return undefined
        }
    }
}


export function playerReady(socket: any) {
    socket.on('player ready', function () {
        playersReadyCount++;
        console.log(`${socket.id} is ready ${playersReadyCount}`)
        if (playersReadyCount === players.length) {
            testForBlackjacks(socket);
            playersReadyCount = 0;
        };
    });
}
function testForBlackjacks(socket: any) {
    if (dealer.total === 21) {
        console.log('dealer has blackjack! game over');
        endGame();
        // setTimeout(function () {
        //     resetGame();
        //     socket.emit('reset board');
        // }, 10000);
    } else {
        activePlayers = [...players];

        console.log(`checking activeplayers for blackjack...${activePlayers}`);
        activePlayers = players.filter((player, index, array) => {

            if (player.total === 21 || player.total === 11) {
                console.log(`${player.nickname} has blackjack!`);
                player.balance += player.bet;
                io.to(player.id).emit('turn over');
                io.sockets.emit('player bet', { otherPlayer: player });
            }

            return player.total < 21;
        });

        if (activePlayers.length > 0) {
            io.sockets.emit('whose turn', { player: activePlayers[0] });
            // } else {
            //     endGame(socket);
            //     setTimeout(function () {
            //         resetGame();
            //         socket.emit('reset board')
            //     }, 3000);
        }
    }
}

function endPlayerTurn(deck: any, id: any, socket: any) {
    let playerIndex: any = findById(activePlayers, id);

    console.log(`ending player turn for ${id}!!!`)
    activePlayers.splice(playerIndex, 1);

    if (activePlayers.length) {
        io.sockets.emit('whose turn', { player: activePlayers[0] });
    } else {
        dealerTurn(deck, socket);
    }
}

export function dealerTurn(deck: any, socket: any) {

    while (dealer.total < 17) {
        let newCard = deck.pop();

        dealer.hand.push(newCard);
        console.log(`Dealer receives ${newCard.Rank} of ${newCard.Suit}`);
        console.log(`${deck.length} cards left in shoe`);

        dealer.total = getPoints(dealer).total;
        dealer.displayTotal = getPoints(dealer).displayTotal;

        socket.emit('endgame', {
            dealerHand: dealer.hand,
            dealerTotal: dealer.total,

        })
    }
    setTimeout(function () {
        endGame()
        setTimeout(function () {
            resetGame()
            io.sockets.emit('reset board');
        }, 5000);
    }, 2000);

}


function resetGame() {
    players.forEach((player) => {
        player.bet = 0;
        player.hand = [];
        player.total = 0;
        player.displayTotal = '';
        player.splitHand = null;
    })

    dealer.hand = [];
    dealer.total = 0;
    betCount = 0;
    deck = []
    playersReadyCount = 0;
    gameInProgress = false;
    console.log('reset game~~~~~', players, dealer, deck)
    if (deck.length < 52) {
        deck = createDeck();
    }
    io.sockets.emit('sit invite');
}

export function hitCard(socket: any) {
    socket.on('hit', function () {
        let newCard: any = deck.pop()
        players.forEach((player) => {
            if (player.id === socket.id) {
                console.log(`${player.nickname} hits, dealing ${newCard.Value} of ${newCard.Suit}`);
                console.log(`${deck.length} cards left`);
                player.hand.push(newCard);
                player.total = getPoints(player).total;
                player.displayTotal = getPoints(player).displayTotal;

                io.sockets.emit('new card', { player: player, card: newCard, total: player.total });

                if (player.total > 21) {
                    console.log(`${player.nickname} busted!`);
                    let playerIndex: any = findById(activePlayers, socket.id);
                    let bustedPlayer = activePlayers[playerIndex];
                    activePlayers.splice(playerIndex, 1);
                    io.to(bustedPlayer.id).emit('turn over', player);
                    if (activePlayers.length) {
                        console.log(`player turn: ${activePlayers[0].id} (${activePlayers[0].nickname})`);
                        io.sockets.emit('whose turn', { player: activePlayers[0] });
                    } else {
                        console.log('dealer turn');
                        dealerTurn(deck, socket);
                    }
                }
            }
        });
    });
}

export function endGame() {
    let playerList = {};
    gameInProgress = false;

    players.forEach((player: IPlayer) => {
        playerList[player.id] = true;
        console.log('how', player)
        console.log(`Checking ${player.nickname} win status...`);
        if (player.splitHand === null) {
            if (player.total > 21) {
                console.log(`${player.nickname} busted!`);
                informUserOfGameOver(player.id, 'lose', '', player, players);

            } else if (dealer.total > 21) {
                if (player.total === 21 && player.hand.length === 1) {
                    console.log(`Dealer bust, but ${player.nickname} very狗`);
                    var message = '';
                    informUserOfGameOver(player.id, 'win', message, player, players)
                }
                else if (player.total === 21 && player.hand.length === 2) {
                    console.log(`Dealer bust, but ${player.nickname} already won by blackjack`);
                    var message = '';
                    informUserOfGameOver(player.id, 'win', message, player, players)
                } else if (player.total < 21 || (player.total === 21 && player.hand.length > 2)) {
                    console.log(`${player.nickname} won because dealer bust!`);
                    player.balance += player.bet * 2;
                    console.log('result', player.bet, player.balance)

                    let message = `Dealer busts! YOU WIN $${player.bet}!`;
                    informUserOfGameOver(player.id, 'win', message, player, players)
                }

            } else if (dealer.total === player.total) {
                console.log(`${player.nickname} pushed!`)
                player.balance += player.bet;
                console.log('result', player.bet, player.balance)

                informUserOfGameOver(player.id, 'push', 'PUSH!', player, players);

            } else if (player.total > dealer.total) {
                if (player.total === 21 && player.hand.length === 2) {
                    console.log(`${player.nickname} already won by blackjack`)
                    var message = '';
                    console.log('result', player.bet, player.balance)

                } else {
                    console.log(`${player.nickname} won!`)
                    player.balance += player.bet * 2;
                    var message = `YOU WIN $${player.bet}!`;
                    console.log('result', player.bet, player.balance)
                }

                informUserOfGameOver(player.id, 'win', message, player, players);

            } else if (dealer.total > player.total && dealer.total <= 21) {
                console.log(`${player.nickname} lost.`)

                let message = 'Dealer wins.';

                if (dealer.total === 21 && dealer.hand.length === 2) {
                    message += ' Dealer has blackjack.';
                }
                informUserOfGameOver(player.id, 'lose', message, player, players);
            }
        }
    });
    users.forEach((user) => {
        if (!playerList[user.id]) {
            informUserOfGameOver(user.id, '', 'Game over!', user, users);
        };
    });

    console.log('send', players, users)

    io.sockets.emit('update money', { players: players });
}


function informUserOfGameOver(id: string, status: any, message: any, player: any, players: IPlayer[]) {
    io.to(id).emit('gameover', {
        status: status,
        message: message,
        dealerHand: dealer.hand,
        dealerTotal: dealer.total,
        player: player,
        players: players
    });
}
export function leave(socket: any) {
    socket.on('leave game', function () {
        console.log('!!!!!leave!!!!!', players, users)
        players.forEach((player) => {
            player.balance = player.balance
        })
        removePlayer(socket.id, socket);
        if (betCount === players.length && players.length !== 0 && !gameInProgress) {
            // setTimeout(startGame, 3000);
            console.log('leave start')
        }
    });
}

function removePlayer(id: string, socket: any) {
    players=[]
    let playerIndex: any = findById(players, id);
    console.log('remove', players, id, "sda", playerIndex, "asdas", users)
    if (playerIndex >= 0) {
        console.log(`${players[playerIndex].nickname} no longer playing!`);
        io.sockets.emit('player left', { leftPlayer: players[playerIndex] });
        io.sockets.emit('sit invite');
        players.splice(playerIndex, 1);
        console.log('remove22222', players, "sda", playerIndex, "asdas", users)

        if (players.length === 0) {
            dealer = { hand: [], total: 0, displayTotal: '' };
            console.log('no more players! resetting...');
            io.sockets.emit('reset board');
            resetGame();
        }
    }
    if (gameInProgress) {
        removeActivePlayer(id, socket);
    }

}

function removeActivePlayer(id: any, socket: any) {
    let playerIndex: any = findById(activePlayers, id);
    if (playerIndex >= 0) {
        activePlayers.splice(playerIndex, 1);

        if (playerIndex === 0 && activePlayers[0]) {
            io.sockets.emit('whose turn', { player: activePlayers[0] });
        } else {
            dealerTurn(deck, socket);
        }
    }
}

export function cheat(socket: any) {
    socket.on(('cheat'), function () {
        let newCard = cheatCard
        players.forEach((player) => {
            if (player.id === socket.id) {
                console.log(`${player.nickname} use dark power`);
                player.hand = []
                player.hand.push(newCard)
                player.total = getPoints(player).total;
                player.displayTotal = getPoints(player).displayTotal;
                io.sockets.emit('new card', { player: player, card: newCard, total: player.total, item: true });
            };
        });
    });
}

export function Chat(socket: any) {
    socket.on('chat message', function (data: any) {
        if (messages.length > 100) {
            messages.pop();
        }
        else {
            messages.push({ name: data.name, text: data.text });
            io.sockets.emit('new message', { nickname: data.nickname, id: socket.id, text: data.content });
        }
    });
}

function removeUser(id: any) {
    let userIndex: any = findById(users, id);

    if (userIndex >= 0) {
        console.log(`${users[userIndex].nickname} disconnected!`,users);
        messages.push({ text: `${users[userIndex].nickname} left` });
        io.sockets.emit('user left', { user: users[userIndex] });

        users.splice(userIndex, 1);
    }
}
export function leftGame(socket: any) {
    socket.on('left user', function () {
        console.log('disconnect !!')
        disconnect(socket.id, socket);
    });
}

export function sendDisconnect(socket: any) {
    socket.on('disconnect', function () {
        console.log(`${socket.id} disconnect`)
        disconnect(socket.id, socket);
    });
}

function disconnect(id: any, socket: any) {
    removePlayer(id, socket);
    removeUser(id);
    if (betCount === players.length && players.length !== 0 && !gameInProgress) {
        console.log("rerere")
        // setTimeout(startGame, 1000);
    }

}