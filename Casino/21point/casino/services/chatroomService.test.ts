import Knex from "knex"
import * as knexConfig from "../knexfile"
import tables from "../tables"
import { ChatroomService } from "./chatroomService"
import { User } from "../models"

const config = knexConfig["test"]
const knex = Knex(config)

let userID = 1

describe("ChatroomService Testing", () => {
    let service: ChatroomService

    beforeEach(async () => {
        let users: Array<{ id:number }> = await knex.select("id").from<User>("users")
        userID = users[0].id

        await knex(tables.CHATROOM).del()
        await knex(tables.CHATROOM).insert([
            {
                user_id: users[0].id,
                content: "食飽飯無野做？"
            }
        ])
        service = new ChatroomService(knex)
    })

    it("test public_chat Success", async () => {
        const message_send = await service.sending_chat("食蕉啦你",userID)
        expect( message_send ).toBeCalled
    })

    afterAll(async () => {
        await knex.destroy()
    })
})