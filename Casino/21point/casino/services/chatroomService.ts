import { Knex } from "knex"
import tables from "../tables"

export class ChatroomService {
    constructor(private knex: Knex) { }

    async sending_chat(message: string, user_id: number) {
        const [player_chat_id] = await this.knex(tables.CHATROOM)
            .insert({
                "user_id": user_id,
                "content": message
            })
            .into("chat_room")
            .returning("id")
        return player_chat_id
    }
}