import { Knex } from "knex"
import { Room } from "../models"

export class CreateRoomService {
    constructor(private knex: Knex) { }

    async create_room(room_name: string, room_password: string | null, user_id: number) {
        const room_id: Room = await this.knex("room").insert({
            room_name: room_name,
            room_password: room_password,
            user_id: user_id
        }).returning('id')
        // console.log(typeof (room_id))
        // console.log(room_id[0])
        // const room_id_number = room_id[0]
        return room_id[0]

    }

    async show_room() {
        const room_info = await this.knex.select('*').from("room").orderBy('id', 'desc')
        // console.log(room_info)
        return room_info

    }
    async show_latest_room() {
        const room_info = await this.knex.select('*').from("room").orderBy('id', 'desc').limit(1)
        // console.log(room_info)
        return room_info

    }

    async join_room(user_id: number) {
        const nickname = await this.knex.select('nickname').from("users").where("id", user_id)
        // console.log(nickname[0].nickname)
        return nickname[0].nickname
    }




}