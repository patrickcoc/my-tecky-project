import { Knex } from "knex";
import tables from"../tables"
import {IPlayer, Record}from "../models"
export class GamePlayService {
    constructor(private knex: Knex) {}

    async getPlayer(id:number){
        const users_data :IPlayer = await this.knex(tables.USERS)
        .where("id",id)
        .first();
        return users_data
    }

    async createPlayerRecord(id:number,win:boolean,earn_coin:number){
        const result=await this.knex<Record>(tables.PERSONAL_MATCH_HISTORY_LIST)
        .insert({id,win,earn_coin})
        return result  
    }
    async updataPlayerAmount(nickname:string,amount:number){
        const playerAmount = await this.knex(tables.USERS)
        .update("balance",amount)
        .where("nickname",nickname)
        .returning("balance")
        return playerAmount
    }
   
}










