import { Knex } from "knex"

export class GameRoomService {
    constructor(private knex: Knex) {}

    async get_room_info(room_id: number) {
        const room_info = await this.knex.select("room_name")
        .from("room")
        .where("id", room_id)
        return room_info[0].room_name
    }

}