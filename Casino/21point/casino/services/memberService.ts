import { Knex } from "knex"
import { Member, UserItem } from "../models"
import tables from "../tables"

export class MemberService {
    constructor(private knex: Knex) { }

    async get_member_information(username: string) {
        // console.log(username)
        const member_info = await this.knex<Member>(tables.USERS)
            .where("username", username)
        return member_info
    }

    async get_member_item(id: number) {
        const get_item = await this.knex<UserItem>(tables.USERITEM)
            .join('users', 'users.id', 'user_item.user_id')
            .join('game_shop_items','game_shop_items.id','user_item.item_id')
        return get_item
    }
}