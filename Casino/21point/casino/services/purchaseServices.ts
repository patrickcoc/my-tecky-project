import { Knex } from "knex"
// import { Items } from "../models"

export class PurchaseService {
    constructor(private knex: Knex) { }

    async purchasing_personal_item(user_id: number, item_name: string, quantity: number) {

        try {
            await this.knex.transaction(async trx => {
                const product : {cost:number} = await trx("game_shop_items")
                    .select("id","cost")
                    .where("item_name", item_name)
                    .first()

                const deduct_amount = await trx("users")
                    .where("id", user_id)
                    .decrement("balance", product['cost'] * quantity)
                console.log(`${deduct_amount * quantity} has been deducted`)

                const latest_balance: number = await trx("users")
                    .where("id", user_id)
                    .returning("balance")
                    .first()
                    
                if (latest_balance['balance'] < 0) {
                    throw "negative balance";
                }

                return await trx.insert({
                    "user_id": user_id,
                    "item": item_name,
                    "item_id":product['id'],
                    "quantity": quantity
                }).into("user_item")
                .returning("id")
            })
        } catch (error) {
            console.error(error);

            throw error;
        }


    }
}