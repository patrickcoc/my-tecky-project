import { Knex } from "knex";

export class QuitRoomService {
    constructor(private knex: Knex) {}
    
    async quit_room(user_id: number) {
        const nickname = await this.knex.select('nickname').from("users").where("id", user_id)
        // console.log(nickname[0].nickname)
        return nickname[0].nickname
    }
}