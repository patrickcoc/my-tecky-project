import { Knex } from "knex"
import tables from "../tables"

export class ShopService {
    constructor(private knex: Knex) {}
    
    async showItem(id:number,item_name:string,cost:number,game_coin_id:number,item_icon:string){
        const item_data=await this.knex(tables.ITEMS)
        .insert({id,item_name,cost,game_coin_id,item_icon})
        return item_data       
    }

    async get_shop_item(){
        const item_data = await this.knex(tables.ITEMS)
        console.log(item_data)
        return item_data       
    }
    
    
}