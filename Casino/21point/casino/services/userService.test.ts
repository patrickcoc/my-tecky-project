import Knex from "knex"
import * as knexConfig from '../knexfile'
import tables from "../tables"
import { UserService } from "./UserService"

const config = knexConfig["test"]
const knex = Knex(config)

describe("UserService Testing", () => {
    let service: UserService

    beforeEach(async () => {
        await knex(tables.USERITEM).del()
        await knex(tables.CHATROOM).del()
        await knex(tables.USERS).del()
        await knex(tables.USERS).insert([
                {
                    username: "hello",
                    password: "$2a$10$FLFQdB7v9W8452NASUteRusHvNthzGkGPTZgHEHs5w1Sp7CBj/5c2",
                    nickname: "hello",
                    email: "hello@casino.com",
                    user_icon: "Charizard.webp",
                    created_at: "2021-07-12 12:57:55.970024+08",
                    updated_at: "2021-07-12 12:57:55.970024+08",
                    online: "",
                    balance: 100000,
                    online_boolean: false
                },
                {
                    username: "byebye",
                    password: "$2a$10$FLFQdB7v9W8452NASUteRusHvNthzGkGPTZgHEHs5w1Sp7CBj/5c2",
                    nickname: "byebye",
                    email: "byebye@casino.com",
                    user_icon: "Charizard.webp",
                    created_at: "2021-07-12 12:57:55.970024+08",
                    updated_at: "2021-07-12 12:57:55.970024+08",
                    online: "",
                    balance: 100000,
                    online_boolean: false
                }
        ])
        service = new UserService(knex)
    })

    it("test login Success", async () => {
        const user = { username: "hello" }
        const found_user = await service.login(user.username)
        expect( found_user?found_user.username:"undefined user").toBe(user.username)
    })

    it("test login Success", async () => {
        const user = { username: "777染頭" }
        const found_user = await service.login(user.username)
        expect( found_user ).toBeUndefined()
    })

    afterAll(async () => {
        await knex.destroy()
    })
})