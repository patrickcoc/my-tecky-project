import { Knex } from "knex"
import { hashPassword } from "../hash"
import { User } from "../models"
import tables from "../tables"

export class UserService {
  constructor(private knex: Knex) { }

  // login
  async login(username: string) {
    await this.knex('users')
      .update('online_boolean', true)
      .where('username', username)
    const found_user = await this.knex<User>(tables.USERS)
      .where('username', username)
      .first()
    console.log(`[found_user]:${found_user}`)
    console.log(found_user)
    return found_user
  }

  // logout
  async logout(user_id: number) {
    await this.knex('users')
      .update('online_boolean', false)
      .where('id', user_id)
  }

  // show all online users
  async display_all_online_users() {
    const online_users = await this.knex<User>(tables.USERS)
      .whereNotNull('online')
      .andWhere('online_boolean', true)
      .select('nickname')

    return online_users
  }

  // updating user socket_ID
  async updating_user_socket_ID(userdata: {}) {
    const username = userdata['username']
    const socket_ID = userdata['socket_ID']
    console.log(`[username]:${username},[socket_ID]:${socket_ID}`)
    await this.knex<User>(tables.USERS)
      .where('username', username)
      // .update()
      .update("online", socket_ID)
  }

  // add user balance
  async add_user_balance(user_id: number) {
    const balance_update = await this.knex<User>(tables.USERS)
      .where('id', user_id)
      .increment('balance', 10000)
    console.log(balance_update)
  }

  // register user
  async register_user(email: string, nickname: string, username: string, password: string) {
    const check_email = await this.knex<User>(tables.USERS)
      .where('email', email).orWhere('username', username)
    console.log("check email")
    console.log(check_email)

    if (check_email[0] === undefined) {
      const hashed_password = await hashPassword(password)
      console.log(hashed_password)
      const id = await this.knex<User>(tables.USERS)
        .insert({
          username: username,
          password: hashed_password,
          email: email,
          nickname: nickname,
          user_icon: "hihi",
          balance: 100000,
          online_boolean: true
        }).returning('id')
      console.log("This is id")
      console.log(id)
      return {
        id: id[0],
        boolean: true,
      }
    } else if (check_email) {
      console.log(typeof (check_email))
      console.log(check_email.find((check) => check.email === email))
      console.log(check_email.find((check) => check.username === username))
      return { boolean: false }
    }
    return {}
  }

  // change user nickname
  async change_user_nickname(user_id: number, nickname: string, item_name: string) {
    console.log("This is userService")
    console.log(`user_id:${user_id}`)
    console.log(`nickname:${nickname}`)

    const current_nickname = await this.knex<User>(tables.USERS)
      .where('id', user_id)
    console.log("below is current nickname")
    console.log(current_nickname[0].nickname)

    if (current_nickname[0].nickname !== nickname) {
      console.log("compare result")
      console.log(current_nickname[0].nickname)
      console.log(nickname)
      const new_nickname: { nickname: string } = await this.knex<User>(tables.USERS)
        .where("id", user_id)
        .update("nickname", nickname)
        .returning("nickname")

      console.log("find result")
      const target_del_item = await this.knex('user_item')
        .where('user_id', user_id)
        .andWhere('item', item_name)
        .first()
      console.log(target_del_item)
      await this.knex('user_item')
        .del()
        .where('id', target_del_item.id)

      return new_nickname
    } else {
      return
    }


  }
}
