const tables = Object.freeze({
    USERS: "users",
    ROOM: "room",
    FRIEND_LIST: "friend_list",
    GAMECOIN: "game_coin",
    USERITEM: "user_item",
    PERSONAL_MATCH_HISTORY_LIST: "personal_match_history_list",
    GAMEMATCGHISTORY: "game_match_history",
    CHATROOM: "chat_room",
    NOTES: "notes",
    ITEMS: "game_shop_items",
})

export default tables