CREATE DATABASE project;

\c project

CREATE TABLE users(
    id SERIAL PRIMARY KEY,
    fullname VARCHAR(255) NOT NULL, 
    birthday DATE, 
    email TEXT NOT NULL,
    countrycode TEXT NOT NULL,
    phone TEXT NOT NULL,
    question VARCHAR(255),
    answer VARCHAR(255),
    password VARCHAR(255) NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL DEFAULT NOW()
);

CREATE TABLE merchants(
id SERIAL PRIMARY KEY,
merchants_name VARCHAR(255) NOT NULL,
merchants_code Varchar (5) check (merchants_code in ('RCB','DCL'))
);

CREATE TABLE cruiseplan(
id SERIAL PRIMARY KEY,
merchants_id integer,
FOREIGN KEY (merchants_id) REFERENCES merchants(id),
cruise_name TEXT,
price TEXT,
date_of_sailing TEXT,
starting_location TEXT,
return_location TEXT,
day TEXT,
image TEXT
);

CREATE TABLE orders(
    id SERIAL PRIMARY KEY,
    user_id integer,
    FOREIGN KEY (user_id) REFERENCES users(id),
    cruiseplan_id integer,
    FOREIGN KEY (cruiseplan_id) REFERENCES cruiseplan(id),
    firstname TEXT NOT NULL,
    lastname TEXT NOT NULL,
    email TEXT NOT NULL,
    address TEXT NOT NULL,
    country TEXT NOT NULL,
    state TEXT NOT NULL,
    cardname TEXT NOT NULL,
    cardnumber TEXT NOT NULL,
    expiration TEXT NOT NULL,
   cvv TEXT NOT NULL
);



CREATE TABLE order_items(
id SERIAL PRIMARY KEY,
cruiseplan_id integer,
FOREIGN KEY (cruiseplan_id) REFERENCES cruiseplan(id),
orders_id integer,
FOREIGN KEY (orders_id) REFERENCES orders(id),
cruisedate TEXT NOT NULL
);

CREATE TABLE notice(
    id SERIAL PRIMARY KEY,
    title TEXT,
    content TEXT NOT NULL
);

insert INTO merchants (merchants_name,merchants_code) values ('dreamcruiseline','DCL');
insert INTO merchants (merchants_name,merchants_code) values ('royalcaribbean','RCB');


--先npx ts-node scraping-data/cruise-data.ts--
UPDATE cruiseplan SET image = '../image/curiseimage/Explorer Dream.jpeg'  where cruise_name = 'Explorer Dream';
UPDATE cruiseplan SET image = '../image/curiseimage/Genting Dream.jpeg'  where cruise_name = 'Genting Dream';
UPDATE cruiseplan SET image = '../image/curiseimage/World Dream.jpeg'  where cruise_name = 'World Dream';
UPDATE cruiseplan SET image = '../image/curiseimage/allure.jpeg'  where cruise_name = ' Okinawa & Ishigaki Cruise';
UPDATE cruiseplan SET image = '../image/curiseimage/mariner.jpeg'  where cruise_name = ' Best Of Nagasaki Cruise';
UPDATE cruiseplan SET image = '../image/curiseimage/oasis.jpeg'  where cruise_name = ' Chan May Cruise';
UPDATE cruiseplan SET image = '../image/curiseimage/ovation.jpeg'  where cruise_name = ' Kagoshima & Nagasaki Cruise';
UPDATE cruiseplan SET image = '../image/curiseimage/symphony.jpeg'  where cruise_name = ' Best Of Fukuoka Cruise';
UPDATE cruiseplan SET image = '../image/curiseimage/wonder.jpeg'  where cruise_name = ' Fukuoka & Nagasaki Cruise';
UPDATE cruiseplan SET image = '../image/curiseimage/wonder.jpeg'  where cruise_name = ' Best Of Sasebo Cruise';