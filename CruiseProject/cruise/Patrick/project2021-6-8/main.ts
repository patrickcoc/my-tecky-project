import express from "express";
import expressSession from "express-session";
import path from "path";
// import multer from "multer";
import { logger } from "./logger";
import { isLoggedIn } from "./guards";
import { Client } from "pg";
import dotenv from "dotenv";
import http from "http";
import { Information } from "./models";
import { Server as SocketIO } from "socket.io";
import grant from "grant";

const app = express();

const server = new http.Server(app);
export const io = new SocketIO(server);

app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(
    expressSession({
        secret: "cruise project",
        resave: true,
        saveUninitialized: true,
    })
);

dotenv.config(); // load .env 嘅內容

export const client = new Client({
    database: process.env.DB_NAME,
    user: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD

});
client.connect();



app.use((req, res, next) => {
    logger.debug(`ip: [${req.ip}], path: [${req.path}] method: [${req.method}]`);
    next();
});


const grantExpress = grant.express({
    defaults: {
        origin: "http://localhost:8080",
        transport: "session",
        state: true,
    },
    google: {
        key: process.env.GOOGLE_CLIENT_ID || "",
        secret: process.env.GOOGLE_CLIENT_SECRET || "",
        scope: ["profile", "email"],
        callback: "/login/google",
    },
});
app.use(grantExpress);

app.get("/compare", async (req, res) => {
    const cruisePlan:Information[]= (await client.query(`SELECT * FROM cruiseplan;`)).rows;
    res.json(  cruisePlan );
});

import { userRoutes } from "./routers/usersRoutes";
import{noticesRoutes} from"./routers/noticesRoutes"
import { orderRoutes} from "./routers/orderRoutes";
import { memberRoutes} from "./routers/memberRoutes";

app.use("/",userRoutes);
app.use("/",orderRoutes);
app.use("/",noticesRoutes);
app.use("/",memberRoutes);

app.use(express.static(path.join(__dirname, "public")));
app.use(isLoggedIn, express.static(path.join(__dirname, "protected")));

app.use((req, res) => {
    res.sendFile(path.join(__dirname, "public/404.html"));
});

const PORT = 8080;
server.listen(PORT, () => {
    console.log(`listening to port: [${PORT}]`);
});