export interface User {
    id: string,
    fullname: string,
    birthday: Date,
    email: string,
    countrycode: string,
    phone: string,
    question: string,
    answer: string,
    password: string,
}


export interface googleUser {
    id: string,
    fullname: string,
    email: string,

}
export interface Information {
    curisename: string[];
    startingLocation: any[];
    returnLocation: any[];
    price: any[];
    day: any[];
    date: any[][] | any[];
}