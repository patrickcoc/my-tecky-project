// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
    'use strict'
  
    window.addEventListener('load', function () {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation')
  
      // Loop over them and prevent submission
      Array.prototype.filter.call(forms, function (form) {
        form.addEventListener('submit', function (event) {
          if (form.checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
          }
          form.classList.add('was-validated')
        }, false)
      })
    }, false)
  }())


document.getElementById("checkout-form").addEventListener("submit", async function (e) {
  e.preventDefault();
  const form = this;
  let urlParams = new URLSearchParams(window.location.search);
  const planID = urlParams.get('planID')
  document.getElementById('cruiseplanID').innerHTML=planID;
  const date = urlParams.get('date');
  document.getElementById('cruiseDate').innerHTML=date;
  const firstname = form["firstname"].value;
  const lastname = form["lastname"].value;
  const email = form["email"].value;
  const address = form["address"].value;
  const country = form["country"].value;
  const state = form["state"].value;
  const cardname = form["cardname"].value;
  const cardnumber = form["cardnumber"].value;
  const expiration = form["expiration"].value;
  const cvv = form["cvv"].value;
  const formObject = { firstname,lastname,email,address,country,state,cardname,cardnumber,expiration,cvv };
  console.log(formObject)
  const res = await fetch("/checkout", {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
      },
      body:JSON.stringify(formObject),
  });

  if (res.status === 200) {
     console.log(formObject)
     window.location="member.html"   
  } else {
      const data = await res.json();
      const errMessage = data.message;
      alert("worng information");
  }
});