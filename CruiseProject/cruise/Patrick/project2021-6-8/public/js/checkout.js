// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
    'use strict'
  
    window.addEventListener('load', function () {
      // Fetch all the forms we want to apply custom Bootstrap validation styles to
      var forms = document.getElementsByClassName('needs-validation')
  
      // Loop over them and prevent submission
      Array.prototype.filter.call(forms, function (form) {
        form.addEventListener('submit', function (event) {
          if (form.checkValidity() === false) {
            event.preventDefault()
            event.stopPropagation()
          }
          form.classList.add('was-validated')
        }, false)
      })
    }, false)
  }())

  function displayOrders(){
  let urlParams = new URLSearchParams(window.location.search);
  const planID = urlParams.get('planID')
  document.getElementById('cruiseplanID').innerHTML=planID;
  const date = urlParams.get('date');
  document.getElementById('cruiseDate').innerHTML=date;
  const location = urlParams.get('location');
  document.getElementById('cruiseLocation').innerHTML=location;
  const price = urlParams.get('price');
  document.getElementById('cruisePrice').innerHTML=price;
  }
  displayOrders()

document.getElementById("checkout-form").addEventListener("submit", async function (e) {
  e.preventDefault();
  const form = this;  
  const planID = document.getElementById('cruiseplanID').textContent;
  const date = document.getElementById('cruiseDate').textContent;
  const firstname = form["firstname"].value;
  const lastname = form["lastname"].value;
  const email = form["email"].value;
  const address = form["address"].value;
  const country = form["country"].value;
  const state = form["state"].value;
  const cardname = form["cardname"].value;
  const cardnumber = form["cardnumber"].value;
  const expiration = form["expiration"].value;
  const cvv = form["cvv"].value;
  const formObject = { planID,date,firstname,lastname,email,address,country,state,cardname,cardnumber,expiration,cvv };
  console.log(formObject)
  const res = await fetch("/checkout", {
      method: "POST",
      headers: {
          "Content-Type": "application/json; charset=utf-8",
      },
      body:JSON.stringify(formObject)
  });
  //  const data = await res.json();
  //  window.location = `/member.html?orderID=${data.orderId.id}`
  if (res.status === 200) {
   
     window.location = `/member.html`
      
  } else {
      const errMessage = data.message;
      alert("worng information");
  }
});