/*!
* Start Bootstrap - Shop Homepage v5.0.0 (https://startbootstrap.com/template/shop-homepage)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-shop-homepage/blob/master/LICENSE)
*/



// This file is intentionally blank
// Use this file to add JavaScript to your project

window.onload = async function () {
    await displayCruisePlan()

    await addListenerToCruisePlan()
}

async function displayCruisePlan() {
    const res = await fetch("/compare");
    const data = await res.json();
    const instr = document.querySelector(".planPosition")
    for (let i = 0; i < data.length; i++) {
        if (data[i].merchants_id === 1) {
            let newDate = data[i].date_of_sailing;
            let text = newDate.split(",")
        }
        let date = data[i].date_of_sailing.replace(/\"/g, "").replace(/{/g, "").replace(/}/g, "").split(",")


        const str = `
        <div class="col mb-5 compareBox company${data[i].merchants_id} location${data[i].return_location}" data-lacation="${data[i].return_location}">
        <div class="card h-100">
        <form class="cart-form" method="POST" action="/compare">
            <div class="badge bg-dark text-white position-absolute" style="top: 0.5rem; right: 0.5rem">Sale</div>
            <img class="card-img-top" src="${data[i].image}" alt="..." />
            <div class="card-body p-4">
                <div class="text-center">
                    <h5 class="fw-bolder" name="cruiseName">${data[i].cruise_name}</h5>
                    <div class="d-flex justify-content-center small text-warning mb-2">
                        <div><i onclick="myFunction(this)" class="bi bi-star"></i></div>
                        <div><i onclick="myFunction(this)" class="bi bi-star"></i></div>
                        <div><i onclick="myFunction(this)" class="bi bi-star"></i></div>
                        <div><i onclick="myFunction(this)" class="bi bi-star"></i></div>
                        <div><i onclick="myFunction(this)" class="bi bi-star"></i></div>
                    </div>
                    <p class="planID" name="ID" style="display:none">${data[i].id}</p>
                    <p class="location" name="location">${data[i].starting_location} > ${data[i].return_location}</p>
                    <p class="day" name="day">日數：${data[i].day}</p>
                    <p class="price" name="price">${data[i].price}</p>
                    <select class="text-muted" name="date">
                    <option>出發日期</option>
                    ${date.map(date => `<option type="text" >${date}</option>`)}
                    </select>
                </div>
            </div>
            <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                <div class="text-center">
                <button class="btn btn-outline-dark mt-auto" type="submit">Add to cart</button>
                </div>
            </div>
            </form>
        </div>   
    </div>
    `
        instr.innerHTML += str
    }
}




function myFunction(x) {
    if (x.classList == "bi bi-star") {
        x.classList.remove("bi-star");
        x.classList.add("bi-star-fill");
    }
    else if (x.classList == "bi bi-star-fill") {
        x.classList.remove("bi-star-fill");
        x.classList.add("bi-star");
    }
}


async function addListenerToCruisePlan() {
    document.querySelectorAll('.cart-form').forEach(e => {
        e.addEventListener("submit", async function (e) {
            e.preventDefault();
            const form = this;
            const planID = form.querySelector(".planID").textContent
            const location = form.querySelector(".location").textContent
            const price = form.querySelector(".price").textContent
            const date = form["date"].value;
            const formObject = { planID, date };
            console.log(formObject)
            window.location = `/checkout.html?date=${date}&planID=${planID}&location=${location}&price=${price}`
            if (res.status === 200) {
                console.log(formObject)
                // window.location = "/checkout.html"
            } else {
                const data = await res.json();
                const errMessage = data.message;
                alert(errMessage);
            }

        })
    })
}
document.getElementById("tagBox").addEventListener("click",function(e){
    document.querySelectorAll('.compareBox').forEach(e=>e.style.display= "block");
})

document.getElementById("tagBox").addEventListener("submit", async function (e) {
    e.preventDefault();
    const form = this;
    const company1 = form["company1"].checked;
    const company2 = form["company2"].checked;
    const aim1 = form["aim1"].checked;
    const aim2 = form["aim2"].checked;
    const aim3 = form["aim3"].checked;
    const aim4 = form["aim4"].checked;
    console.log(company1,company2)
    
document.querySelectorAll('.compareBox').forEach(e=>e.style.display= "none");
if(company1){
    document.querySelectorAll(".company1").forEach(e=>e.style.display="block");
}
if(company2){
    document.querySelectorAll(".company2").forEach(e=>e.style.display="block");
}
if(aim1){
    document.querySelectorAll(".location香港").forEach(e=>e.style.display="block");
}    
if(aim2){
    document.querySelectorAll(".location新加坡").forEach(e=>e.style.display="block");
}  
if(aim3){
    document.querySelectorAll(".location北京天津").forEach(e=>e.style.display="block");
    document.querySelectorAll(".location上海寶山").forEach(e=>e.style.display="block");
}  
if(aim4){
    document.querySelectorAll(".location基隆").forEach(e=>e.style.display="block");
}     
});



