// const loginForm = document.getElementById("login-form");
// const loginButton = document.getElementById("submit");
const loginErrorMsg = document.getElementById("login-error-msg");

// loginButton.addEventListener("click", (e) => {
//     e.preventDefault();
//     const username = loginForm.email.value;
//     const password = loginForm.password.value;

//     if (username === "user" && password === "password") {
//         alert("You have successfully logged in.");
//         location.reload();
//     } else {
//         loginErrorMsg.style.opacity = 1;
//     }
// })

document.getElementById("login-form").addEventListener("submit", async function (e) {
    e.preventDefault();
    const form = this;
    const email = form["email"].value;
    const password = form["password"].value;
    const formObject = { email, password };
    console.log(formObject)
    const res = await fetch("/log-in", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formObject),
    });
    const data = await res.json();
    console.log(data)
    window.location = `/member.html?id=${data.users.id}`  
    if (res.status === 200) {
       console.log(formObject)
    //    window.location="/member.html"   
    } else {
        loginErrorMsg.style.opacity = 1;
        const data = await res.json();
        const errMessage = data.message;
        alert(errMessage);
    }
});