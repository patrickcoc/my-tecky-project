
async function displayMemberData() {
  const res = await fetch("/member");
  const data = await res.json();
  console.log(data)
  const instr = document.querySelector(".memberData")
  for (let i = 0; i < data.length; i++) {

    const str = `
        <div class="orderTable" style="padding:30px">
        <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Order Information</th>
        </tr>
      </thead>
      
      <tbody>
        <tr>
          <th scope="row">userID</th>
          <td>${data[i].user_id}</td>
        </tr>
        <tr>
        <th scope="row">orderID</th>
        <td class="orderID">${data[i].orders_id}</td>
      </tr>
        <tr>
          <th scope="row">Email</th>
          <td>${data[i].email}</td>
        </tr>
        <tr>
          <th scope="row">Name</th>
          <td>${data[i].firstname}${data[i].lastname}</td>
        </tr>
        <tr>
          <th scope="row">Cruise name</th>
          <td>${data[i].cruise_name}</td>
        </tr>
        <tr>
        <th scope="row">Start location</th>
        <td>${data[i].starting_location}</td>
      </tr>
      <tr>
      <th scope="row">Return location</th>
      <td>${data[i].return_location}</td>
    </tr>
        <tr>
          <th scope="row">Price</th>
          <td>${data[i].price}</td>
        </tr>
        <tr>
        <th scope="row">Day</th>
        <td>${data[i].day}</td>
      </tr>
        <tr>
          <th scope="row">Date</th>
          <td>${data[i].cruisedate}</td>
        </tr>
      </tbody>
      <button onclick="deleteOrder(${data[i].orders_id})">Delete</button>
      </div>`
    instr.innerHTML += str
  }
}
displayMemberData();

async function deleteOrder(id) {
  const res = await fetch(`/member/${id}`, {
      method: "DELETE",
  });
  if (res.status === 200) {
      await displayMemberData();
      location.reload()
  } else {
 
  }
}