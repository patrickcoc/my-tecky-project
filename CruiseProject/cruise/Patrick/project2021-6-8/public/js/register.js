document.getElementById("register-form").addEventListener("submit", async function (e) {
    e.preventDefault();
    const form = this;
    const fullName =form["fullName"].value;
    const birthday =form["month"].value+"/"+form["day"].value+"/"+form["year"].value;
    const email = form["email"].value;
    const countryCode = form["countryCode"].value;
    const phone =form["phoneNumber"].value;
    const question =form["question"].value;
    const asnwer =form["answer"].value;
    const createPassword = form["createPassword"].value;
    // const repeatPassword = form["repeatPassword"].value;
    const formObject = { fullName,birthday,email,countryCode,phone,question,asnwer,createPassword}; 
    console.log(formObject)
    const res = await fetch("/register", {
        method: "POST",
        headers: {
            "Content-Type": "application/json; charset=utf-8",
        },
        body: JSON.stringify(formObject),
    });
    const data = await res.json();
    // window.location = `/member.html?id=${data.users.id}`
    if (res.status === 200) {
        window.location = "/member.html";
    } else {
        const data = await res.json();
        const errMessage = data.message;
        alert(errMessage);
    }
});
