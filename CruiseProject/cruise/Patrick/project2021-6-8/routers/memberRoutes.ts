import express from "express";
// import path from "path";
import { client } from "../main";

export const memberRoutes = express.Router();

memberRoutes.post("/member", (req, res) => {
    const inputData = req.body;
    if (!inputData) {
        res.status(400).json({ message: "invalid input" });
        return;
    }
})



memberRoutes.get("/member", async (req, res) => {
    try {
       

        const userId= req.session["user"]
        console.log(userId)
      
        const order = (await client.query(`
        SELECT * FROM order_items 
        INNER JOIN orders ON order_items.orders_id = orders.id
        INNER JOIN cruiseplan ON order_items.cruiseplan_id = cruiseplan.id
        where user_id =${userId.userid};`)).rows
      
    
       res.json(order)
        
    }
    catch (err) {
        console.error(err.message);
        res.status(500).json({ message: "internal server error" })
    }
})

memberRoutes.delete("/member/:id", async (req, res) => {
    try {
        
        const orderID = parseInt(req.params.id);
        console.log(orderID)
        if (isNaN(orderID)) {
            res.status(400).json({ message: "invalid order id" });
            return;
        }
        const result  = await client.query("delete from  order_items where orders_id = $1",[orderID]);
        const result1  = await client.query("delete from  orders where id = $1",[orderID]);
        console.log(result,result1)
      
      
        res.json({ message: "success" });
    } catch (err) {
        console.error(err.message);
        res.status(500).json({ message: "internal server error" });
    }
});