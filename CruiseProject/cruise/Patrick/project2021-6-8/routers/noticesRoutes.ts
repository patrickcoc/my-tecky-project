import express from "express";
import { webkit } from "playwright";
// import { Client } from "pg";
import {client} from"../main";
// import dotenv from "dotenv";

// dotenv.config();

// const client = new Client({
//     database: process.env.DB_NAME,
//     user: process.env.DB_USERNAME,
//     password: process.env.DB_PASSWORD

// });

export const noticesRoutes = express.Router();
(async () => {
    
    const browser = await webkit.launch();
    const page = await browser.newPage();
    await page.goto('https://www.dreamcruiseline.com/zh-hk/notice');
    await page.waitForLoadState("networkidle")
    await page.waitForTimeout(5000)
    // await page.screenshot({ path: `example-chromium.png` });
    const information = await page.evaluate(() => {
        const news = document.getElementsByClassName('notice-item-1')
        const newsresult = [];
        document.title = 'New Title';
        for (let i = 0; i < news.length; i++) {
            const notice: any = news[i].querySelector('.title-list')
            newsresult.push(notice.textContent);
        }
        return newsresult
    });

    //insert data for第一次insert使用，請勿用第2次
    for (let i = 0; i < information.length; i++) {
        information[i] = information[i].replace(/ /g, '')
        information[i] = information[i].replace(/\n/g, '')
        await client.query('INSERT INTO notice (content) values ($1) returning id', [information[i]])
    }
    //insert data for第一次insert使用

   
    await browser.close();
})();


(async () => {
    const browser = await webkit.launch();
    const page = await browser.newPage();
    await page.goto('https://www.royalcaribbean.com/blog');
    await page.waitForLoadState("networkidle")
    
    await page.screenshot({ path: `routers/example-chromium.png` });
    const information = await page.evaluate(() => {
        const blogTitle = document.querySelectorAll('.entry-title.p-name')
        const blog=document.querySelectorAll('.copy.entry-summary.p-summary>p')
        const blogResult = [];
        for (let i = 0; i < blog.length; i++) {
            if(blogTitle[i]){
            blogResult.push(`${blogTitle[i].textContent}${blog[i].textContent}`);
        }}
        return blogResult
    });

    //nsert data for第一次insert使用，請勿用第2次
    for (let i = 0; i < information.length; i++) {   
        information[i] = information[i].replace(/\n/g, '')
        await client.query('INSERT INTO notice (content) values ($1) returning id', [information[i]])
    }
    //insert data for第一次insert使用

   
    await browser.close();
})();    


noticesRoutes.post("/information",(req,res)=>{
    const myContent = req.body.content; 
    if (!myContent) {
        res.status(400).json({ message: "invalid input" });
        return;
    }
})
noticesRoutes.get("/information", async (req, res) => {
    const note:string[] = (await client.query(`SELECT * FROM notice LIMIT 8 OFFSET 0;`)).rows;
    res.json({ note });
});
noticesRoutes.get("/information2", async (req, res) => {
    const note:string[] = (await client.query(`SELECT * FROM notice LIMIT 8 OFFSET 8;`)).rows;
    res.json({ note });
});

