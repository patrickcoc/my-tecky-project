import express from "express";
// import path from "path";
import { client } from "../main";



export const orderRoutes = express.Router();



orderRoutes.post("/checkout",async(req,res)=>{
    try{
        const {planID,date,firstname,lastname,email,address,country,state,cardname,cardnumber,expiration,cvv} = req.body; 
        console.log(req.body)
        const user= req.session["user"]
        const orderId = (await client.query(`
        INSERT INTO orders (user_id,firstname,lastname,email,address,country,state,cardname,cardnumber,expiration,cvv,cruiseplan_id) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12) returning id`,
        [user.userid,firstname,lastname,email,address,country,state,cardname,cardnumber,expiration,cvv,planID])).rows[0];
        await client.query(`
        INSERT INTO order_items (orders_id,cruiseplan_id,cruisedate) values ($1,$2,$3)`,
        [orderId.id,planID,date]);
        console.log("checkout")
      
        res.json({message:"payment success",orderId,user})
    }
    catch (err){
        console.error(err.message);
        res.status(500).json({message:"internal server error"})
    }
})




