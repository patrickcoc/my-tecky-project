import express from "express";
// import path from "path";
import { User } from "../models";
import { client } from "../main";
import fetch from "node-fetch";
// import crypto from "crypto";
// import {hashPassword} from "../hash";



export const userRoutes = express.Router();




userRoutes.post("/register", async (req, res) => {
    try {
        const { fullName, birthday, email, countryCode, phone, question, answer, createPassword } = req.body;
        console.log(req.body)
        const users: User = (await client.query('SELECT * FROM users where email = $1 ', [email])).rows[0];
        const foundUser = users;
        console.log(users)
        console.log(foundUser)
        
        if (!foundUser) {
            const users = (await client.query(`
            INSERT INTO users (fullname,birthday,email,countrycode,phone,question,answer,password)
            values ($1,$2,$3,$4,$5,$6,$7,$8) returning id`,
            [fullName, birthday, email, countryCode, phone, question, answer, createPassword])).rows[0];
            console.log(users)

            // res.redirect("/public/login.html");
            const ID = req.session["user"] = {
                username: users.email,
                userid: users.id
            };
            res.json({ message: "regiseter success", users: users, ID })
        }else{
            res.status(400).json({ message: "email was used" });
            return;
        }
    }    
    catch {
            console.error("error");
            res.status(500).json({ message: "internal server error" })
        }
})

userRoutes.post("/log-in", async (req, res) => {
    const { email, password } = req.body;
    if (!email || !password) {
        res.status(400).json({ message: "missing email/password" });
        return;
    }
    const users: User = (await client.query('SELECT * FROM users where email = $1 and password = $2', [email, password])).rows[0];
    const foundUser = users;
    console.log(users)
    if (!foundUser) {
        res.status(400).json({ message: "invalid email/password" });
        return;
    }
    const ID = req.session["user"] = {
        username: foundUser.email,
        userid: users.id
    };
    // res.redirect("member.html");
    console.log("suceuss")
    res.status(200).json({ message: "/member.html", users: users, ID });
});

userRoutes.get('/login/google', async (req, res) => {
    const accessToken = req.session['grant'].response.access_token;
    // verify返個accesstoken係咪真嘅
    const fetchRes = await fetch('https://www.googleapis.com/oauth2/v2/userinfo', {
        headers: {
            "Authorization": `Bearer ${accessToken}`
        }
    });
    const fetchedUser = await fetchRes.json();
    const users: any[] = (await client.query(`SELECT * FROM users where fullname = $1 and email = $2 `, [fetchedUser.name, fetchedUser.email])).rows;
    const foundUser = users[0];
    console.log(users)
    let newuserid
    if (!foundUser) {
        newuserid = await client.query(`INSERT INTO users (email,fullname,countrycode,phone,password) values ($1,$2,$3,$4,$5) RETURNING id`,
            [fetchedUser.email, fetchedUser.name, 'none', 'none', 'none']);
    }
    // const users:User[] = (await client.query('SELECT * FROM users where username = $1',[fetchedUser.email])).rows;
    // let foundUser = users[0];

    // if(!foundUser){
    //     // Option 1 :幫佢開返, random 一個password
    //     const generatedPassword = crypto.randomBytes(20).toString('hex');
    //     const users = (await client.query('insert into users (username,password) values ($1,$2) returning *'
    //             ,[fetchedUser.email,await hashPassword(generatedPassword)])).rows;

    //     foundUser = users[0];

    const ID = req.session["user"] = {
        username: fetchedUser.email,
        userid: users[0] ? users[0].id : newuserid ,

    };
    console.log(ID);
    res.redirect('/member.html');
    // res.status(200).json({ message: "/member.html", users: users, ID })
})


userRoutes.get('/logout', async (req, res) => {
    req.session.destroy(() => {
        res.redirect('/index.html');
    });
});





