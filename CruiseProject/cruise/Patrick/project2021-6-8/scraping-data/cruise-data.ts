// import express from "express";
import { webkit } from "playwright";
import { Client } from "pg";
import dotenv from "dotenv";
import {Information} from "../models"
dotenv.config();

const client = new Client({
  database: process.env.DB_NAME,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD

});

(async () => {
  await client.connect();
  const browser = await webkit.launch();
  const page = await browser.newPage();
  await page.goto('https://hk.dreamcruiseline.com/swift/cruise?siid=260398&lang=18');
  await page.waitForLoadState("networkidle")
  await page.screenshot({ path: `scraping-data/example-chromium.png` });

  const information: Information = await page.evaluate(() => {
    const curiseName: any = document.querySelectorAll('.cruise-name.ships-view-more')
    const curiseNameResult = [];
    for (let i = 0; i < curiseName.length; i++) {
      const name: any = curiseName[i].querySelector('span')
      curiseNameResult.push(name.textContent);
    }

    const destination: any = document.querySelectorAll('.cruise-location-item-inner.flex-spacer')
    const startingLocationResult = []
    const returnLocationResult = []
    for (let i = 0; i < destination.length; i++) {
      const aim = destination[i].textContent.length / 2
      const target = destination[i].textContent.slice(aim)
      const backTarget = destination[i].textContent.slice(0, aim)
      startingLocationResult.push(target);
      returnLocationResult.push(backTarget);
    }

    const price: any = document.querySelectorAll('.cruise-low-price')
    const priceResult = [];
    for (let i = 0; i < price.length; i++) {
      priceResult.push(price[i].textContent);
    }

    const day: any = document.querySelectorAll('.cruise-info-detail-item-inner.text-nowrap')
    const dayResult = [];
    for (let i = 0; i < day.length; i++) {
      dayResult.push(day[i].textContent);
    }

    const curises = document.querySelectorAll('table.table.striped')
    const dateArr = []
    // dateArr.push(curises.item(1).querySelectorAll('tr').item(1)?.querySelectorAll('td')[1].textContent)  
    for(let i = 0; i < curises.length;i++){
      let curiseArr = []
      const tr = curises.item(i).querySelectorAll('tr')
      for(let i = 0; i < tr.length;i++){
        const data = tr.item(i).querySelectorAll('td')
        if(data){
          if(data[1]){
            curiseArr.push(data[1].textContent)
          }
        }
      }
      dateArr.push(curiseArr)
    }
    return {
      curisename: curiseNameResult,
      startingLocation: startingLocationResult,
      returnLocation: returnLocationResult,
      price: priceResult,
      day: dayResult,
      date: dateArr

    }
  });
  console.log(information)
  let infoArr = []
  for (let i = 0; i < information.curisename.length; i++) {
    infoArr.push({
      curisename: information.curisename[i],
      startingLocation: information.startingLocation[i],
      returnLocation: information.returnLocation[i],
      price: information.price[i],
      day: information.day[i],
      date:information.date[i]
    })
  }

  // insert new plan data

  for (const plan of infoArr) {
    let inserplan = await client.query('INSERT INTO cruiseplan (merchants_id,cruise_name,date_of_sailing,starting_location,return_location,price,day) values ($1,$2,$3,$4,$5,$6,$7) returning id', 
    [1,plan.curisename,plan.date,plan.startingLocation,plan.returnLocation,plan.price,plan.day])
    console.log(inserplan)
    }
  
  await browser.close();
})();



(async () => {
  
  const browser = await webkit.launch();
  const page = await browser.newPage();
  await page.goto('https://www.royalcaribbean.com/hkg/zh/cruises/?currentPage=5&destinationRegionCode_FAR.E=true');
  await page.waitForLoadState("networkidle")
  await page.screenshot({ path: `scraping-data/royal-chromium.png` });

  const information: Information = await page.evaluate(() => {
    const curiseName: any = document.querySelectorAll('.itinerary-name')
    const curiseNameResult = [];
    for (let i = 0; i < curiseName.length; i++) {
      curiseNameResult.push(curiseName[i].textContent);
    }
    const destination: any = document.querySelectorAll('.itinerary-description')
    const startLocationResult = [];
    const returnLocationResult = [];
    for (let i = 0; i < curiseName.length; i++) {
      const startLocation: any = destination[i].querySelector('b')
      const target = startLocation.textContent.replace(/ /g, '').replace(/\(/g, '').replace(/\)/g, '')
      const remove= target.slice(0,target.length-1)
      const location = remove.split(',')
      console.log(target)
      console.log(location)
      for(let i=0;i<location.length;i++){
        if(i%2==1){
          startLocationResult.push(location[i])
        }else{
          returnLocationResult.push(location[i])
        }
      }
    }


    const price: any = document.querySelectorAll('.price-value')
    const priceResult = [];
    for (let i = 0; i < price.length; i++) {
      priceResult.push(`HK$${price[i].textContent}.00`);
    }

        const day: any = document.querySelectorAll('.itinerary-duration')
    const dayResult = [];
    for (let i = 0; i < day.length; i++) {
      dayResult.push(day[i].textContent.replace(/數/g, ''));
    }

    const date: any = document.querySelectorAll('.default-date-value')
    const dateResult = [];
    for (let i = 0; i < date.length; i++) {
      const target = date[i].textContent.replace(/ /g, '')
      const cutDate = target.split(',')
        const lastDate= (`${cutDate[1]}年${cutDate[0]}日`)
        dateResult.push(lastDate)
      
    }
    return {
      curisename: curiseNameResult,
      startingLocation: startLocationResult,
      returnLocation: returnLocationResult,
      price: priceResult,
      day: dayResult,
      date: dateResult
    }
  });

  let infoArr2 = []
  for (let i = 0; i < information.curisename.length; i++) {
    infoArr2.push({
      curisename: information.curisename[i],
      startingLocation: information.startingLocation[i],
      returnLocation: information.returnLocation[i],
      price: information.price[i],
      day: information.day[i],
      date:information.date[i]
    })
  }
  //   // insert new plan data

  for (const plan of infoArr2) {
    let inserplan = await client.query('INSERT INTO cruiseplan (merchants_id,cruise_name,date_of_sailing,starting_location,return_location,price,day) values ($1,$2,$3,$4,$5,$6,$7) returning id', 
    [2,plan.curisename,plan.date,plan.startingLocation,plan.returnLocation,plan.price,plan.day])
    console.log(inserplan)
  }
  console.log(information)
  await browser.close();
})();
