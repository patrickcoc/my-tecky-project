
import './App.css';

import { LoginPage } from './components/Login/login';
import Home from './components/Home/Home';
import BottemBar from "../src/components/Home/bottemBar"


function App() {
  return (
    <div className="App">
      <header>
        {/* <NavBar /> */}
        <LoginPage />
        <Home />
      </header>
      <BottemBar />
    </div>
  );
}

export default App;
