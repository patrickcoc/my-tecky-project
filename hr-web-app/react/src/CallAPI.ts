
export async function callAPI<Input, Output>(
    method: 'GET' | 'POST',
    url: string,
    body?: Input,
  ): Promise<Output> {
    const { REACT_APP_API_URL } = process.env
    if (!REACT_APP_API_URL) {
      throw new Error('Missing REACT_APP_API_URL in env')
    }
    const res = await fetch(REACT_APP_API_URL + url, {
      method,
      headers: {
        'Content-Type': 'application/json', 
        Authorization: `Bearer ${localStorage.getItem('token')}`
      },
      body: JSON.stringify(body),
    })
    const json = await res.json()
    if (json.error) {
      throw new Error(json.error)
    }
    return json.data
  }