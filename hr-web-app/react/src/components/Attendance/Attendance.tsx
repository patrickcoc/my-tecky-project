import { useEffect, useState } from 'react'
import { callAPI } from '../../CallAPI'
import { CreateCalender, dateFormat } from '../Calendar/calendar'
import TableContainer from './TableContainer'
import "bootstrap/dist/css/bootstrap.min.css"
import './Attendance.scss'
import { CreateMonthCalender, dateMonthFormat } from '../Calendar/MonthCalender'


function AttendanceRecord() {
    const { startDate, setStartDate, calendar } = CreateCalender()
    const { Monthcalendar, startMonthDate, setStartMonthDate } = CreateMonthCalender()
    type options = {
        time: string;
        id: string;
        attendance_date: string,
        staff_id: string,
        in_out: string
    }
    const [updateData, setUpdateDate] = useState<any>()
    const [newData, setNewDate] = useState<any>()
    const [message, setMessage] = useState<string>()
    const [off, setOff] = useState(true)
    const [staff_id, setStaff_id] = useState<any>('')
    const [updateTime, setUpdateTime] = useState<any>('')
    const [dateInput, setDateInput] = useState<any>('')
    const [newIn_out, setIn_out] = useState<any>('in')
    const updateBy = localStorage.getItem('user_id')
    const [searchMonth, setSearchMonth] = useState(false)
    const [data, setData] = useState([])

    const inputDate = {
        staff_id: staff_id,
        attendance_date: dateInput.replace(/-/g, '/'),
        time: updateTime,
        in_out: newIn_out,
        update_by: updateBy
    }

    useEffect(() => {
        if (!searchMonth) {
            if (startDate) {
                const doFetch = async () => {
                    callAPI<{ date: string; }, any>('POST', '/get_attendance', {
                        date: dateFormat(startDate)
                    }).then(data => setData(data))
                }
                doFetch()
            }
            else{return}
        }
        else{return}
    }, [startDate])

    useEffect(() => {
        if (searchMonth) {
            if (startMonthDate) {
                callAPI<{ date: string }, any>('POST', '/get_month_attendance', {
                    date: dateMonthFormat(startMonthDate)
                }).then(data => setData(data))
            }
            else{return}
        }
        else{return}
    }, [startMonthDate])

    useEffect(() => {
        if (newData) {
            callAPI<any, null>('POST', '/del_attendance', {
                data: newData
            })
            if (!searchMonth) {
                callAPI<{ date: string; }, any>('POST', '/get_attendance', {
                    date: dateFormat(startDate)
                }).then(data => setData(data))
            }
            if (searchMonth) {
                if (startMonthDate) {
                    callAPI<{ date: string }, any>('POST', '/get_month_attendance', {
                        date: dateMonthFormat(startMonthDate)
                    }).then(data => setData(data))
                }
                else{return}
            }
            else{return}
        }
        else{return}
    }, [newData]);

    function update() {
        callAPI<any, any>('POST', '/add_attendance', {
            data: inputDate
        }).then(data => setMessage(data.message))
        callAPI<{ date: string; }, any>('POST', '/get_attendance', {
            date: dateFormat(startDate)
        }).then(data => setData(data))

        setStaff_id('')
        setUpdateTime('')
        setDateInput('')
    }

    const columns: any[] = [
        {
            Header: 'Dep',
            accessor: 'department_id',
        },
        {
            accessor: 'staff_id',
            Header: 'Staff',
        },
        {
            accessor: 'attendance_date',
            Header: 'Date',
        },
        {
            accessor: 'time',
            Header: 'Time',

        },
        {
            accessor: 'in_out',
            Header: 'In/Out',
        },
        {
            accessor: (value: options) => {
                const first = <div><span onClick={() => { setNewDate(value) }}><i className="fas fa-minus-circle"></i></span></div>
                {/* <span onClick={() => { setUpdateDate(value) }}><i className="fas fa-pen-square" onClick={closeClick} ></i></span></div> */ }
                return (first)
            },
            Header: 'For Admin',
            disableFilters: true,
            disableSortBy: true
        },
    ]

    function closeClick() {
        setOff(!off)
        setMessage('')
    }
    function changeDayMode() {
        setSearchMonth(false)
    }
    function changeMonthMode() {
        setSearchMonth(true)
    }
    return (
        <div className="att_page">
            <div className="att_content">
                <div className="calenderSelect">
                    <span onClick={changeDayMode}>{calendar}</span>
                    <span onClick={changeMonthMode}>{Monthcalendar}</span>
                    <i className="fas fa-plus-circle" onClick={closeClick}></i>
                </div>
                <>
                    {off ? (<></>) : (
                        <>
                            <div className='addAttendance'>
                                <div className='addForm'>
                                    <i className="closeBtn fas fa-times-circle" onClick={closeClick}></i>
                                    <label>Staff id:<input type='text' value={staff_id} onChange={event => setStaff_id(event.target.value)}></input></label>
                                    <label>Attendance date:<input type='date' value={dateInput} onChange={event => setDateInput(event.target.value)} ></input></label>
                                    <label >Time:<input style={{ width: '120px' }} type='time' value={updateTime} onChange={event => setUpdateTime(event.target.value)} ></input></label>
                                    <label className="in_out">In/Out:<select onChange={event => setIn_out(event.target.value)} >
                                        <option value='in'>in</option>
                                        <option value='out'>out</option>
                                    </select>
                                    </label>
                                    <button className='updateAttBtn' onClick={update}>update</button>
                                    {message?(<p className="message_add_att">{message}</p>):(null)}
                                </div>
                            </div>
                        </>
                    )}

                </>
            </div>
            <>
                {startDate ? (
                    <>
                        {data ? (<TableContainer columns={columns} data={data} />) : (<></>)}
                    </>
                ) : (<></>)}

            </>
        </div >
    )
}
export default AttendanceRecord