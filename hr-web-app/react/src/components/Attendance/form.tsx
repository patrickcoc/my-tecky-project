import React, { useState, ChangeEvent } from 'react'

export function useForm(fieldList: string[][]) {
  const [state, setState] = useState<any>({})
  let table = (
    <table>
      <tbody>
        {fieldList.map(([field, type]) => {
          function onChange(
            e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
          ) {
            setState({
              ...state,
              [field]: e.target.value,
            })
          }
          return (
            <tr key={field}>
              <td className="title">{field}:</td>
              <td>
                {type === 'textarea' ? (
                  <textarea value={state[field]} onChange={onChange} />
                ) : (
                  <input value={state[field]} type={type} onChange={onChange} />
                )}
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
  return { state, setState, table }
}