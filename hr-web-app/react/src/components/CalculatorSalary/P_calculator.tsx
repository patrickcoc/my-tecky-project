import React, { useEffect, useState } from 'react'
import { Link, Route, Switch, useLocation, useRouteMatch } from 'react-router-dom'
import { callAPI } from '../../CallAPI'
import TableContainer from '../Attendance/TableContainer';
import { CreateMonthCalender, dateMonthFormat } from '../Calendar/MonthCalender';
import './P_calculator.scss'

type data = {
  staff_id: string,
  name: string,
  salary: any,
  attendance_date: string,
  in_out: string,
  time: string
}
function calculatorForMin(time: string) {
  const hour = time.split(':')[0]
  const min = time.split(':')[1]
  const total = (parseInt(hour) * 60) + parseInt(min)
  return total
}

function Calculator() {
  const [userData, setData] = useState<data[]>([]);
  const [counts, setcount] = useState<number>(0)
  const [OTcounts, setOTcount] = useState<number>(0)
  let countArr: number[] = []
  let OTcountArr: number[] = []

  const [salary, setSalary] = useState<number>(0)
  const [lateHour, setlateHour] = useState<string>('09:00')
  const [offHour, setOffHour] = useState<string>('18:00')
  const [overtime_pay, setOverTimePay] = useState<number>(0)
  const [hourly_rate, setHourlyRate] = useState<number>(0)
  const [leave, setLeave] = useState<number>(0)

  const [salaryResult, setSalaryResult] = useState<number>(salary)
  const location = useLocation()
  const { Monthcalendar, startMonthDate, setStartMonthDate } = CreateMonthCalender()
  const params = new URLSearchParams(location.search)
  const staff_id = useRouteMatch().params

  useEffect(() => {
    if (startMonthDate) {
      callAPI<any, any>('POST', '/calculator_salary', {
        user: staff_id,
        date: dateMonthFormat(startMonthDate)
      }).then(data => setData(data))
    }
  }, [staff_id, startMonthDate]);

  const columns: any[] = [
    {
      accessor: 'staff_id',
      Header: 'Staff',
    },
    {
      Header: 'Name',
      accessor: 'name',
    },
    {
      accessor: 'attendance_date',
      Header: 'Date',
    },
    {
      accessor: 'time',
      Header: 'Time',

    },
    {
      accessor: 'in_out',
      Header: 'In/Out',
    },
    {
      accessor: (value: any) => {
        if (value.in_out === "in") {
          const inHour = calculatorForMin(value.time)
          const result = inHour - calculatorForMin(lateHour)
          if (result > 0) {
            countArr.push(result)
            const resultForHour = result / 60
            const first = `${result}min/${resultForHour.toFixed(1)}Hour`

            return (first)
          }
        }
      },
      Header: 'Late by Min/Hour',
    },
    {
      accessor: (value: any) => {
        if (value.in_out === "out") {
          const outHour = calculatorForMin(value.time)
          const result = outHour - calculatorForMin(offHour)
          if (result > 0) {
            OTcountArr.push(result)
            const resultForHour = result / 60
            const first = `${result}min/${resultForHour.toFixed(1)}Hour`
            return (first)
          }
        }
      },
      Header: 'OT by Min/Hour',
    },
  ]

  useEffect(() => {
    if (userData[0]) {
      setSalary(userData[0].salary)
    }
  }, [userData]);

  useEffect(() => {
    let x = 0
    for (let i = 0; i < countArr.length; i++) {
      x = x + countArr[i]
      setcount(x)
    }
  }, [countArr]);

  useEffect(() => {
    let x = 0
    for (let i = 0; i < OTcountArr.length; i++) {
      x = x + OTcountArr[i]
      setOTcount(x)
    }
  }, [OTcountArr]);

  useEffect(() => {
    const calculator = salary + (overtime_pay * (OTcounts / 2 / 60)) - (leave * (hourly_rate * 24))
    setSalaryResult(calculator)
  }, [overtime_pay, OTcounts, salary, leave, hourly_rate]);
  return (

    <div className="p_calculator_page"> 
      <div className="calculator_data">

        {userData ? (<label className="staff_text">Salary:$   {salary}</label>) : null}
        <label className="staff_text lateText"> Late: {counts / 2} min/ {(counts / 2 / 60).toFixed(1)} hour </label><label className="staff_text OTtext">OT: {OTcounts / 2} min/ {(OTcounts / 2 / 60).toFixed(1)} hour</label>
        <label className="calculator_result">Result:$ {salaryResult}</label>
      </div>
      <div className="calculator_input">
        <label className="calculator_monthCalender">Select Month: {dateMonthFormat(startMonthDate)}{Monthcalendar}</label>
        <label>Overtime pay(per hour):<input type="number" value={overtime_pay} step="0.1" onChange={event => setOverTimePay(parseFloat(event.target.value))} ></input></label>
        <label>Hourly rate(per hour)：<input type="number" value={hourly_rate} onChange={event => setHourlyRate(parseFloat(event.target.value))}></input></label>
        <label>Leave day(per day):<input type="number" value={leave} step="0.5" onChange={event => setLeave(parseFloat(event.target.value))}></input></label>
        <label>Set working hours:<input type="time" value={lateHour} onChange={event => setlateHour(event.target.value)}></input></label>
        <label>Set off hours:<input type="time" value={offHour} onChange={event => setOffHour(event.target.value)}></input></label>
      </div>


      {userData ? (<TableContainer columns={columns} data={userData} />) : (<></>)}
    </div>

  )
}
export default Calculator