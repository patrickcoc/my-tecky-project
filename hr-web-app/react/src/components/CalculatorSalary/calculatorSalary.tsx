import React, { useState } from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Switch, Route, Link, useLocation } from "react-router-dom";
import { RootState } from "../../redux";
import {
  getDepartmentNameThunk,
  getDepartmentStaffListThunk,
} from "../../redux/calculatorSalary/thunk";
import { selectDepartment } from "../../redux/calculatorSalary/action";
import TableContainer from "../Attendance/TableContainer";
import Calculator from "./P_calculator";
import { callAPI } from "../../CallAPI";
import { Button } from "react-bootstrap";

type data = {
  name: string,
  staff_id: string,
  salary: string,
  checkbox: boolean,
  remark: string
}
function CalculatorSalaryPage() {
  const dispatch = useDispatch();
  const [getNew, setGetNew] = useState<boolean>(true)
  const [inputBar, setInputBar] = useState<boolean>(false)
  const [inputContent, setInputContent] = useState<string>(' ')
  const [staffId, setStaffId] = useState<string>(' ')
  const [check, setCheck] = useState<boolean>(false)
  const [message, setMessage] = useState<string|null>(null)
  let departmentList = useSelector((state: RootState) => state.CalculatorSalary);

  let selectedDepartmentId = useSelector(
    (state: RootState) => state.CalculatorSalary.selectedDepartmentId
  );
  let selectedDepartmentName = useSelector(
    (state: RootState) =>
      state.CalculatorSalary.departmentList.find(
        (d) => d.id === selectedDepartmentId
      )?.department_name
  );
  let staffList = useSelector(
    (state: RootState) => state.CalculatorSalary.staffList
  );
  const location = useLocation()
  const params = new URLSearchParams(location.search)
  // get a department list from user
  useEffect(() => {
    getDepartmentList();
  }, []);

  useEffect(() => {
    dispatch(getDepartmentStaffListThunk(selectedDepartmentId));
  }, [getNew])

  const getDepartmentList = () => {
    dispatch(getDepartmentNameThunk());
  };

  const handleChange = async (event: any) => {
    dispatch(selectDepartment(+event.target.value));
    dispatch(getDepartmentStaffListThunk(+event.target.value));
  };
  function onChangeBox() {
    setCheck(!check)
  }
  function hiddenBox() {
    setInputBar(false)
  }
  async function addRemark() {

    callAPI<any, any>('POST', '/add_salary_remark', {
      remark: inputContent,
      id: staffId,
      checkBox: check
    }).then(data => {
      setMessage(data.message)
      setGetNew(!getNew)
    })
  }
  const columns: any[] = [
    {
      accessor: 'staff_id',
      Header: 'Staff',
    },
    {
      Header: 'Name',
      accessor: 'name',
    },

    {
      accessor: 'salary',
      Header: 'Salary',
    },
    {
      accessor: (value: data) => {
        const first = (<div>
          <Link to={`/admin_salary/calculator/${value.staff_id}`}>
            <span><i className="fas fa-calculator"></i></span>
          </Link>

        </div>)
        return first
      },
      Header: 'For Calculator',
      disableFilters: true,
      disableSortBy: true
    },
    {
      accessor: (value: data) => {
        const result = value.remark
        if (result) {
          const first = (<div className="remarkContent">
            <label >{result}</label>
            <label onClick={changeShow}><span onClick={() => { setInputContent(value.remark) }}>
              <i className="fas fa-plus-circle" onClick={() => { setStaffId(value.staff_id) }}></i>
            </span></label>
          </div>)
          return first
        }
        else {
          return (<div className="remarkContent"><label onClick={reSet}><span>
            <i className="fas fa-plus-circle" onClick={() => { setStaffId(value.staff_id) }}></i>
          </span></label></div>)
        }
      },
      Header: 'Remark',
      disableFilters: true,
      disableSortBy: true
    },
    {
      accessor: (value: data) => {
        const result = value.checkbox
        if (result) {
          const first = (<div className="checkBox">
            <input
              type="checkbox"
              value="ture"
              onChange={onChangeBox}
              checked={true}
            />
          </div>)
          return first
        }
        else {
          return (<> <input
            type="checkbox"
            value="fasle"
            onChange={onChangeBox}
            checked={false}
          /></>)
        }
      },
      Header: 'Check Box',
      disableFilters: true,
      disableSortBy: true
    },
  ]
  function changeShow() {
    setInputBar(true)
    setMessage(null)
  }
  function reSet() {
    setInputBar(true)
    setInputContent(' ')
    setMessage(null)
  }
  return (
    <Switch>
      <Route path={`/admin_salary/calculator/:id`} component={Calculator} />
      <Route path="/admin_salary" exact>
        <Route path="/admin_page" exact></Route>
        <div className="calculator_page">
          <div className="salary_bar">
            {/* <Link to='/admin_page'><i className="fas fa-arrow-circle-left gobackSalary"></i></Link> */}
            <select onChange={handleChange}>
              {departmentList.departmentList ? (
                departmentList.departmentList.map((department, i) => (
                  <option value={department.id} key={department.id}>
                    {department.department_name}
                  </option>
                ))
              ) : (
                <div className="StaffList">hi</div>
              )}
            </select>
            {inputBar ? (<div className="remarkBox">
            <span className="closebtn" onClick={hiddenBox}>×</span>
            <label>Staff id:    {staffId}</label>

            <label>Remark: <input className="the_content" type='text'
              value={inputContent} onChange={event => setInputContent(event.target.value)}></input></label>
            <label> Check:<input
              type="checkbox"
              value="fasle"
              onChange={event => setCheck(event.target.checked)}
            /></label>
            <button  className="addBtn" onClick={addRemark}><i className="far fa-bookmark"></i>Add</button>
            {message?(<p className="addMessage">{message}</p>):(null)}
          </div>) : (null)}

          </div>
   


          <div className="selectedDepartmentTitle">

            {staffList ? (<TableContainer columns={columns} data={staffList} />) : (<></>)}
          </div>
        </div>
      </Route>
    </Switch>
  );
}

export default CalculatorSalaryPage;