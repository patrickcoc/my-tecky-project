import DatePicker from "react-datepicker";
import React, { useState } from 'react';
export function dateMonthFormat(date: any ) {
    if(typeof date.getMonth === 'function' ){
        let year = date.getFullYear()
        let month: string | number = date.getMonth() + 1
        if (month < 10) {
            month = "0" + month
        }
        const newDate = year + "/" + month

        return newDate
    }
    else{return date}
}

export const CreateMonthCalender = () => {
    const [startMonthDate, setStartMonthDate] = useState<any>(new Date);
    // const [endDate, setEndDate] = useState<any>(new Date("2014/04/08"));
    let Monthcalendar = (
        <>
            <DatePicker
                selected={startMonthDate}
                onChange={(date) => setStartMonthDate(date)}
                selectsStart
                startDate={startMonthDate}
                // endDate={endDate}
                dateFormat="MM/yyyy"
                showMonthYearPicker

            />
            <span className="select_month">Select Month:  {dateMonthFormat(startMonthDate)}</span>

            {/* <DatePicker
                selected={endDate}
                onChange={(date) => setEndDate(date)}
                selectsEnd
                startDate={startMonthDate}
                endDate={endDate}
                dateFormat="MM/yyyy"
                showMonthYearPicker
            /> */}
        </>
    );
    return { Monthcalendar, startMonthDate, setStartMonthDate }
};