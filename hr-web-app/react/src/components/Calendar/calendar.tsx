import React, { useState } from 'react';
import './calendar.scss'
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

export function dateFormat(date: Date) {

  let year = date.getFullYear()
  let month: string | number = date.getMonth() + 1
  if (month < 10) {
    month = "0" + month
  }
  let day: string | number = date.getDate()
  if (day < 10) {
    day = "0" + day
  }
  const newDate = year + "/" + month + "/" + day

  return newDate
}

export const CreateCalender = () => {

  const [startDate, setStartDate] = useState(new Date());

  let calendar = (
    <div>

      <DatePicker
        // filterDate={d => {
        //   return new Date() < d;
        // }}
        selected={startDate}
        onChange={(date: Date) => setStartDate(date)}
        dateFormat="MMMM d, yyyy"
        isClearable={true}
        showDisabledMonthNavigation
      />

      <div>Selected Date : {startDate ? dateFormat(startDate) : null}</div>
    </div>
  )
  return { startDate, setStartDate, calendar }
};
