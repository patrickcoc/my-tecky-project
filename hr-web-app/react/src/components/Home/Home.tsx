import { NavLink, Switch, Route, Redirect, Link } from 'react-router-dom'
// import HomePage from '../../pages/HomePage/HomePage'
import UserPage from '../../pages/UserPage/UserPage'
import RosterPage from '../../pages/RosterPage/RosterPage'
import RecordPage from '../../pages/LeavePage/RecordPage'
import AttendacePage from '../../pages/AttendancePage/AttendancePage'
import AdminPage from '../../pages/AdminPage/AdminPage'
import QrPage from '../../pages/QrPage/QrPage'
import UserAttendancePage from '../UserAttendance/U_attendance'
import AttendanceRecord from '../Attendance/Attendance'
import { PersonalForm } from '../PersonalForm/P_Form'
import SearchContent from '../Personal/SearchContent'
import CalculatorSalaryPage from '../CalculatorSalary/calculatorSalary'
import AdminRoster from '../Roster/AdminRoster'
import Scanner from '../../pages/AdminPage/ScannQrPage'
import { EditLeave } from '../Leave/EditLeave'
import HomePage from './HomeList'
import QrCode from '../../pages/QrPage/QRcode'
import { UserDetail } from '../Personal/UserDetail'
import { LeaveDetail } from '../Leave/LeaveDetail'
import NoMatch from '../../pages/NoMatch/NoMatch'
import { useSelector } from 'react-redux'
import { RootState } from '../../redux'

function Page() {
    const isAuthenticated = useSelector(
        (state: RootState) => state.auth.isAuthenticated,
      )
    return (
        <>
            <div>
        
                <Switch>
              
                     <Route path="/users" component={UserPage} />
                    <Route path="/roster" component={RosterPage} />
                    <Route path="/leave_detail/:id" component={LeaveDetail} />
                    <Route path="/leave" component={RecordPage} />
                    <Route path="/attendance" component={UserAttendancePage} />
                    <Route path="/admin_page" component={AdminPage} />
                    <Route path="/qr_check" component={QrPage} />
                    <Route path="/admin_attendance" component={AttendanceRecord} />
                    <Route path="/admin_department" component={PersonalForm} />
                    <Route path="/admin_personal" component={SearchContent} />
                    <Route path="/admin_leave" component={EditLeave} />
                    <Route path="/admin_salary" component={CalculatorSalaryPage} />
                    <Route path="/admin_roster" component={AdminRoster} />
                    <Route path="/admin_scanner" component={Scanner} />
                    <Route path="/QR/:type" component={QrCode} />
                    <Route path="/home_page" component={HomePage} />
                    <Route path="/personal/:id" component={UserDetail} />
                    {isAuthenticated? (<Route  component={NoMatch} />):(null)}
                
                </Switch> 
            </div>
        </>
    )
}


export default Page