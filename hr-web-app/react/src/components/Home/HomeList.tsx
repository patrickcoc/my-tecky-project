import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { Link, Redirect } from "react-router-dom"
import { RootState } from "../../redux"
import { logoutThunk } from "../../redux/login"
import "./HomeList.scss"
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import { dateFormat } from "../Calendar/calendar"
import SliderBar from "./Slider"


function HomePage() {
    const username = localStorage.getItem('username')
    const [admin, setAdmin] = useState(false)
    const [dateState, setDateState] = useState(new Date());
    // useEffect(() => {
    //     setInterval(() => setDateState(new Date()), 30000);
    // }, []);

    const isAuthenticated = useSelector(
        (state: RootState) => state.auth.isAuthenticated,
    )
    const isAdmin = useSelector(
        (state: RootState) => state.auth.isAdmin,

    )
    const check = localStorage.getItem('token')
    useEffect(() => {
        if (isAdmin) {
            setAdmin(true)
        }
    }, [isAdmin])


    const dispatch = useDispatch()

    if (!isAuthenticated) {
        return <Redirect to="/login" />
    }
    function logout() {
        dispatch(logoutThunk())
        setAdmin(false)
    }


    return (
        <div className="HomePage">
            <SliderBar/>

            <div className="HomeBtn">
                <div><Link className='link' to="/roster"><i className="far fa-calendar"></i>Roster</Link></div>
                <div> <Link className='link' to="/qr_check"><i className="fas fa-qrcode"></i>QR code</Link></div>
                <div> <Link className='link' to="/leave"><i className="fas fa-head-side-cough"></i>Leave</Link></div>
                <div> <Link className='link' to="/users"><i className="fas fa-user"></i>User</Link></div>
                <div><Link className='link' to="/attendance"> <i className="fas fa-user-clock"></i>Attendance</Link></div>

                {admin ? (
                    <div><Link className='link' to="/admin_page"><i className="fas fa-user-cog"></i>Admin</Link></div>
                ) : (<></>)}

            </div>

            <div>

            </div>
        </div>
    )
}
export default HomePage

