import { useEffect, useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import {  Redirect } from "react-router-dom"
import { RootState } from "../../redux"
import { logoutThunk } from "../../redux/login"
import Slider from "react-slick";



function SliderBar() {
    const username = localStorage.getItem('username')
    const [admin, setAdmin] = useState(false)
    const [dateState, setDateState] = useState(new Date());

    const isAuthenticated = useSelector(
        (state: RootState) => state.auth.isAuthenticated,
    )
    const isAdmin = useSelector(
        (state: RootState) => state.auth.isAdmin,

    )
    const check = localStorage.getItem('token')
    useEffect(() => {
        if (isAdmin) {
            setAdmin(true)
        }
    }, [isAdmin])


    const dispatch = useDispatch()

    if (!isAuthenticated) {
        return <Redirect to="/login" />
    }
    function logout() {
        dispatch(logoutThunk())
        setAdmin(false)
    }
    var settings = {
        dots: false,
        infinite: true,
        speed: 1000,
        slidesToShow: 1,
        slidesToScroll: 1
    };

    return (
        <div className="sliderBar">
            <Slider {...settings}>
                <div>
                    <p>Welcome~  {username}     <span> <i className="fas fa-sign-out-alt" onClick={logout}></i></span></p>
                </div>
                <div className="date_clock">
                    <p>

                        <i className="far fa-calendar"></i>
                        {dateState.toLocaleDateString('en-GB', {
                            day: 'numeric',
                            month: 'short',
                            year: 'numeric',
                        })}
                        <span>
                            <i className="far fa-clock"></i>
                            {dateState.toLocaleString('en-US', {
                                hour: 'numeric',
                                minute: 'numeric',
                                hour12: true,
                            })}
                      
                        </span>
                        <span> <i className="fas fa-sign-out-alt" onClick={logout}></i></span></p>
                </div>
                <div >
                    <p> 未收工牙，仲望<span> <i className="fas fa-sign-out-alt" onClick={logout}></i></span></p>
                </div>



            </Slider>
        </div>
    )
}
export default SliderBar