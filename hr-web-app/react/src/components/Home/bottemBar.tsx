import { useState } from "react";
import { Link, useHistory } from "react-router-dom"

function BottemBar() {
    const history = useHistory();

    return (
        <>

            <div className="bottemBar">
                <label onClick={() => history.goBack()}><i className="fas fa-arrow-circle-left"></i></label>
                <Link className='link' to="/home_page"><i className="fas fa-home"></i></Link>
                <ScrollButton />
                </div>

        </>
    )
}


export default BottemBar

export const ScrollButton = () => {

    const [visible, setVisible] = useState(false)

    const toggleVisible = () => {
        const scrolled = document.documentElement.scrollTop;
        if (scrolled > 300) {
            setVisible(true)
        }
        else if (scrolled <= 300) {
            setVisible(false)
        }
    };

    const scrollToTop = () => {
        window.scrollTo({
            top: 0,
            behavior: 'smooth'
        });
    };

    window.addEventListener('scroll', toggleVisible);
    return (
       
            <label onClick={scrollToTop}
                style={{ display: visible ? 'inline' : 'none' }}><i className="fas fa-arrow-circle-up"></i></label>
        
    );

}