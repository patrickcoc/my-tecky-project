import useTable from '../../hooks/useTable'
import { IonButton } from '@ionic/react'
import './ApplyLeave.scss'
import { callAPI } from '../../CallAPI'
import { LeaveRecord } from '../../../../server/models'

function ApplyLeave() {
    const { state, setState, table } = useTable(fieldList)
    const submit = () => {
      
        if (!state["Leave Type"] || !state["Leave Date"] || !state.is_all_day || !state.is_all_day) {
            alert("Please Fill In All required Fields!")
            return
        } else {
            callAPI<any, null>("POST", "/apply_leave", {
                leave_type: state["Leave Type"],
                leave_date: state["Leave Date"],
                is_all_day: state.is_all_day,
                remark: state.Remark
            }).then(data => {
            
                alert("Submitted!")
            }).catch(err => {
                console.log(err)
            })
        }
    }
    return <>
        <div className="page_title">
            <h1>Leave Form</h1>
        </div>
        <div className="leave-table">
            {table}
        </div>
        <div className="ion-padding">
            <IonButton onClick={submit}>submit</IonButton>
            <div>
            </div>
        </div>
    </>
}
const fieldList = [
    ['Leave Type', 'options',
        ', rest day, sick leave, casual leave, annual leave, non-pay leave, maternity leave, paternity leave, others'
    ],
    ['Leave Date', 'date'],
    ['', 'radios', 'All Day,Half Day'],
    ['Remark', 'textarea'],
]
export default ApplyLeave