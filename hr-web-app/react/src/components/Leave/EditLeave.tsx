import React, { useState, useEffect } from 'react';
import { callAPI } from '../../CallAPI';
import './EditLeave.scss'
import Swal from 'sweetalert2'
import { Link, Route, Switch } from 'react-router-dom';

type AllUsersLeave = {
    id: number,
    staff_id: string,
    name: string,
    leave_date: string,
    leave_type: string,
    is_all_day: string,
    application_status: string
}

export function EditLeave() {
    const [allUsersLeaveList, setAllUsersLeaveList] = useState<AllUsersLeave[]>(
        [
            {
                id: 123,
                staff_id: "12340",
                name: "Isaac",
                leave_date: "2021-09-30T16:00:00.000Z",
                leave_type: "sick leave",
                is_all_day: "1",
                application_status: "pending",
            }
        ]
    )
    useEffect(() => {
        callAPI<null, AllUsersLeave[]>("GET", "/get_pending").then(data => {
            setAllUsersLeaveList(data)
        }
        )
    }, [])
    const showAllLeave = () => {
        callAPI<null, AllUsersLeave[]>("GET", "/get_allleave").then(data => {
            setAllUsersLeaveList(data)
        }
        )
    }
    const showLeaveList = (leave_date: string) => {
        const leaveDate = new Date(leave_date)
        const date = `${leaveDate.getDate()}` + "/" + `${leaveDate.getMonth() + 1}` + "/" + `${leaveDate.getFullYear()}`
        return date
    }
    const approve = async (id: number) => {
        await callAPI<any, null>("POST", "/approve_leave", {
            id
        })
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: 'success',
            title: 'Approved!'
        })

        const newList = allUsersLeaveList.map((leave) => {
            if (leave.id == id) {
                return {
                    ...leave, application_status: "approved"
                }
            } else {
                return leave
            }
        })
        setAllUsersLeaveList(newList)
    }
    const decline = async (id: number) => {
        await callAPI<any, null>("POST", "/decline_leave", {
            id
        })
        const Toast = Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            didOpen: (toast) => {
                toast.addEventListener('mouseenter', Swal.stopTimer)
                toast.addEventListener('mouseleave', Swal.resumeTimer)
            }
        })

        Toast.fire({
            icon: 'success',
            title: 'Declined!'
        })

        const newList = allUsersLeaveList.map((leave) => {
            if (leave.id == id) {
                return {
                    ...leave, application_status: "declined"
                }
            } else {
                return leave
            }
        })
        setAllUsersLeaveList(newList)
    }
    return (
        <>
            <div className="leave_page">
                <div className="pageTitle">
                    <div className="leave_goback">
                        <div></div>
                        <h3>Pending Leave</h3>
                        <div>
                            <i className="fas fa-clipboard-list" onClick={showAllLeave}></i>
                        </div>
                    </div>
                </div>
                <div className="editleave-table">
                    <table>
                        <thead>
                            <tr>
                                <th>Staff Id</th>
                                <th>Date</th>
                                <th>Leave Type</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            {allUsersLeaveList.map((leave, i) => {
                                return (
                                    <tr key={i}>
                                        <td>{leave.staff_id}</td>
                                        <td><Link to={"/leave_detail/" + leave.id}>{showLeaveList(leave.leave_date)}</Link></td>
                                        <td>{leave.leave_type}</td>
                                        <td style={{
                                            width: "9ch", backgroundColor: leave.application_status == "approved" ? "darkgreen" :
                                                leave.application_status == "declined" ? "darkred" : "#cfbb25"
                                        }}>{leave.application_status}</td>
                                        <td style={{ width: "6ch" }}>
                                            <i className="fas fa-check-circle" onClick={() => approve(leave.id)}></i>
                                            <i className="fas fa-times-circle" onClick={() => decline(leave.id)}></i>
                                        </td>
                                    </tr>
                                )
                            }
                            )}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    )
}