import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { callAPI } from '../../CallAPI';

export function LeaveDetail() {
    const [leaveDetail, setLeaveDetail] = useState<any>('')
    const params: any = useParams()
   
    const id = params.id
    useEffect(() => {
        callAPI<null, any>("GET", "/leave_detail/" + id).then(data => {
            setLeaveDetail(data)
        })
    }, [id])
    const showLeaveList = (leave_date: string) => {
        const leaveDate = new Date(leave_date)
        const date = `${leaveDate.getDate()}` + "/" + `${leaveDate.getMonth() + 1}` + "/" + `${leaveDate.getFullYear()}`
        return date
    }
    const isAllDay = (is_all_day: number) => {
        if (is_all_day == 0.5) {
            return `Half Day Leave`
        } else if (is_all_day == 1) {
            return `Full Day Leave`
        } else {
            return `UNKNOWN IS ALL DAY LEAVE`
        }
    }
    return (
        <>
            {leaveDetail ?
                <>
                    <div>
                        <h3 style={{ padding: "20px" }}>Leave number: {id}</h3>
                        <div className="leave-detail">
                            <p>User id: {leaveDetail.users_id}</p>
                            <p>Name: {leaveDetail.name}</p>
                            <p>Data: {showLeaveList(leaveDetail.leave_date)}</p>
                            <p>Type: {leaveDetail.leave_type}</p>
                            <p>{isAllDay(leaveDetail.is_all_day)}</p>
                            <p>Status: {leaveDetail.application_status}</p>
                            <p>Remark: {leaveDetail.remark}</p>
                        </div>
                    </div>
                </> : null
            }
        </>
    )
}

