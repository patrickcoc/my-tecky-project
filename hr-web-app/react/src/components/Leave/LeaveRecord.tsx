import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { IonButton } from '@ionic/react'
import './LeaveRecord.scss'
import { callAPI } from '../../CallAPI';

type AllLeave = {
    id: Number
    leave_date: string,
    leave_type: string,
    application_status: string
}

export function LeaveRecord() {
    const [leaveList, setLeaveList] = useState<AllLeave[]>(
        []
    )
    useEffect(() => {
        callAPI<null, any>("GET", "/get_leave").then(data =>
            setLeaveList(data)
        )
    }, [])
    const showLeaveList = (leave_date: string) => {
        const leaveDate = new Date(leave_date)
        const date = `${leaveDate.getDate()}` + "/" + `${leaveDate.getMonth() + 1}` + "/" + `${leaveDate.getFullYear()}`
        return date
    }
    return (
        <>
            <div className="page_title">
                <h1>Leave Record</h1>
            </div>
            <div className="leave-record-table">
                <table>
                    <tbody>
                    <tr>
                        <th>Date</th>
                        <th>Leave Type</th>
                        <th>Status</th>
                    </tr>
                    {leaveList.map((element, i) => {
                        return (
                            <tr key={i}>
                                <td><Link to={"/leave_detail/" + element.id}>{showLeaveList(element.leave_date)}</Link></td>
                                <td>{element.leave_type}</td>
                                <td className="status" style={{
                                    backgroundColor: element.application_status == "approved" ? "darkgreen" :
                                        element.application_status == "declined" ? "darkred" : "#cfbb25"
                                }}>{element.application_status}</td>
                            </tr>
                        )
                    }
                    )}
                    </tbody>
                </table>
            </div>
            <Link to="/leave/applyleave">
                <IonButton className="leaveBtn">Apply Leave</IonButton>
            </Link>
        </>
    )
}