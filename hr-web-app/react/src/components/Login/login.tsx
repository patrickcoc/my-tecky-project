import { useEffect } from 'react'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Redirect } from 'react-router'
import { Route, Switch } from 'react-router-dom'
import NoMatch from '../../pages/NoMatch/NoMatch'
import {userSubmit} from '../../redux/login/thunk'
import { RootState } from '../../redux/state'
import './LoginPage.scss'

export function LoginPage() {


  const getMessage = useSelector(
    (state: RootState) => state.auth.errorMessage,
  )
  const isAuthenticated = useSelector(
    (state: RootState) => state.auth.isAuthenticated,
  )
  const [username, setUserName] = useState<string>("")
  const [password, setPassword] = useState<string>("")
  const [message, setMessage] = useState<string>('')
  const dispatch = useDispatch()


  function submit() {
    dispatch(userSubmit(username, password))
  }
  useEffect(() => {
    if(getMessage){
      setMessage(getMessage)
    }
}, [getMessage])

  if (isAuthenticated) {
    return <Redirect to="/home_page" />
   
  } 
 


 
  return (
    <div className="login">
      <h1>Login</h1>
   
      <div className="form">

        <p className="field">
          <input type="text" name="login" placeholder="Username" required onChange={event => setUserName(event.target.value)}/>
          <i className="fa fa-user"></i>
        </p>

        <p className="field">
          <input type="password" name="password" placeholder="Password" required onChange={event => setPassword(event.target.value)}/>
          <i className="fa fa-lock"></i>
        </p>

        <p className="submit"><input type="submit" name="sent" value="Login" onClick={submit}/></p>
        <div className="loginMessage">{message}</div>

      </div>
    </div>
  )
}