import React, { useEffect, useState } from 'react'
import { NavLink, Switch, Route, Redirect, Link } from 'react-router-dom'
import UserPage from '../../pages/UserPage/UserPage'
import RosterPage from '../../pages/RosterPage/RosterPage'
import RecordPage from '../../pages/LeavePage/RecordPage'
import AttendacePage from '../../pages/AttendancePage/AttendancePage'
import AdminPage from '../../pages/AdminPage/AdminPage'
import NoMatch from '../../pages/NoMatch/NoMatch'
import QrPage from '../../pages/QrPage/QrPage'
import "./Bar.scss"
import { useSelector, useDispatch } from 'react-redux'
import { RootState } from '../../redux'
import { logoutThunk } from '../../redux/login'
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Container, Nav } from 'react-bootstrap';
import UserAttendancePage from '../UserAttendance/U_attendance'
import { callAPI } from '../../CallAPI'
import AttendanceRecord from '../Attendance/Attendance'
import { PersonalForm } from '../PersonalForm/P_Form'
import SearchContent from '../Personal/SearchContent'
import CalculatorSalaryPage from '../CalculatorSalary/calculatorSalary'
import AdminRoster from '../Roster/AdminRoster'
import Scanner from '../../pages/AdminPage/ScannQrPage'
import { EditLeave } from '../Leave/EditLeave'

function NavBar() {
    const [admin, setAdmin] = useState(false)
    const isAuthenticated = useSelector(
        (state: RootState) => state.auth.isAuthenticated,
    )
    const isAdmin = useSelector(
        (state: RootState) => state.auth.isAdmin,

    )
    const check = localStorage.getItem('token')
    useEffect(() => {
        if (isAdmin) {
            setAdmin(true)
        }
    }, [isAdmin])


    const dispatch = useDispatch()
    function logout() {
        dispatch(logoutThunk())
        setAdmin(false)
    }
    if (!isAuthenticated) {
        return <Redirect to="/login" />
    }

    const username = localStorage.getItem('username')

    return (
        <>
            <div>
                <Navbar bg="light" expand="lg">
                    <Container>
                        <Navbar.Brand className="username">員工編號: {username}</Navbar.Brand>
                        <Navbar.Toggle aria-controls="basic-navbar-nav" />
                        <Navbar.Collapse id="basic-navbar-nav">
                            <Nav className="me-auto">
                                <Link className='link' to="/roster">Roster</Link>
                                <Link className='link' to="/qr_check">QR code</Link>
                                <Link className='link' to="/leave">Apply Leave</Link>
                                <Link className='link' to="/users">User</Link>
                                <Link className='link' to="/attendance">Attendance</Link>
                                {admin ? (
                                    <Link className='link' to="/admin_page">Admin</Link>
                                ) : (<></>)}
                                <i className="fas fa-sign-out-alt" onClick={logout}></i>
                            </Nav>
                        </Navbar.Collapse>
                    </Container>
                </Navbar>

                <Switch>
                    {/* <Route path="/home" component={HomePage} /> */}
                    <Route path="/users" component={UserPage} />
                    <Route path="/roster/" component={RosterPage} />
                    <Route path="/leave" component={RecordPage} />
                    <Route path="/attendance" component={UserAttendancePage} />
                    <Route path="/admin_page" component={AdminPage} />
                    <Route path="/qr_check" component={QrPage} />
                    <Route path="/admin_attendance" component={AttendanceRecord} />
                    <Route path="/admin_department" component={PersonalForm} />
                    <Route path="/admin_personal" component={SearchContent} />
                    <Route path="/admin_leave" component={EditLeave} />
                    <Route path="/admin_salary" component={CalculatorSalaryPage} />
                    <Route path="/admin_roster" component={AdminRoster} />
                    <Route path="/admin_scanner" component={Scanner} />

                    <Route component={NoMatch} />
                </Switch>
            </div>
        
        </>
    )
}



//     }
// }


export default NavBar
