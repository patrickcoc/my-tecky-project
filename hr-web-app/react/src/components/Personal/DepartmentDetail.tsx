import { useEffect, useState } from "react";
import { callAPI } from "../../CallAPI";
import { UserDetail } from "./UserDetail";
import './DepartmentDetail.scss'
import { Link, Route, Switch, useHistory, useLocation } from "react-router-dom";
export const DepartmentDetail = (props: any) => {
    const { department_name, id } = props;
    const [filteredUser, setFilteredUser] = useState([]);
    const [search, setSearch] = useState("");
    const [userData, setData] = useState<never[]>([]);
    const [On, setOn] = useState(false)
    const [delUserData, setDelUser] = useState(null)
    const history = useHistory()
    const location = useLocation()
    const params = new URLSearchParams(location.search)
    const key = Math.random().toString(36).substr(2, 9)
    // function getPersonalData() {
    //     callAPI<any, any>('POST', '/get_personal_data', {
    //         select: id
    //     }).then(data => setData(data))
    //     setOn(true)
 
    // }
    // useEffect(() => {
    //     if (userData) {
    //         setFilteredUser(
    //             userData.filter((userData: any) =>
    //                 userData.name.toLowerCase().includes(search.toLowerCase())
    //             ));
    //     }
    // }, [search, userData]);
    
    useEffect(() => {
        if (delUserData) {
            callAPI<any, any>('POST', '/delet_user', {
                user: delUserData
            })
            callAPI<any, any>('POST', '/get_personal_data', {
                select: id
            }).then(data => setData(data))
        }
    }, [delUserData]);


    function btnClick() {
        setOn(false)

        history.push('/admin_personal')
    }
    const getID = params.get('id')?.toString()

    return (
        <>
        
     
            {getID == id || getID == undefined ? (
                <>
                    {On ? (<></>) : (
                          <Link to={`/personal/${id}`} className="department_list">
         
                                <p className={id}  >{department_name} </p>     
                        
                         </Link>
                        )
                    }
                    {On ? (
                        <div className='searchBar' >
                            <input
                                type="text"
                                placeholder="Search Record"
                                onChange={(e) => setSearch(e.target.value)}
                            /> <i className="fas fa-undo" onClick={btnClick}></i></div>
                    ) : (<></>)}
                    <div className="user_detail">
             
                        </div>
                </>) : (<></>)}
        </>
    );
};