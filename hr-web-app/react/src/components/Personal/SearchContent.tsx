import React, { useState, useEffect } from "react";
// import ReactDOM from "react-dom";
// import axios from "axios";
import { callAPI } from "../../CallAPI";
import "./SearchContent.scss"
import { DepartmentDetail } from "./DepartmentDetail";
import { Link, Route, Switch, useLocation } from "react-router-dom";
import { UserDetail } from "./UserDetail";

// import "./styles.css";
const SearchContent = () => {
  const [department, setDepartment] = useState<never[]>([]);
  // const [userData, setData] = useState<never[]>([]);
  const [loading, setLoading] = useState(false);
  const [search, setSearch] = useState("");
  const [On, setOn] = useState(true)
  const [filteredDepartment, setFilteredDepatment] = useState([]);
  const location = useLocation()
  const params = new URLSearchParams(location.search)
  const getID = params.get('id')?.toString()

  useEffect(() => {
    setLoading(true);
    callAPI<null, never[]>('GET', '/get_department').then(data =>
      setDepartment(data)
    )
    setLoading(false)
  }, [])

  useEffect(() => {
    setFilteredDepatment(
      department.filter((department: any) =>
        department.department_name.toLowerCase().includes(search.toLowerCase())
      ))

  }, [search, department]);

  if (loading) {
    return <p>Loading ...</p>;
  }

  return (
    <div className="searchContent">
      {/* <div className="pageTitle">
        <Link to='/admin_page'><i className="fas fa-arrow-circle-left gobackSalary"></i></Link>
        <Switch><Route path="/admin_page" exact></Route>
          <Route path={`/admin_personal/personal/:id`} component={UserDetail}></Route>
        </Switch>
      </div> */}
      {getID ? (<></>) : (
        <div className='searchBar'>
          <input
            type="text"
            placeholder="Search Department"
            onChange={(e) => setSearch(e.target.value)}
          /></div>
      )}
      {filteredDepartment.map((department, idx) => (
        <DepartmentDetail key={idx} {...department} />
      ))}

    </div>
  )
}

export default SearchContent;







