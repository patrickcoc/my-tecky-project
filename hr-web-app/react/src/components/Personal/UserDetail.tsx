
import { useEffect, useState } from "react";
import { useHistory, useLocation, useRouteMatch } from "react-router-dom";
import { callAPI } from "../../CallAPI";
import './UserDetail.scss';

type personalData = {
  id: number,
  name: string,
  phone: number,
  IDcard: string,
  date_of_birth: Date,
  address: string,
  email: string,
  post: string,
  salary: number,
  leave_day: number,
  entry_date: Date,
  staff_id: string
}
export const UserDetail = (props: any) => {
  // const { department_id, name, IDcard, address, phone, email,
  //   date_of_birth, post, salary, leave_day, staff_id, admin, entry_date, id } = props;
  const [message, setMessage] = useState<string>('')
  const [show, setShow] = useState(false)
  const [read, setreadOnly] = useState(true)
  const [id, setID] = useState<number>()
  const [display, setDisplay] = useState<string>('')
  const [newName, setName] = useState<string>('')
  const [newPhone, setPhone] = useState<any>()
  const [new_date_of_birth, setBirth] = useState<string>('')
  const [new_IDcard, setIDcard] = useState<string>('')
  const [new_address, setAddress] = useState<string>('')
  const [new_email, setEmail] = useState<string>('')
  const [new_post, setPost] = useState<string>('')
  const [new_salary, setSalary] = useState<any>('')
  const [new_leave_day, setLeave] = useState<any>('')
  const [new_entry_date, setEntry] = useState<string>('')
  const [userData, setData] = useState<personalData[]>([]);
  const [filteredUser, setFilteredUser] = useState<personalData[]>([]);
  const [search, setSearch] = useState("");
  const history = useHistory()
  const location = useLocation()
  const params = new URLSearchParams(location.search)
  const departmentID: any = useRouteMatch().params
  const getID = departmentID.id

  useEffect(() => {
    callAPI<any, any>('POST', '/get_personal_data', {
      select: getID
    }).then(data => setData(data))

  }, [getID]);

  // const location = useLocation()
  // const params = new URLSearchParams(location.search)
  // const getID = params.get('id')?.toString()
  const newData = {
    id: id,
    name: newName,
    phone: newPhone,
    IDcard: new_IDcard,
    date_of_birth: new_date_of_birth,
    address: new_address,
    email: new_email,
    post: new_post,
    salary: new_salary,
    leave_day: new_leave_day,
    entry_date: new_entry_date,
  }


  useEffect(() => {
    if (userData) {
      setFilteredUser(
        userData.filter((userData: any) =>
          userData.name.toLowerCase().includes(search.toLowerCase())
        ));
    }
  }, [search, userData]);
  function updateData() {
    const { REACT_APP_API_URL } = process.env
    const res = {
      method: 'PUT',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ data: newData })
    };
    fetch(REACT_APP_API_URL + '/update_data', res)
      .then(res => res.json())
      .then(data=>setMessage(data.message))
      callAPI<any, any>('POST', '/get_personal_data', {
        select: getID
      }).then(data => setData(data))
      setreadOnly(true)
    // .then(data => this.setState({ postId: data.id }));
  }


  function showDetail() {
    setShow(!show)
    setMessage('')
    setreadOnly(true)
  }
  function clickRead() {
    setreadOnly(!read)
  }
 

  return (
    <> <div className="userDetail_page">
      {!show ? (<div className='searchBar' >
        <input
          type="text"
          placeholder="Search Record"
          onChange={(e) => setSearch(e.target.value)}
        /> </div>
      ) : (null)}
<div className="bigDetail">
      <div className="user_list" >
        {show ? (<></>) : (
          filteredUser.map((detail, idx) => (
            <div className="personal_select" key={idx} onClick={() => {
              setDisplay(detail.staff_id); setShow(true); setName(detail.name);
              setPhone(detail.phone); setBirth(detail.date_of_birth.toString().slice(0, 10));
              setIDcard(detail.IDcard); setAddress(detail.address);
              setEmail(detail.email); setPost(detail.post);
              setSalary(detail.salary); setLeave(detail.leave_day);
              setEntry(detail.entry_date.toString().slice(0, 10));
              setID(detail.id);
            }}>
              <p >Staff Name:{detail.name}<br></br>
                Staff ID:{detail.staff_id}</p>
            </div>))
        )}

        {show ? (
          <>
            {/* {userData.map((detail, idx) => ( */}
              <div className="DetailList">
               
                  <div>
                    <form className="formList"  >
                      <label>Name:</label><label><input type='text' name="fullname" value={newName} onChange={event => setName(event.target.value)} readOnly={read} /></label>
                      <label>Phone:</label><label>
                        <input type='text' name="phone" value={newPhone} pattern="[0-9]*" onChange={event => setPhone(event.target.value)} readOnly={read} /></label>
                      <label>Email:</label><label>
                        <input type='text' name="email" value={new_email} onChange={event => setEmail(event.target.value)} readOnly={read} /></label>
                      <label>Date of birth:</label><label>
                        <input type='date' name="email" className="birth_data_date" value={new_date_of_birth} onChange={event => setBirth(event.target.value)} readOnly={read} /></label>
                      <label>Post:</label><label>
                        <input type='text' name="post" value={new_post} onChange={event => setPost(event.target.value)} readOnly={read} /></label>
                      <label>Salary:</label><label>
                        <input type='text' name="salary" value={new_salary} onChange={event => setSalary(event.target.value)} readOnly={read} /></label>
                      <label>IDcard:</label><label>
                        <input type='text' name="IDcard" value={new_IDcard} onChange={event => setIDcard(event.target.value)} readOnly={read} /></label>
                      <label>Leave day:</label><label>
                        <input type='text' name="leave_day" value={new_leave_day} onChange={event => setLeave(event.target.value)} readOnly={read} /></label>
                      <label>Address:</label><label>
                        <input type='text' name="address" value={new_address} onChange={event => setAddress(event.target.value)} readOnly={read} /></label>
                      <label>Entry date:</label><label>
                        <input type='date' name="entry_date"  className="data_date" value={new_entry_date} onChange={event => setEntry(event.target.value)} readOnly={read} /></label>
                    </form>
                    <div className="imageBar" >
                      <i className="fas fa-check-circle" onClick={updateData}></i>
                      <i className="fas fa-edit" onClick={clickRead}></i>
                      <i className="fas fa-arrow-circle-left" onClick={showDetail}></i>
                    </div>
                    {message?(<div className="update_message">{message}</div>):(null)}
                  </div>

          
              </div>

            {/* ))} */}
          </>

        ) : (<></>)}


      </div>
      </div>
    </div>
    </>
  );
};