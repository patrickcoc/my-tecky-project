import { useState, useEffect } from 'react'
import { callAPI } from '../../CallAPI'
import "./P_Form.scss"
import UploadFile from '../uploadFile/uploadExcel'
import { ExportReactCSV } from '../uploadFile/exportExcel'
import { Link, Route, Switch } from 'react-router-dom'
import { Button } from 'react-bootstrap'

export function PersonalForm() {
  const [message, setMessage] = useState<string>('')
  const [department, setDepartment] = useState<string>('')
  const [departmentArr, setarr] = useState<any[]>(['select'])
  const [openUpload, setOpenUpload] = useState<boolean>(false)
  useEffect(() => {
    const getCompanyID = localStorage.getItem('company_id')
    if (getCompanyID) {
      checkNum(getCompanyID)
    }
    callAPI<null, any[]>('GET', '/get_department').then(data =>
      setarr(data)
    )
  }, [])


  const [addBtn, setAddBtn] = useState(false)
  const [comfirm, setComfirm] = useState(false)
  const [newDepartment, setNewDepartment] = useState<string>('')
  const [IDcard, setIDcard] = useState<string>('')
  const [count, setcount] = useState<string>('')
  const [ac_level, setLevel] = useState<string>('1')
  const [company_id, setComapny] = useState<number>(1)
  const [ac_id, setID] = useState<string>('')
  const [name, setName] = useState<string>('')
  const [phone, setPhone] = useState<string>('')
  const [date_of_birth, setBirth] = useState<string>('')
  const [address, setAddress] = useState<string>('')
  const [email, setEmail] = useState<string>('')
  const [post, setPost] = useState<string>('')
  const [salary, setSalary] = useState<string>('')
  const [leave_day, setLeave] = useState<string>('')
  const [entry_date, setEntry] = useState<string>('')



  function checkNum(data: string) {
    if (typeof data == "string") {
      return setComapny(parseInt(data))
    }
    else { return 1 }
  }

  const TakeSubmit = (event: any) => {
    event.preventDefault();
  }
  const employeeData = {
    name: name,
    phone: phone,
    IDcard: IDcard,
    date_of_birth: date_of_birth,
    address: address,
    email: email,
    department: department,
    post: post,
    salary: salary,
    leave_day: leave_day,
    entry_date: entry_date,
    users_id: ac_id
  }

  function clickAdd() {
    setAddBtn(!addBtn)
  }

  function clickOff() {
    setAddBtn(false)
  }
  function createDepartment() {
    if (newDepartment != '') {
      newDepartment.replace(/ /i, '_')
      callAPI<any, null>('POST', '/add_department', {
        newDepartment: newDepartment
      })
      callAPI<null, any[]>('GET', '/get_department').then(data =>
        setarr(data)
      )
    }
  }
  function createUsername() {
    callAPI<any, string>('GET', '/get_count').then(data =>
      setcount(data)
    )
    if (employeeData.name === ''
      || employeeData.phone === ''
      || employeeData.IDcard === ''
      || employeeData.date_of_birth === ''
      || employeeData.address === ''
      || employeeData.email === ''
      || employeeData.department === ''
      || employeeData.post === ''
      || employeeData.salary === ''
      || employeeData.leave_day === ''
      || employeeData.entry_date === ''
    ) {
      alert("please enter")
    }
    else {
      setComfirm(true)
    }
  }
  const date = new Date
  const number = date.getFullYear()
  const username = number.toString() + company_id + department + count

  function closeWindow() {
    setComfirm(false)
  }
  function checkBoolen(value: string) {
    if (value === '2') {
      return true
    }
    else { return false }
  }
  const account = {
    staff_id: username,
    password: parseInt(IDcard.slice(1)),
    company_id: company_id,
    department: parseInt(department),
    admin: checkBoolen(ac_level)
  }

  async function InsertFormData() {

    await callAPI<any, any>('POST', '/create_ac', {
      account: account,
      data: employeeData
    })
      .then(data => {
        setMessage(data.message)
      })
  }


  function clickOpenUpload() {
    setOpenUpload(true)
  }
  function clickCloseUpload() {
    setOpenUpload(false)
  }
  function clearAll() {
    setEmail('')
    setName('')
    setPhone('')
    setBirth('')
    setAddress('')
    setPost('')
    setSalary('')
    setLeave('')
    setEntry('')
    setLevel('1')
    setIDcard('')
    setComfirm(false)
  }

  return (
    <>
      <div className="pageTitle">

        <div className='uploadForm'>
          {/* <Link to='/admin_page'><i className="fas fa-arrow-circle-left gobackSalary"></i></Link>
            <Switch><Route path="/admin_page" exact></Route></Switch> */}
          {openUpload ? (
            <><label>Bulk upload</label>
              <i className="fas fa-chevron-up" onClick={clickCloseUpload}></i>
              <UploadFile />
            </>)
            : (<><label>Bulk upload</label> <i className="fas fa-chevron-down" onClick={clickOpenUpload}></i></>)}

        </div>
      </div>


      <div className="employeeData">
        <form className="employeeForm DataInput" onSubmit={TakeSubmit}>


          <ol >
            <li>
              <label>Name:</label><input type='text' name="fullname" className="textInput" value={name} onChange={event => setName(event.target.value)} />
            </li>
            <li>
              <label>IDcard:</label><input type='text' name="IDcard" className="textInput" value={IDcard} onChange={event => setIDcard(event.target.value)} />
            </li>
            <li>
              <label>Address:</label>
              <input type='text' name="address" className="textInput" value={address} onChange={event => setAddress(event.target.value)} />
            </li>

            <li>
              <label>Phone Number:</label>
              <input type='text' name="phone" className="textInput" value={phone} pattern="[0-9]*" onChange={event => setPhone(event.target.value)} />
            </li>

            <li>
              <label>Date of birth:</label>
              <input type='date' className="dateInput" name="birth" value={date_of_birth} onChange={event => setBirth(event.target.value)} />
            </li>

            <li>
              <label>Email:</label>
              <input type='email' name="email" className="textInput" value={email} onChange={event => setEmail(event.target.value)} />
            </li>

            <li>
              <label>Department:</label>
              <select value={department} onChange={event => setDepartment(event.target.value)} >
                <option>Select </option>
                {departmentArr.map((options) => (<option key={options.department_name + options.id} value={options.id}>{options.department_name}</option>))}
              </select>
              <button className="addNew" onClick={clickAdd}>add</button>
            </li>

            <li>
              <label>Post:</label>
              <input type='text' name="post" className="textInput" value={post} onChange={event => setPost(event.target.value)} />
            </li>

            <li>
              <label>Salary:</label>
              <input type='text' name="salary" className="textInput" value={salary} pattern="[0-9]*" onChange={event => setSalary(event.target.value)} />
            </li>

            <li>
              <label>Annaul Leave:</label>
              <input type='number' name="leave_day" className="leaveInput" value={leave_day} min="0" max="364" onChange={event => setLeave(event.target.value)} />
            </li>

            <li>
              <label>Entry_date:</label>
              <input type='date' name="entry_date" className="dateInput" value={entry_date} onChange={event => setEntry(event.target.value)} />
            </li>

            <div className="formBtn">
              <i onClick={createUsername} className="submitBtn fas fa-arrow-circle-up" ></i>
              <i className="clearBtn fas fa-eraser" onClick={clearAll}></i>
            </div>
          </ol>

        </form>

        {comfirm ? (
          <div className="comfirmAC">
            <i className="far fa-times-circle" onClick={closeWindow}></i>
            <br></br>
            <label>username:<span>{username}</span></label>
            <br></br>
            <label>password:<span>{IDcard.slice(1)}</span></label>
            <br></br>
            <select onChange={event => setLevel(event.target.value)}>
              <option value='1'>user</option>
              <option value='2'>admin</option>
            </select>
            <label style={{ color: "#ffed07" }}>make sure information correct!!</label>
            <label className="message_comfirm">{message}</label>
            <br></br>
            <Button className="insertBtn" onClick={InsertFormData}>comfirm</Button>
            <br></br>
          </div>
        ) : (<></> )}
      </div>
      {addBtn ? (
        <div className='addList'>
          <input type="text" name="addDepartment" value={newDepartment} onChange={event => setNewDepartment(event.target.value)} />
          <button type='submit' onClick={createDepartment}>add</button>
          <i className="far fa-times-circle" onClick={clickOff}></i>
          <ol>
            {departmentArr.map((options) => (<li key={options.department_name + options.id}
              value={options.id}>{options.department_name}
            </li>))}
          </ol>
        </div>
      ) : (<></>)}
    </>
  )
}
