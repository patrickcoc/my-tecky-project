import React from "react";
import { useEffect, useMemo, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Switch, Route, Link } from "react-router-dom";
import { RootState } from "../../redux";
import {
  getDepartmentNameThunk,
  getDepartmentStaffListThunk,
  getDepartmentStaffRosterThunk,
} from "../../redux/adminRoster/thunk";
import { selectDepartment } from "../../redux/adminRoster/action";
import "./AdminRoster.scss";
import { CreateMonthCalender } from "./MonthCalender";
import { callAPI } from "../../CallAPI";
import Swal from "sweetalert2";

type State = {
  [staff_id: string]: {
    [isoDate: string]: string;
  };
};

type LeaveRecord = {
  leave_date: string;
  leave_type: string;
  staff_id: string;
  name: string;
  application_status: string;
};

type LeaveRecordInput = {
  departmentId: number;
  startDay: string;
  endDay: string;
};

function AdminRoster() {
  const dispatch = useDispatch();

  const { Monthcalendar, startMonthDate, setStartMonthDate } =
    CreateMonthCalender();

  const selectedYear = new Date(startMonthDate);

  let year = selectedYear.getFullYear();
  let month = selectedYear.getMonth();

  function daysInMonth(month: number, year: number) {
    month = month + 1;
    return new Date(year, month, 0).getDate();
  }

  let weekday = selectedYear.getDay();

  const maxDay = 31;
  let days = new Array(daysInMonth(month, year)).fill("");
  let numberOfDays = days.length;

  const staffList = useSelector(
    (state: RootState) => state.adminRoster.staffList
  );

  const numberOfStaff = staffList.length;

  const departmentList = useSelector((state: RootState) => state.adminRoster);

  let selectedDepartmentId = useSelector(
    (state: RootState) => state.adminRoster.selectedDepartmentId
  );

  let selectedDepartmentName = useSelector(
    (state: RootState) =>
      state.adminRoster.departmentList.find(
        (d) => d.id === selectedDepartmentId
      )?.department_name
  );

  const tableDataList = useMemo(() => {
    return new Array(numberOfStaff)
      .fill(0)
      .map(() => new Array(numberOfDays).fill(null));
  }, [Monthcalendar, selectedDepartmentName, numberOfDays, numberOfStaff]);

  while (tableDataList.length < numberOfStaff) {
    tableDataList.push(new Array(numberOfDays).fill(null));
  }

  const weekendRender = (j: number, weekday: number) => {
    const chineseWeeks = ["日", "一", "二", "三", "四", "五", "六"];
    const weekDay = (j + weekday) % 7;
    const chineseWeekDay = chineseWeeks[weekDay];
    return chineseWeekDay;
  };

  // const [ state, setState ] = useState<State>({})
  const state = useMemo(() => {
    function makeState(): State {
      let state: State = {};
      staffList.forEach((staff) => {
        state[staff.staff_id] = {};

        let day = 1;
        for (;;) {
          let date = new Date();
          date.setFullYear(year, month, day);
          if (date.getMonth() !== month) {
            break;
          }
          date.setHours(0, 0, 0, 0);
          let isoDate = date.toISOString();
          state[staff.staff_id][isoDate] = "";

          day++;
        }
      });
      state.loading = {};
      return state;
    }

    return makeState();
  }, [staffList]);

  // get a department list from user
  useEffect(() => {
    dispatch(getDepartmentNameThunk());
  }, []);

  const handleChange = async (event: any) => {
    dispatch(selectDepartment(+event.target.value));
    dispatch(getDepartmentStaffListThunk(+event.target.value));
  };

  // putting a 0 in front of int as it is small than 10
  const d2 = (x: number) => {
    return x < 10 ? "0" + x : x;
  };

  const ondutyCalSumbit = () => {
    callAPI<any, null>("POST", "/monthSchedule", {
      data: state,
    });
    Swal.fire({
      position: "top-end",
      icon: "success",
      title: "提交成功",
      showConfirmButton: false,
      timer: 1500,
    });
  };

  // updating state when users select a new date
  const [version, setVersion] = useState(0);

  useEffect(() => {
    let start = new Date()
    start.setFullYear(year,month,1)
    start.setHours(0,0,0,0)

    let end = new Date()
    end.setFullYear(month >= 12? year + 1 : year, month >= 12? 1 : month + 1 , 1)
    end.setHours(0,0,0,0)

    end.setTime(end.getTime() -1)

    callAPI<any, State>("POST", "/departmentStaffRoster", {
      departmentId: selectedDepartmentId,
      start: start.toISOString(),
      end: end.toISOString(),
    }).then((data) => {
      Object.keys(state).forEach((staffId) => {
        delete state[staffId];
      });
      Object.assign(state, data);
      setVersion((x) => x + 1);
    });
  }, [selectedDepartmentId, month, setVersion, staffList]);

  // Leave Type Change Handling

  const leaveType = [
    "Rest Day",
    "Sick Leave",
    "Annual Leave",
    "Non-pay Leave",
    "Maternity Leave",
    "Paternity Leave",
    "Others",
  ];

  const [leaveItem, setLeaveItem] = useState({});

  const [onlyApproved, setOnlyApproved] = useState(false);

  const [leaveRecord, setLeaveRecord] = useState<Array<LeaveRecord>>([]);

  useEffect(() => {
    const selectMonth = month;
    const selectYear = year;
    const departmentId = selectedDepartmentId;
    const localStartDay = new Date(year, month, 1);
    const localEndDay = new Date(
      month >= 12 ? year + 1 : year,
      month >= 12 ? 1 : month + 1,
      1
    );
    const startDay = localStartDay.toISOString();
    const endDay = localEndDay.toISOString();

    callAPI<LeaveRecordInput, LeaveRecord[]>(
      "POST",
      "/departmentStaffMonthlyLeaveApplication",
      {
        departmentId,
        startDay,
        endDay,
      }
    ).then((data) => {
      setLeaveRecord(data);
    });
  }, [month, selectedDepartmentId]);

  const toLocalString = (isoDate: string) => {
    const isoDateFormat = new Date(isoDate);
    const localDate = isoDateFormat.toLocaleString();
    return localDate.slice(0, 10);
  };

  return (
    <Switch>
      <Route path="/admin_roster" exact>
        <div className="adminRosterContainer">
          <div className="adminRoster">
            {/* <Link to='/admin_page'><i className="fas fa-arrow-circle-left gobackSalary"></i></Link> */}
            <div className="title_bar">
              <Route path="/admin_page" exact></Route>

              <select onChange={handleChange}>
                {departmentList.departmentList ? (
                  departmentList.departmentList.map((department, i) => (
                    <option value={department.id} key={department.id}>
                      {department.department_name}
                    </option>
                  ))
                ) : (
                  <div className="StaffList">Loading</div>
                )}
              </select>

              <div>{Monthcalendar}</div>
            </div>
            <div className="selectedDepartmentTitle">
              <div className="selectedDepartment">
                Department: {selectedDepartmentName}
              </div>

              <div className="staffListHeader">
                <div className="StaffListItem"></div>
                <div className="StaffListItem"></div>
              </div>

              {state.loading ? (
                "loading"
              ) : (
                <table className="adminRosterTable">
                  <thead>
                    <tr>
                      <th></th>
                      <th></th>
                      <th></th>
                      {days.map((i, j) => (
                        <th className="StaffListCal" key={j}>
                          {weekendRender(j, weekday)}
                        </th>
                      ))}
                    </tr>
                  </thead>
                  <thead>
                    <tr>
                      <th>StaffID</th>
                      <th>Name</th>
                      <th>Post</th>
                      {days.map((_, i) => (
                        <th key={i}>{d2(i + 1)}</th>
                      ))}
                    </tr>
                  </thead>
                  <tbody>
                    {staffList.map((staff, staffIndex) => (
                      <tr key={staffIndex}>
                        <td className="adminRosterTitle">{staff.staff_id}</td>
                        <td className="adminRosterTitle">{staff.name}</td>
                        <td className="adminRosterTitle">{staff.post}</td>
                        {days.map((_, dayIndex) => (
                          <td
                            ref={(e) => {
                              tableDataList[staffIndex][dayIndex] = e;
                              if (e) {
                                let date = new Date(year, month, dayIndex + 1);
                                date.setHours(0, 0, 0, 0);
                                let isoDate = date.toISOString();
                                if(!state[staff.staff_id]){
                                  return
                                }
                                e.textContent = state[staff.staff_id][isoDate];
                              }
                            }}
                            className="input"
                            key={dayIndex}
                            contentEditable
                            onInput={(event) => {
                              let element = event.target as HTMLElement;
                              let text = (element.textContent || "")
                                .trim()
                                .slice(0, 2);
                              if (element.textContent !== text) {
                                element.textContent = text;
                              }
                              let date = new Date(year, month, dayIndex + 1);
                              date.setHours(0, 0, 0, 0);
                              let isoDate = date.toISOString();
                              state[staff.staff_id][isoDate] = text;
                            }}
                            onKeyDown={(e) => {
                              let target: HTMLTableCellElement | null = null;
                              switch (e.key) {
                                case "ArrowUp":
                                  if (staffIndex === 0) return;
                                  target =
                                    tableDataList[staffIndex - 1][dayIndex];
                                  break;
                                case "ArrowDown":
                                  if (staffIndex === staffList.length - 1)
                                    return;
                                  target =
                                    tableDataList[staffIndex + 1][dayIndex];
                                  break;
                                case "ArrowLeft":
                                  if (dayIndex === 0) return;
                                  target =
                                    tableDataList[staffIndex][dayIndex - 1];
                                  break;
                                case "ArrowRight":
                                  if (dayIndex === numberOfDays - 1) return;
                                  target =
                                    tableDataList[staffIndex][dayIndex + 1];
                                  break;
                                default:
                                  return;
                              }
                              target?.focus();
                            }}
                          ></td>
                        ))}
                      </tr>
                    ))}
                  </tbody>
                </table>
              )}

              <button onClick={ondutyCalSumbit}>Submit</button>
            </div>
          </div>

          {/* <div className="adminRosterApplyLeave">
            <div className="adminRosterApplyLeaveTitle">Leave Application</div>
            <div>
              <form className="adminRosterLeaveForm">
                <h3 className="adminRosterLeaveFormItem">Leave Type:</h3>
                {leaveType.map((leave, i) => (
                  <label className="adminRosterLeaveFormItem">
                    <div>{leave}</div>
                    <input
                      key={i}
                      name="leaveType"
                      value={leave}
                      type="checkbox"
                      onChange={(e) =>
                        setLeaveItem({
                          ...leaveItem,
                          [leave]: e.target.checked,
                        })
                      }
                    />
                  </label>
                ))}
                <br></br>
                <h3 className="adminRosterLeaveFormItem">Status: </h3>
                <label className="adminRosterLeaveFormItem">
                  <div>Approved:</div>
                  <input
                    type="checkbox"
                    onChange={(e) => setOnlyApproved(e.target.checked)}
                  />
                </label>
              </form>
            </div>
          </div> */}

          <div className="adminRosterDepartmentLeaveInfoContainer">
            <div className="leaveTableTitle">Leave Application</div>
            <table>
              <tbody>
                <tr className="leaveTable">
                  <th className="leaveTableItem">Staff Id</th>
                  <th className="leaveTableItem">Name</th>
                  <th className="leaveTableItem">Leave Type</th>
                  <th className="leaveTableItem">Leave Date</th>
                </tr>
                {leaveRecord.map((staff, i) => (
                  <tr className="leaveTable" key={i}>
                    <td className="leaveTableItem">{staff.staff_id}</td>
                    <td className="leaveTableItem">{staff.name}</td>
                    <td className="leaveTableItem">
                      {toLocalString(staff.leave_date)}
                    </td>
                    <td className="leaveTableItem">{staff.leave_type}</td>
                  </tr>
                ))}
                <tr></tr>
              </tbody>
            </table>
          </div>
        </div>
      </Route>
    </Switch>
  );
}

export default AdminRoster;
