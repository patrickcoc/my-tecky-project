import React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Switch, Route } from "react-router-dom";
import { RootState } from "../../redux";
import {
  getDepartmentNameThunk,
  getDepartmentStaffListThunk,
} from "../../redux/adminRoster/thunk";
import { selectDepartment } from "../../redux/adminRoster/action";
import "./AdminRoster.scss";

function AdminRoster() {
  const dispatch = useDispatch();

  let departmentList = useSelector((state: RootState) => state.adminRoster);

  let selectedDepartmentId = useSelector(
    (state: RootState) => state.adminRoster.selectedDepartmentId
  );
  let selectedDepartmentName = useSelector(
    (state: RootState) =>
      state.adminRoster.departmentList.find(
        (d) => d.id === selectedDepartmentId
      )?.department_name
  );
  let staffList = useSelector(
    (state: RootState) => state.adminRoster.staffList
  );

  // get a department list from user
  useEffect(() => {
    getDepartmentList();
  }, []);

  const getDepartmentList = () => {
    dispatch(getDepartmentNameThunk());
  };

  const handleChange = async (event: any) => {
    dispatch(selectDepartment(+event.target.value));
    dispatch(getDepartmentStaffListThunk(+event.target.value));
  };

  const titleRender31 = (numberOfDaysInMonth:number) => {
    const inputDiv = new Array(numberOfDaysInMonth).fill(null)
    return(
      inputDiv.map((j,i) => <div className="StaffListCal">{i + 1}</div>)
    )
  }

  const weekTitleRender31 = () => {
    const inputDiv = new Array(31).fill(null)
    return(
      <div className="staffListHeader">
      <div className="StaffListItem"></div>
      <div className="StaffListItem"></div>
      <div className="StaffListItem"></div>
      {inputDiv.map((i,j)=> <div  className="StaffListCal" key={j}>{weekendRender(j)}</div>)}
      </div>
    )
  }

  const weekendRender = (j:number) => {
    const chineseWeeks = ['一','二','三','四','五','六','日']
    const weekDay = (j + 2) % 7
    return chineseWeeks[weekDay]
  }

  const inputDivRender = () => {
    const inputDiv = new Array(31).fill('hi')
    return(
      <form className="inputCell">
      {inputDiv.map((j,i) => <input type="text" className="StaffListCal" key={i} maxLength={2}></input>)}
      <br></br>
      <button onSubmit={submitCal}>submit</button>
      </form>
    )
  }

  const submitCal = () => {
    
  }

  return (
    <Switch>
      <Route path="/admin_page/roster" exact>
        <div className="adminRoster">
          <select onChange={handleChange}>
            {departmentList.departmentList ? (
              departmentList.departmentList.map((department, i) => (
                <option value={department.id} key={department.id}>
                  {department.department_name}
                </option>
              ))
            ) : (
              <div className="StaffList">hi</div>
            )}
          </select>

          <div className="selectedDepartmentTitle">

            <div className="selectedDepartment">
              Department: {selectedDepartmentName}
            </div>

            <div>Department id: {selectedDepartmentId}</div>
            <div>{weekTitleRender31()}</div>

            <div className="staffListTitle">
                <div className="StaffList">Index</div>
                <div className="StaffList">Staff id</div>
                <div className="StaffList">Name</div>
                {titleRender31(31)}
                </div>

            <div>    
              {staffList ? (
                staffList.map((staff, i) => (
                  <div className="StaffListContainer">
                      <div className="StaffList">{i + 1}</div>
                    <div key={i} className="StaffList">{staff.staff_id}</div>
                    <div className="StaffList">{staff.name}</div>
                    {inputDivRender()}
                  </div>
                ))
              ) : (
                <div>Server Error</div>
              )}
            </div>
          </div>
        </div>
      </Route>
    </Switch>
  );
}

export default AdminRoster;
