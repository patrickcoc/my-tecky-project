import React, { useState } from 'react';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { setHours, setMinutes } from 'date-fns'

export const CreateRoster = () => {
    const [startDate, setStartDate] = useState(
        setHours(setMinutes(new Date(), 30), 17)
    );
    return (
        <div>
         
            <DatePicker
                selected={startDate}
                onChange={(date: Date) => setStartDate(date)}
                showTimeSelect
                minTime={setHours(setMinutes(new Date(), 0), 17)}
                maxTime={setHours(setMinutes(new Date(), 30), 20)}
                dateFormat="MMMM d, yyyy h:mm aa"
                inline
                
            />
            <div>Selected start date : {startDate ? startDate.toString().slice(0, -18) : null}</div>
        </div>
    );
}