import React, { useEffect, useState } from 'react'
import { callAPI } from '../../CallAPI'
import TableContainer from '../Attendance/TableContainer'
import { CreateCalender, dateFormat } from '../Calendar/calendar'
import { CreateMonthCalender, dateMonthFormat } from '../Calendar/MonthCalender'
import "./U_attendance.scss"

function UserAttendancePage() {
    const { startDate, setStartDate, calendar } = CreateCalender()
    const { Monthcalendar,startMonthDate,setStartMonthDate} = CreateMonthCalender()
    const [data, setData] = useState([])
    const [color, setColor] = useState('#248277')
    const [searchMonth, setSearchMonth] = useState(false)
    const user_id = localStorage.getItem('user_id')
    useEffect(() => {
        if(!searchMonth){
        if (startDate) {
            const doFetch = async () => {
                callAPI<{ date: string; user_id: string | null }, any>('POST', '/get_user_attendance', {
                    date: dateFormat(startDate),
                    user_id: user_id
                }).then(data => setData(data)) 
            }
            doFetch()
            setColor('#028090')
        }
    }
    }, [startDate])

    useEffect(() => {
        if(searchMonth){
         if (startMonthDate) {  
                callAPI<{ date: string; user_id: string | null }, any>('POST', '/get_user_Month_attendance', {
                    date: dateMonthFormat(startMonthDate),
                    user_id: user_id
                }).then(data => setData(data))
            }
            setColor('#248277')
        }
    }, [startMonthDate])
    const columns: any[] = [
        {
            accessor: 'staff_id',
            Header: 'Staff',
            disableFilters: true,
            disableSortBy: true
        },
        {
            accessor: 'attendance_date',
            Header: 'Date',
            disableFilters: true,
            disableSortBy: true
        },
        {
            accessor: 'time',
            Header: 'Time',
            disableFilters: true,
            disableSortBy: true
        },
        {
            accessor: 'in_out',
            Header: 'In/Out',
            disableFilters: true,
            disableSortBy: true
        },
    ]
    function changeMonthMode(){
        setSearchMonth(true)
     
    }
    function changeDayMode(){
        setSearchMonth(false)
    }
    return (
        <div className="useradd_page">
            <div className="page_title">
            <h1>Attendance Record</h1>
            </div>
            <div className="calenderSelect">
                <span className="calendar_day" onClick={changeDayMode}>{calendar}</span>
                <span className="calendar_month" onClick={changeMonthMode}>{Monthcalendar}</span>
            </div>
            {startDate ? (
                <>
                    {data ? (<TableContainer columns={columns} data={data} theadColor={color} />) : (<></>)}
                </>
            ) : (<></>)}
        </div>
    )
}
export default UserAttendancePage