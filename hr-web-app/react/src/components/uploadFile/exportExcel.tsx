import React from 'react'
import { CSVLink } from 'react-csv'
import "./export.scss"


export const ExportReactCSV = ({csvData, fileName,button}:any) => {
    return (
        <button>
            <CSVLink data={csvData} filename={fileName}>{button} <i className="fas fa-download"></i></CSVLink>
        </button>
    )
}