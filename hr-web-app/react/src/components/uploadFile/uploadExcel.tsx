import React from "react";
import axios from "axios";
import { ExportReactCSV } from "./exportExcel";
import "./export.scss"


function UploadFile() {
  const [uploadFile, setUploadFile] = React.useState<any>([{ name: '' }]);
  const [getBackAC, setGetBackAC] = React.useState<string>();
  const [getBackMassage, setGetBackMassage] = React.useState<string>('');

  const company_id = localStorage.getItem('company_id')
  function checkNum(data: string | null) {
    if (typeof data == "string") {
      return data
    }
    else { return '1' }
  }
  const new_ID = checkNum(company_id)
  const submitForm = (event: any) => {
    event.preventDefault();
    let bodya = new FormData();
    bodya.set("file", uploadFile[0]);
    // bodya.append("company_id", new_ID);

    const { REACT_APP_API_URL } = process.env

    fetch(REACT_APP_API_URL + '/upload', {
      method: 'POST',
      body: bodya
    }).then(res => res.json())
      .then(getback => {
        setGetBackAC(getback.data)
        setGetBackMassage(getback.message)
      })

  }

  const acDetail = getBackAC
  const csvData = [
    [' ', 'Company', 'Name', 'IDcard', 'Address', 'Phone_Number', 'Birth', 'Email', 'Department', 'Post', 'Salary', 'Annual', 'Entry'],
    ['Example', 'TECKY', "Ahmed", "R1234567", "Phase II, Lei Tung Commercial Centre, Ap Lei Chau, Aberdeen", "91234567", "1999-10-10", "ah@smthing.co.com", "Human Resources Department", "clerk", '10000', '7', '2021-01-01'],
  ];

  const fileName = 'new_employee_input_model.csv'
  const result = "new_employee_account.csv"

  return (
    <div className="page">
      <form className="bulk_part">
        <br />
        <input type="file" name="file"

          onChange={(e) => setUploadFile(e.target.files)} />
        <br />
        <input type="submit" onClick={submitForm} />
        <div className="excelBtn">
          <ExportReactCSV csvData={csvData} fileName={fileName} button= "sample"  />
          <span className="messageBox">{getBackMassage}</span>
        </div>
      </form>
      <hr />
      <pre className="resultBtn">{acDetail ? (<>  <ExportReactCSV csvData={acDetail} fileName={result} button="result" />
        {uploadFile[0] ? (<><label className="downloadText">uploadFile:{uploadFile[0].name}</label>
          <label style={{ color: "white" }}>please,download user account result</label></>) : (null)}
      </>) : (null)}</pre>

    </div>
  );
}
export default UploadFile





