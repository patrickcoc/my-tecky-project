import React, { useState, ChangeEvent } from 'react'

function useTable(fieldList: string[][]) {
  const [state, setState] = useState<any>({})
  let table = (
    <table className="hook-table">
      <tbody>
        {fieldList.map(([field, type, options]) => {
          function onChange(
            e: ChangeEvent<HTMLInputElement | HTMLTextAreaElement | HTMLSelectElement>,
          ) {
            setState({
              ...state,
              [field]: e.target.value,
            })
          }
          return (
            <tr key={field}>
              <td className="title">{field}</td>
              <td>
                {type === 'textarea' ? (
                  <textarea value={state[field]} style={{width: 155}} rows={5} onChange={onChange} />
                ) : type === 'options' ? (
                  <select value={state[field]} style={{width: 155}} onChange={onChange}>
                    {options.split(",").map(text => <option>{text}</option>)}
                  </select>
                ) : type === 'radios' ? (
                  <div>
                    <input name="is-full-day" type="radio" value="1" onInput={() => {
                      setState({
                        ...state,
                        ["is_all_day"]: "1"
                      })
                    }} /> Full Day
                    <span>  </span>
                    <input name="is-full-day" type="radio" value="0.5"
                      onInput={() => {
                        setState({
                          ...state,
                          ["is_all_day"]: "0.5"
                        })
                      }} /> Half Day
                  </div>
                ) : (
                  <input value={state[field]} type={type} style={{width: 155}} onChange={onChange} />
                )}
              </td>
            </tr>
          )
        })}
      </tbody>
    </table>
  )
  return { state, setState, table }
}

export default useTable