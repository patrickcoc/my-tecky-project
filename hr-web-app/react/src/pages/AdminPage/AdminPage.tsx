import { Link } from 'react-router-dom'
import SliderBar from '../../components/Home/Slider'


function AdminPage() {

  return (
    <div className="admin_page">
      <div className="sliderBar">
        <SliderBar/>
        </div>
    <>


      <div className="adminBtn">
        <Link to='/admin_department'>
          <label >
            <span className="item">
              {/* <img className="adminIcon" src="https://campmanagement.com/wp-content/uploads/camp-icons-orange_staff-recruiting-1.svg" /> */}
              <i className="fas fa-users"></i>
              New Employee
            </span></label>
        </Link>

        <Link to='/admin_attendance'>
          <label  >
            <span className="item">
              {/* <img className="adminIcon" src="https://campmanagement.com/wp-content/uploads/camp-icons-orange_transportation-1.svg" /> */}
              <i className="fas fa-user-clock"></i>
              Attendeance</span>
          </label>
        </Link>

        <Link to='/admin_personal'>
          <label >
            <span className="item">
              {/* <img className="adminIcon" src="https://campmanagement.com/wp-content/uploads/camp-icons-orange_attendance-1.svg" /> */}
              <i className="fas fa-user-edit"></i>
              Personal
            </span></label>
        </Link>

        <Link to='/admin_leave'>
          <label>
            <span className="item">
              {/* <img className="adminIcon" src={leave_icon} /> */}
              <i className="far fa-calendar-times"></i>
              Pending Leave </span>
          </label>
        </Link>

        <Link to='/admin_roster'>
          <label >
            <span className="item">
              {/* <img className="adminIcon" src={roster_icon} /> */}
              <i className="far fa-calendar-alt"></i>
              Roster</span>
          </label>
        </Link>

        <Link to='/admin_scanner'>
          <label >
            <span className="item">
              {/* <img className="adminIcon" src={scanner_icon} /> */}
              <i className="fas fa-qrcode"></i>
              Scan QR </span>
          </label>
        </Link>

        <Link to='/admin_salary'>
          <label >
            <span className="item">
              {/* <img className="adminIcon" src={salary_icon} /> */}
              <i className="fas fa-file-invoice-dollar"></i>
              Salary
            </span></label>
        </Link>

      </div>
       </>
    </div>
   
  )
}
export default AdminPage