import React, { Component, useState } from "react";
import QrReader from "react-qr-reader";
import { callAPI } from "../../CallAPI";
import { dateFormat } from "../../components/Calendar/calendar";
import "./ScannQr.scss";

let message = ""
let date = "";
let type = "";
let user = "";
export class Scanner extends Component {
  state = {
    date: "No result",
    time: "",
    user: "None",
    type: "",
    message: ''
  };

  handleScan = (data: any) => {

    if (data) {
      let today = new Date();

      let inputMin: string | number = today.getMinutes();
      if (inputMin < 10) {
        inputMin = "0" + inputMin;
      }
      let inputHour: string | number = today.getHours();
      if (inputHour < 10) {
        inputHour = "0" + inputHour;
      }
      const inputTime = inputHour + ":" + inputMin;
      const getDate = dateFormat(today);

      this.setState({
        date: getDate,
        time: inputTime,
        user: user,
        type: type,
        message: message,
        user_id: localStorage.getItem("user_id"),
      });
      // if (
      date = getDate;
      user = data.split(",")[0];
      type = data.split(",")[1]
      // ) {
      //   console.log("repley")
      //   return
      // } else if (
      //   date !== getDate &&
      //   user !== data.split(",")[0] &&
      //   type !== data.split(",")[1]
      // )
      //  {
      //   date = getDate;
      //   type = data.split(",")[1];
      //   user = data.split(",")[0];

      callAPI<any, any>("POST", "/Qr_check", {
        state: {
          date: getDate,
          time: inputTime,
          type: type,
          user_id: user,
        },
      }).then(data => {
        message = data.message
      });

      // } 
      // else if (
      //   date === getDate &&
      //   user === data.split(",")[0] &&
      //   type === data.split(",")[1]
      // ){return}
      // else if (
      //   date === getDate &&
      //   user === data.split(",")[0] &&
      //   type !== data.split(",")[1]
      // ) {
      //   date = getDate;
      //   type = data.split(",")[1];
      //   user = data.split(",")[0];
      //   message="Success"
      //   callAPI<any, any>("POST", "/Qr_check", {
      //     state: {
      //       date: getDate,
      //       time: inputTime,
      //       type: type,
      //       user_id:user,
      //     },

      //   }).then(data => {
      //     message=data.message
      //   });;
      // } else if (
      //   date !== getDate &&
      //   user == data.split(",")[0] &&
      //   type == data.split(",")[1]
      // ) {
      //   date = getDate;
      //   type = data.split(",")[1];
      //   user = data.split(",")[0];
      //   message="Success"
      //   callAPI<any, any>("POST", "/Qr_check", {
      //     state: {
      //       date: getDate,
      //       time: inputTime,
      //       type: type,
      //       user_id:user,
      //     },
      //   }).then(data => {
      //     message=data.message
      //   });;
      // }
    }
  };

  handleError = (err: any) => {
    console.error(err)
  };
  render() {
    return (
      <div className="pageTitle">

        <div className="scanner">
          <QrReader
            delay={300}
            onError={this.handleError}
            onScan={this.handleScan}

          />
          <p className="result">
            {this.state.date} {this.state.time}(
            {this.state.type})
          </p>
          <p className="scan_result">{this.state.message}</p>
        </div>
      </div>
    );
  }
}

export default Scanner;
