import React, { useState } from "react";
import { Link, Switch, Route } from "react-router-dom";
import ApplyLeave from "../../components/Leave/ApplyLeave";
import { LeaveRecord } from "../../components/Leave/LeaveRecord";

function RecordPage() {
    // const [applyLeave, setApplyLeave] = useState(false)
    // function createApplyLeave() {
    //     setApplyLeave(!applyLeave)
    return (
        // <div className="page">
        //     {applyLeave ? (<><LeavePage /></>) : (<>
        //         <h1>Record</h1>
        //         <div className="record">
        //             <table>"record"</table>
        //         </div>
        //         <div className="applyLeave">
        //             <button onClick={createApplyLeave}>Apply Leave</button>
        //         </div>
        //     </>)}

        // </div>
        <>
        <Switch>
            <Route path="/leave" exact component={LeaveRecord} />
            <Route path="/leave/applyleave" component={ApplyLeave} />
        </Switch>
        </>
    )
}

export default RecordPage