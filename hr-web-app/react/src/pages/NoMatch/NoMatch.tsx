import React from 'react'
import notfound from "../../images/nofound.jpeg"
import "./NoMatch.scss"
function NoMatch() {
  return (
    <div className="NoMatchPage">

      <img  className='notFound' src={notfound} />
     
    </div>
  )
}
export default NoMatch