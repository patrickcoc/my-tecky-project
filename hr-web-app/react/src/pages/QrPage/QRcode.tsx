import QRCode from "qrcode.react";
import { useState } from "react";
import { useLocation, useRouteMatch } from "react-router-dom";
import { dateFormat } from "../../components/Calendar/calendar";
import './QrPage.scss';

function QrCode() {

    const location = useLocation()
    const params = new URLSearchParams(location.search)
    const in_out:any = useRouteMatch().params
 
    const my_id = localStorage.getItem('user_id')

    const value = my_id + ',' + in_out.type

    return (
        <div className="QrCode">

            <h1 className={`${in_out.type}_title`}>{in_out.type}</h1>

            <QRCode
                value={value}
                size={290}
                level={"H"}
                includeMargin={true}
            />
        </div>


    );
}
export default QrCode