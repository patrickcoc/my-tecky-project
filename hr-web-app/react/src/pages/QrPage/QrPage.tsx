import './QrPage.scss';
import { Button } from "reactstrap";
import { Link } from "react-router-dom";
import in_men from "../../images/inmen.png"
import out_women from "../../images/outwomen.png"
function QrPage() {

  return (
    <div className="App">
      <div className="qrcode">
        <div className='btn_list'>
          <div className='clickbtnQR Inbtn' >
            <Link to="/QR/In" className="link">
              <img  className='in_men' src={in_men} />
              <label>In</label></Link>
          </div>
          <div className='clickbtnQR Outbtn' >
            <Link to="/QR/Out" className="link">
            <img  className='out_women' src={out_women} />
              <label>Out</label></Link>
          </div>
          {/* <i className="far fa-times-circle" onClick={closeAll}></i> */}
        </div>

      </div>
    </div>
  );
}
export default QrPage