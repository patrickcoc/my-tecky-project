import React, { useState, useEffect } from "react";
import { Link, Switch, Route } from "react-router-dom";
import Calendar from "react-calendar";
import moment from "moment";
import "react-calendar/dist/Calendar.css";
import "./RosterPage.scss";
import { useSelector, useDispatch } from "react-redux";
import { RootState } from "../../redux";
import { getDepartmentDailyRosterThunk } from "../../redux/roster/thunk";
import { callAPI } from "../../CallAPI";

type monthlyRoster = {
  day: number;
  month: number;
  yaer: number;
  shift_type: string;
};

function Roster() {
  const dispatch = useDispatch();

  const [dateState, setDateState] = useState(new Date());
  const [currentMonthRoster, setCurrentMonthRoster] = useState([
    {
      day: 9,
      month: 99,
      year: 9999,
      shift_type: "689",
    },
  ]);
  const [nextMonthRoster, setNextMonthRoster] = useState([
    {
      day: 9,
      month: 99,
      year: 9999,
      shift_type: "689",
    },
  ]);

  const dailyDepartmentRoster = useSelector(
    (state: RootState) => state.roster.departmentDailyRoster
  );

  const changeDate = (event: any) => {
    setDateState(event);
  };

  useEffect(() => {
    let date = new Date(dateState);
    let day = date.getDate();
    let month = date.getMonth();
    let year = date.getFullYear();
    const adjustedMonth = month + 1;
    dispatch(getDepartmentDailyRosterThunk(day, adjustedMonth, year));
  }, [dateState]);
  const userId = localStorage.getItem("user_id");
  useEffect(() => {
  if(userId){
    callAPI<null, any>("GET", `/currentMonthSchedule/${userId}`)
      .then((data) => setCurrentMonthRoster(data))
      .catch((error) => {
        console.error(error.message);
      });
    }
  }, []);

  useEffect(() => {
  if(userId){
    callAPI<null, any>("GET", `/nextMonthSchedule/${userId}`)
      .then((data) => setNextMonthRoster(data))
      .catch((error) => {
        console.error(error.message);
      });
    }
  }, []);

  return (
    <>
    <div className="page_title">
    <h1>Roster</h1>
  </div>
    <div className="rosterPage">
    
      <div className="rosterOption">
        <div className="rosterOption">
          <Link className="rosterLink" to="/roster/daily">每日更</Link>
        </div>
        <div className="rosterOption">
          <Link className="rosterLink" to="/roster/thisMonth">本月更</Link>
        </div>
        <div className="rosterOption">
          <Link className="rosterLink" to="/roster/nextMonth">下月更</Link>
        </div>
      </div>

      <Switch>
        <Route path="/roster/daily">
          <div>
            <Calendar value={dateState} onChange={changeDate} />
          </div>
          <div className="onDuty">
            <br></br>
            <div>選擇日期：{moment(dateState).format("LL")}</div>
            <div>上班:</div>
            <div className="onDutyStaffList">
              <div className="staffTitle">
                <div className="title">職員編號: </div>
                <div className="title">姓名: </div>
                <div className="title">職位: </div>
                <div className="title">更份: </div>
              </div>
              {dailyDepartmentRoster ? (
                dailyDepartmentRoster.map((worker, i) => (
                  <div key={i} className="staffInfo">
                    <div className="item">{worker.staff_id}</div>
                    <div className="item">{worker.name}</div>
                    <div className="item">{worker.post}</div>
                    <div className="item">{worker.shift_type}</div>
                  </div>
                ))
              ) : (
                <div></div>
              )}
            </div>
          </div>
        </Route>
        <Route path="/roster/thisMonth">
          <div>
            <div className="personalMonthlyScheduleTitle">本月更</div>
            <div className="personalMonthlyScheduleContainer">
              {currentMonthRoster.map((roster,i) => (
                <div className="personalMonthlySchedule" key={i}>
                  {roster.day}/{roster.month}/{roster.year}: {roster.shift_type}
                </div>
              ))}
            </div>
          </div>
        </Route>
        <Route path="/roster/nextMonth">
          <div>
            <div className="personalMonthlyScheduleTitle">下月更</div>
            <div className="personalMonthlyScheduleContainer">
              {nextMonthRoster.map((roster ,i ) => (
                <div className="personalMonthlySchedule" key={i}>
                  {roster.day}/{roster.month}/{roster.year}: {roster.shift_type}
                </div>
              ))}
            </div>
          </div>
        </Route>
      </Switch>
    </div>
    </>
  );
}

export default Roster;
