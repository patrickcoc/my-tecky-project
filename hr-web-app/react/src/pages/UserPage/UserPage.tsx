import { getUserInfoThunk } from "../../redux/user/thunk";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "../../redux";
import "./UserPage.scss";
import { useEffect } from "react";

function UserPage() {
  const dispatch = useDispatch();

  let userInfo = useSelector((state: RootState) => state.user.userInfo);

  useEffect(() => {
    dispatch(getUserInfoThunk())
  }, [])


  return (
    <>
      <div className="page_title">
        <h1>Staff information</h1>
      </div>
      <div className="userPage">

        <div className="staffInfo">
          <div className="data_column">
          <div className="title">Company Name: </div>
          <div className="item">{userInfo.company_name}</div>
          </div>
          <div className="data_column">
          <div className="title">Department: </div>
          <div className="item">{userInfo.department_name}</div>
          </div>
          <div className="data_column">
          <div className="title">Staff Name: </div>
          <div className="item">{userInfo.name}</div>
          </div>
          <div className="data_column">
          <div className="title">Post: </div>
          <div className="item">{userInfo.post}</div>
          </div>
          <div className="data_column">
          <div className="title">Staff ID: </div>
          <div className="item">{userInfo.staff_id}</div>
          </div>
          <div className="data_column">
          <div className="title">Email Address: </div>
          <div className="item">{userInfo.email}</div>
          </div>
          <div className="data_column">
          <div className="title">Leave: </div>
          <div className="item">{userInfo.leave_day}</div>
          </div>
        </div>
      </div>
    </>
  );
}
export default UserPage;
