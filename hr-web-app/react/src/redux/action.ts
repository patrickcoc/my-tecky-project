import { CallHistoryMethodAction } from 'connected-react-router'
import { LoginAction } from './login/action'
import { AdminRosterAction } from './adminRoster/action'
import { CalcutorSalaryAction } from './calculatorSalary'
import { UserAction } from './user/action'
import { RosterAction } from './roster'

export type ActionType = LoginAction|CallHistoryMethodAction | AdminRosterAction|CalcutorSalaryAction | UserAction | RosterAction