import { Department, DepartmentRoster } from './state'
import { DepartmentStaff } from './state'

export function setDepartmentList(departmentList:Department[]){
    return {
        type: '@@AdminRoster/setDepartmentList' as const,
        departmentList,
    }
}

export function selectDepartment(selectedDepartmentId:number){
    return {
        type: '@@AdminRoster/selectedDepartment' as const,
        selectedDepartmentId,
    }
}

export function setStaffList(departmentStaffList:DepartmentStaff[]){
    return {
        type: '@@AdminRoster/setStaffList' as const,
        departmentStaffList,
    }
}

export function selectMonth(currentMonth:Date){
    return {
        type: '@@AdminRoster/selectedMonth' as const,
        currentMonth,
    }
}

export function setDepartmentRoster(departmentRoster:DepartmentRoster){
    return {
        type: '@@AdminRoster/setDepartmentRoster' as const,
        departmentRoster,
    }
}

export type AdminRosterAction = 
| ReturnType<typeof setDepartmentList>
| ReturnType<typeof selectDepartment>
| ReturnType<typeof setStaffList>
| ReturnType<typeof selectMonth>
| ReturnType<typeof setDepartmentRoster>