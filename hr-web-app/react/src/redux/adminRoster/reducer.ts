import { AdminRosterAction } from "./action";
import { AdminRosterState } from "./state";

const initialState: AdminRosterState = {
    departmentList: [],
    selectedDepartmentId: 0,
    staffList: [],
    departmentRoster: {}
}

export let adminRosterReducer = (
    state: AdminRosterState = initialState,
    action: AdminRosterAction
) => {
    switch(action.type){
        case '@@AdminRoster/setDepartmentList':
            return {
                ...state,
                departmentList: action.departmentList
            }
        case '@@AdminRoster/selectedDepartment':
            return {
                ...state,
                selectedDepartmentId: action.selectedDepartmentId
            }
        case '@@AdminRoster/setStaffList':
            return {
                ...state,
                staffList: action.departmentStaffList
            }
        case '@@AdminRoster/selectedMonth':
            return {
                ...state,
                currentMonth: action.currentMonth
            }
        case '@@AdminRoster/setDepartmentRoster':
            return {
                ...state,
                departmentRoster: action.departmentRoster
            }
        default:
            return state
    }
}