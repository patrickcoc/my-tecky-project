export type Department = {
    id: number
    department_name: string
}

export type DepartmentStaff = {
    staff_id: string
    name: string
    post: string
}

export type selectedMonth = {
    currentMonth: Date
}

export type DepartmentRoster = {
    [staff_id: string]: {
        [isoDate: string]: string
    }
}

export type AdminRosterState = {
    departmentList: Department[]
    selectedDepartmentId: number
    staffList: DepartmentStaff[]
    departmentRoster: DepartmentRoster
}