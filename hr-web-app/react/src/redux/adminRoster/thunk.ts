import { RootThunkDispatch } from "../store"
import { setDepartmentList, setDepartmentRoster, setStaffList } from './action'
import { callAPI } from '../../CallAPI'
import { DepartmentStaff, DepartmentRoster } from './state'

type DepartmentName = {
    id: number
    department_name: string
}

type DepartmentRosterInput = {
    departmentId: number
    month: number
}

export function getDepartmentNameThunk() {
    return async (dispatch: RootThunkDispatch) => {
        const userId = localStorage.getItem('user_id')
        await callAPI<null, DepartmentName[]>('GET', `/departmentList/${userId}`)
            .then(data => dispatch(setDepartmentList(data))
            )
            .catch(error => {
                console.error('error')
            })
    }
}

export function getDepartmentStaffListThunk(departmentId: number) {
    return async (dispatch: RootThunkDispatch) => {
        await callAPI<number, DepartmentStaff[]>('GET', `/departmentStaffList/${departmentId}`)
            .then(data => dispatch(setStaffList(data))
            )
            .catch(error => {
                console.error('error')
            })
    }
}

export function getDepartmentStaffRosterThunk(departmentId:number,month:number) {
    return async (dispatch: RootThunkDispatch) => {
        await callAPI<DepartmentRosterInput, DepartmentRoster>('POST', '/departmentStaffRoster',{
            departmentId,
            month
        })
            .then(data => dispatch(setDepartmentRoster(data)))
            .catch(error => {
                console.error('error')
            })
    }
}