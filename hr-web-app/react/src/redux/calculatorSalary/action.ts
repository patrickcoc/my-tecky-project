import { Department } from './state'
import { DepartmentStaff } from './state'

export function setDepartmentList(departmentList:Department[]){
    return {
        type: '@@AdminRoster/setDepartmentList' as const,
        departmentList,
    }
}

export function selectDepartment(selectedDepartmentId:number){
    return {
        type: '@@AdminRoster/selectedDepartment' as const,
        selectedDepartmentId,
    }
}

export function setStaffList(departmentStaffList:DepartmentStaff[]){
    return {
        type: '@@CalculatorSalary/setStaffList' as const,
        departmentStaffList,
    }
}

export type CalcutorSalaryAction = 
| ReturnType<typeof setDepartmentList>
| ReturnType<typeof selectDepartment>
| ReturnType<typeof setStaffList>