import { CalcutorSalaryAction } from "./action";
import { CalculatorSalaryState } from "./state";

const initialState: CalculatorSalaryState = {
    departmentList: [],
    selectedDepartmentId: 0,
    staffList: []
}

export let CalculatorSalaryReducer = (
    state: CalculatorSalaryState = initialState,
    action: CalcutorSalaryAction
) => {
    switch(action.type){
        case '@@AdminRoster/setDepartmentList':
            return {
                ...state,
                departmentList: action.departmentList
            }
        case '@@AdminRoster/selectedDepartment':
            return {
                ...state,
                selectedDepartmentId: action.selectedDepartmentId
            }
        case '@@CalculatorSalary/setStaffList':
            return {
                ...state,
                staffList: action.departmentStaffList
            }
        default:
            return state
    }
}