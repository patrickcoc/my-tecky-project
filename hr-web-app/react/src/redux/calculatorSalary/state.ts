export type Department = {
    id: number
    department_name: string
}

export type DepartmentStaff = {
    staff_id: string
    name: string
    salary: number
    remark:string
    checkbox:boolean
}

export type CalculatorSalaryState = {
    departmentList: Department[]
    selectedDepartmentId: number
    staffList: DepartmentStaff[]
}