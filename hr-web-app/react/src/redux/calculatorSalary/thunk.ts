import { RootThunkDispatch } from "../store"
import { setDepartmentList, setStaffList } from './action'
import { callAPI } from '../../CallAPI'
import { DepartmentStaff } from './state'

type DepartmentName = {
    id: number
    department_name: string
}

export function getDepartmentNameThunk() {
    return async (dispatch: RootThunkDispatch) => {
        const userId = localStorage.getItem('user_id')
        await callAPI<null, DepartmentName[]>('GET', `/departmentList/${userId}`)
            .then(data => dispatch(setDepartmentList(data))
            )
            .catch(error => {
                console.error('error')
            })
    }
}

export function getDepartmentStaffListThunk(departmentId:number){
    return async (dispatch: RootThunkDispatch) => {
        await callAPI<number,DepartmentStaff[]>('GET',`/departmentStaffList_getsalary/${departmentId}`)
            .then(data => dispatch(setStaffList(data))
            )
            .catch(error => {
                console.error('error')
            })
    }
}