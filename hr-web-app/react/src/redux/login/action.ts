export function failedAction(reason: string) {
  return {
    type: '@@login/failed' as const,
    reason,
  }

} export function loginAction(admin:boolean) {
  return {
    type: '@@login/success' as const,
    admin,
  }
}export function logoutAction() {
  return {
    type: '@@auth/logout' as const,
  }
}
export function loginMessage(message:string) {
  return {
    type: '@@auth/message' as const,
    message
  }
}
export type LoginAction =
  | ReturnType<typeof failedAction>
  | ReturnType<typeof loginAction>
  | ReturnType<typeof logoutAction>
  | ReturnType<typeof loginMessage>