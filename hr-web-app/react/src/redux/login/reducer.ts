import { LoginAction } from './action'
import { AuthState } from './state'

function changeBoolen(target: string | null) {
    if (target === "ture") {
        return true
    } if (target === "false") {
        return false
    }
}
const initialState: AuthState = {
    isAuthenticated: !!localStorage.getItem('token'),
    isAdmin: (changeBoolen(localStorage.getItem('admin')))
}
export const authReducer = (
    state: AuthState = initialState,
    action: LoginAction,
): AuthState => {
    switch (action.type) {
        case '@@login/success': {
            try {
                return {
                    isAuthenticated: true,
                    isAdmin: action.admin
                }
            } catch (error) {
                return {
                    isAuthenticated: false,
                    errorMessage: 'Failed to decode JWT: ' + error.toString(),
                }
            }
        }
        case '@@login/failed': {
            return {
                isAuthenticated: false,
                errorMessage: action.reason,
            }
        }
        case '@@auth/logout': {
            return {
                isAuthenticated: false,
            }
        }
        case '@@auth/message': {
            return {
                errorMessage:action.message
            }
        }
        default:
            return state
    }
}