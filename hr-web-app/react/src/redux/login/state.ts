export type AuthState = {
    isAuthenticated?: boolean
    errorMessage?: string
    isAdmin?:boolean
  }