import { RootThunkDispatch } from '../store'
import { /*failedAction,*/ loginAction, loginMessage, logoutAction } from './action'
import { callAPI } from '../../CallAPI'
import { push } from 'connected-react-router'

// const { REACT_APP_API_SERVER } = process.env

type Input = {
    username: string,
    password: string
}

type UserType = {
    id: string,
    staff_id: string,
    company_id: string,
    admin: boolean,
    department_id: string
}

type LoginResult = {
    message:string;
    user: UserType,
    token: string
}
function checkString(admin: boolean) {
    if (admin) {
        return "ture"
    }
    else {
        return "false"
    }
}
export function userSubmit(username: string, password: string) {
    return async (dispatch: any) => {
        try {
            const {user: userInfo, token ,message} = await callAPI<Input, LoginResult>('POST', '/login', {
                username: username,
                password: password
            })
         if(userInfo){
            localStorage.setItem('user_id', userInfo.id)
            localStorage.setItem('company_id', userInfo.company_id)
            localStorage.setItem('department_id', userInfo.department_id)
            localStorage.setItem('username', userInfo.staff_id)
            localStorage.setItem('token', token)
            const newAdmin = checkString(userInfo.admin)
            localStorage.setItem('admin', newAdmin)
            dispatch(loginAction(userInfo.admin))
            dispatch(push('/'))
         }
         else{
            dispatch(loginMessage(message))
         }
        }
        catch (error) {
            console.error('Native Login Failed', error)
            console.error(dispatch)
        }
    }
}


export function logoutThunk() {
    return (dispatch: RootThunkDispatch) => {
        localStorage.clear()
        // localStorage.removeItem('token')
        // localStorage.removeItem('username')
        // localStorage.removeItem('user_id')
        // localStorage.removeItem('company_id')
        // localStorage.removeItem('department_id')
        dispatch(logoutAction())
    }
}
