import { connectRouter } from 'connected-react-router'
import { history } from './history'
import { combineReducers, Reducer } from 'redux'
import { RootState } from './state'
import { ActionType } from './action'

import { authReducer } from './login/reducer'
import { adminRosterReducer } from './adminRoster/reducer'
import { CalculatorSalaryReducer } from './calculatorSalary'
import { userReducer } from './user/reducer'
import { rosterReducer } from './roster/reducer'

export const rootReducer: Reducer<RootState, ActionType> = combineReducers({
    router: connectRouter(history),
    auth: authReducer,
    adminRoster: adminRosterReducer,
    CalculatorSalary:CalculatorSalaryReducer,
    user: userReducer,
    roster: rosterReducer
})