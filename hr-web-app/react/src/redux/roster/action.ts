import { departmentDailyRoster } from './state' 

export function setDepartmentDailyRoster(departmentDailyRoster:departmentDailyRoster[]){
    return {
        type: '@@Roster/setDepartmentDailyRoster',
        departmentDailyRoster,
    }
}

export type RosterAction = 
| ReturnType<typeof setDepartmentDailyRoster>