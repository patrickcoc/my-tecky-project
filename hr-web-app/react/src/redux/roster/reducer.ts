import { RosterState } from "./state";
import { RosterAction } from "./action";

const initialState: RosterState = {
    departmentDailyRoster: []
}

export let rosterReducer = (
    state: RosterState = initialState,
    action: RosterAction
) => {
    switch(action.type){
        case '@@Roster/setDepartmentDailyRoster':
            return {
                ...state,
                departmentDailyRoster: action.departmentDailyRoster
            }
        default:
            return state
    }
}