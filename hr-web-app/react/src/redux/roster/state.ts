export type departmentDailyRoster = {
    staff_id: string
    name: string
    post: string
    shift_type: string
}

export type RosterState = {
    departmentDailyRoster: departmentDailyRoster[]
}