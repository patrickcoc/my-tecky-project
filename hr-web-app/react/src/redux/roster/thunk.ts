import { RootThunkDispatch } from "../store";
import { setDepartmentDailyRoster } from './action'
import { callAPI } from "../../CallAPI";
import { departmentDailyRoster } from './state'

type SelectDay = {
    user_id: string | null
    day: number
    month: number
    year: number
}

export function getDepartmentDailyRosterThunk(day:number,month:number,year:number){
    return async (dispatch: RootThunkDispatch) => {
        const user_id = localStorage.getItem("user_id")
        await callAPI<SelectDay,departmentDailyRoster[]>('POST','/dailySchedule',{
            user_id,
            day,
            month,
            year,
        }).then(data => dispatch(setDepartmentDailyRoster(data)))
        .catch(error => {
            console.error(error.message)
        })
    }
}