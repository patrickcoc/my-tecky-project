import { RouterState } from 'connected-react-router'
import { AuthState } from './login/state'
import { AdminRosterState } from './adminRoster/state'
import { CalculatorSalaryState } from './calculatorSalary'
import { UserState } from './user/state'
import { RosterState } from './roster/state'


export type RootState = {
  router: RouterState
  auth: AuthState
  adminRoster: AdminRosterState
  CalculatorSalary:CalculatorSalaryState
  user: UserState
  roster: RosterState
}