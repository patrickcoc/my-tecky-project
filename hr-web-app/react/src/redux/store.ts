import {createStore, compose, applyMiddleware} from "redux";
import { rootReducer } from './reducer'
import logger from 'redux-logger';
import thunk, { ThunkDispatch } from 'redux-thunk'
import { routerMiddleware } from "connected-react-router";
import {history} from "./history"
import { ActionType } from "./action";
import { RootState } from "./state";

export type RootThunkDispatch = ThunkDispatch<RootState, null, ActionType>


declare global {
    /* tslint:disable:interface-name */
    interface Window {
        __REDUX_DEVTOOLS_EXTENSION__: any
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__:any
    }
}


const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


export let store= createStore <RootState, ActionType,{},{}> (
    rootReducer,
    composeEnhancers(
        // applyMiddleware(logger),
        applyMiddleware(routerMiddleware(history)),
        applyMiddleware(thunk),
      )
);