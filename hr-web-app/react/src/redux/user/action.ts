import { UserInfo } from './state'

export function setUserInfo(userInfo:UserInfo){
    return {
        type: '@@User/setUserInfo' as const,
        userInfo,
    }
}

export type UserAction = 
| ReturnType<typeof setUserInfo>