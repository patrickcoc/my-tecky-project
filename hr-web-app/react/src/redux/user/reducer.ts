import { UserState } from "./state";
import { UserAction } from "./action"

const initialState: UserState = {
    userInfo: {
        company_name: 'Tecky',
        department_name: 'IT Department',
        post: 'IT Dog',
        name: 'WFC',
        staff_id: '00001476',
        email: 'itdog@wist2work.com',
        leave_day: 12,

    }
}

export let userReducer = (
    state: UserState = initialState,
    action: UserAction
) => {
    switch(action.type){
        case '@@User/setUserInfo':
            return {
            ...state,
            userInfo: action.userInfo
        }
        default:
            return state
    }
}