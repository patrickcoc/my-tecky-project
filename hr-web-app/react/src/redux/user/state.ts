export type UserInfo = {
    company_name: string
    department_name: string
    post: string
    name: string
    staff_id: string
    email: string
    leave_day: number
}

export type UserState = {
    userInfo: UserInfo
}