import { RootThunkDispatch } from "../store";
import { setUserInfo } from "./action";
import { callAPI } from "../../CallAPI";
import { UserInfo } from './state'

export function getUserInfoThunk() {
    return async(dispatch: RootThunkDispatch ) => {
        const userId = localStorage.getItem('user_id')
        await callAPI<null, UserInfo>('GET',`/userProfile/${userId}`)
            .then(data => dispatch(setUserInfo(data)))
            .catch(error => {
                console.error('error')
            })
    }
}