import{ parse } from "papaparse"
import { readFileSync } from "fs"

export function ReadCsv(file:string){
    let input=readFileSync(file).toString()
    let result=parse(input,{header:true})
    return (result.data)
 
}
