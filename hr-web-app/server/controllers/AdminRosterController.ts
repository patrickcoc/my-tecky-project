import { Request, Response } from 'express'
import { AdminRosterService } from '../service/AdminRosterService'

export class AdminRosterController {
    constructor(private adminRosterService: AdminRosterService) {}

    getDepartmentList = async(req: Request, res: Response) => {
        try{
            const userId = parseInt(req.params.userId)
            // check userid validity
            const isAdmin = await this.adminRosterService.checkIsAdmin(userId)
            if(!isAdmin){
                res.status(400).json({message:'invalid user'})
                return
            }
            const departmentnames = await this.adminRosterService.getDepartmentList(userId)
            res.status(200).json({data:departmentnames})
        } catch(err) {
            console.error(err.message)
            res.status(500).json({message:"internal server error"})
        }
    }

    getDepartmentStaffList = async(req: Request, res: Response) => {
        try{
            const departmentId = parseInt(req.params.departmentId)
            const departmentStaffList = await this.adminRosterService.getDepartmentStaffList(departmentId)
            res.status(200).json({data:departmentStaffList})
        } catch(err){
            console.error(err.message)
            res.status(500).json({message:"internal server error"})
        }
    }

    getDepartmentStaffRoster = async(req: Request, res: Response) => {
        try{
            const department = +req.body.departmentId
            const start = new Date(req.body.start).toISOString()
            const end = new Date(req.body.end).toISOString()
            const departmentStaffRoster = await this.adminRosterService.getDepartmentStaffRoster(department,start,end)
            res.status(200).json({data:departmentStaffRoster})
        } catch(err) {
            console.error(err.message)
            res.status(500).json({message:"internal server error"})
        }
    }
}