import { AttendanceRecordService } from "../service/AttendanceRecord"
import { Request, Response } from "express"


export class AttendanceRecordController {
    constructor(private AttendanceRecordService: AttendanceRecordService) { }

    GetAttendanceRecord = async (req: Request, res: Response) => {
        try {
            const date = req.body.date
            const attendanceRecord=await this.AttendanceRecordService.getAttendanceRecord(date);
            res.json({data:attendanceRecord})
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    GetUserAttendanceRecord = async (req: Request, res: Response) => {
        try {
            const date = req.body.date
            const id =req.body.user_id
            const userAttendanceRecord=await this.AttendanceRecordService.getUserAttendanceRecord(date,id);
            res.json({data:userAttendanceRecord})
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    GetMonthUserAttendanceRecord = async (req: Request, res: Response) => {
        try {
            const date = req.body.date
            const id =req.body.user_id
            const userMonthAttendanceRecord=await this.AttendanceRecordService.getUserMonthAttendanceRecord(date,id);
            res.json({data:userMonthAttendanceRecord})
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    GetMonthAttendanceRecord = async (req: Request, res: Response) => {
        try {
            const date = req.body.date
            const monthAttendanceRecord=await this.AttendanceRecordService.getMonthAttendanceRecord(date);
            res.json({data:monthAttendanceRecord})
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    postAttendanceRecord = async (req: Request, res: Response) => {
        try {
            const data = req.body
            const checked=await this.AttendanceRecordService.checkAttendance(data);
        if(checked<=0){
            await this.AttendanceRecordService.postAttendanceRecord(data);
            res.json({data:{
                message: `newRecord succsess`
            }})
        }
        else{    
            res.json({data:{
            message: `reply record`
        }})}
        } catch (err) {
            console.error(err.message)
            res.status(500).json({data:{
                message: `error record`
            }})
        }
    }
    delAttendanceRecord = async (req: Request, res: Response) => {
        try {
            const Record = req.body.data
            await this.AttendanceRecordService.deleteAttendanceRecord(Record);
            res.json({
                message: `delRecord succsess`
            })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }
    addAttendanceRecord = async (req: Request, res: Response) => {
        try {
            const data = req.body.data
            const user_id=await this.AttendanceRecordService.getUsers_id(data);
            await this.AttendanceRecordService.addAttendanceRecord(data,user_id);
            res.json({data:{
                message: `add record succsess`
            }})
        } catch (err) {
            console.error(err.message)
            res.status(500).json({data:{ message: "internal server error" }})
        }
    }
}