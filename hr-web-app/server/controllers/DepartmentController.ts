import { GetDepartmentService } from "../service/Department"
import { Request, Response } from "express"


export class DepartmentController {
    constructor(private GetDepartmentService: GetDepartmentService) { }

    GetDepartmentList = async (req: Request, res: Response) => {
        try {
            const departmentList=await this.GetDepartmentService.GetDepartment();
            res.json({
                data:departmentList,
                message: `input succsess`
            })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }
    addNewDepartment = async (req: Request, res: Response) => {
        try {
            const name = req.body.newDepartment
            const newDepartment=await this.GetDepartmentService.addNewDepartment(name);
            res.json({
                data:newDepartment,
                message: `add succsess`
            })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }
}