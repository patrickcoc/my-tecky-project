import { CsvService } from "../service/CSVService"
import express, { Request, Response } from "express"
import multer from "multer"
import { ReadCsv } from "../Text"
// import { isAdmin } from "../guards"
function check(data: any) {
    if (typeof data == "undefined") {
        return "is undefined"
    }
    else { return data }

}



export class InserCsvController {
    constructor(private CsvService: CsvService) { }

    toRouter(){
        const InserCsvRoutes = express.Router()

        var storage = multer.diskStorage({
            destination: "./uploads",
            filename: function (req, file, cb) {
              cb(null, `${Date.now()}-${file.fieldname}-${file.originalname}`);
            }
          })
           var upload = multer({ storage: storage })
        InserCsvRoutes.post('/upload',upload.single('file'),this.InsertCsv)
        return InserCsvRoutes
      }

    InsertCsv = async (req: Request, res: Response, err: any) => {
        try {
            if (err instanceof multer.MulterError) {
                res.status(500).json(err)
            }
            else {
                const filePath = check(req.file)
                const data = ReadCsv(filePath.path)

                let returnForm = []
                for (let i = 1; i < data.length; i++) {
                    const company_id=await this.CsvService.getCompanyID(data[i]);
                    const department_id = await this.CsvService.getDepartmentID(data[i]);
                    if (department_id && company_id) {
                        const staff_id = await this.CsvService.getcount()
                        const user_ac = await this.CsvService.CsvCreateUser(data[i], department_id.id, staff_id,company_id.id);
                        const newArrary = Object.assign(user_ac[0],data[i])
                        returnForm.push(newArrary)
                        await this.CsvService.csvInsert(data[i], user_ac);
                    }
                }
                res.status(200).json({ message:"Success",data: returnForm })
            }

        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "error" })
        }
    }

}