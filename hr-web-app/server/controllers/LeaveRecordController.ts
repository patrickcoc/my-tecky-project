import { LeaveRecordService } from "../service/LeaveRecordService"
import { Request, Response } from "express"
import { LeaveRecord } from "../models"

export class LeaveRecordController {
    constructor(private leaveService: LeaveRecordService) { }
    applyLeave = async (req: Request, res: Response) => {
        try {
            const leave: LeaveRecord = {
                users_id: req.jwtPayload.id,
                leave_type: req.body.leave_type,
                leave_date: req.body.leave_date,
                is_all_day: req.body.is_all_day,
                pay_leave: false,
                remark: req.body.remark,
                application_status: "pending"
            }
            await this.leaveService.applyLeave(leave)
            res.status(200).json({ data: leave, message: "applied" })
        } catch (error) {
            console.error(error.message)
            res.status(500).json({ message: "Error from LeaveRecordController" })
        }
    }
    getLeaveDetail = async (req: Request, res: Response) => {
        try {
            const id = req.jwtPayload.id
            const leaveId = +req.params.id
            const getLeaveDetail = await this.leaveService.getLeaveDetail(leaveId)
            if (!getLeaveDetail) {
                res.status(404).json({ message: "error 404 not found" })
            } else if (req.jwtPayload.isAdmin || getLeaveDetail?.users_id == id) {
                res.status(200).json({ data: getLeaveDetail })
            } else if(!req.jwtPayload.isAdmin ){
                res.status(403).json({ message: "invalid permission" })
            }
        } catch (error) {
            console.error(error.message)
            res.status(500).json({ message: "Error from LeaveRecordController" })
        }
    }
    getLeaveRecord = async (req: Request, res: Response) => {
        try {
            const id = req.jwtPayload.id
            const getLeave = await this.leaveService.getLeaveResult(id)
            res.status(200).json({ data: getLeave })
        } catch (error) {
            console.error(error.message)
            res.status(500).json({ message: "Error from LeaveRecordController" })
        }
    }
    getPendingLeave = async (req: Request, res: Response) => {
        try {
            const getPending = await this.leaveService.getAllPendingLeave()
            res.status(200).json({ data: getPending })
        } catch (error) {
            console.error(error.message)
            res.status(500).json({ message: "Error from LeaveRecordController" })
        }
    }
    getAllLeaveRecord = async (req: Request, res: Response) => {
        try {
            const getAllLeave = await this.leaveService.getAllLeaveResult()
            res.status(200).json({ data: getAllLeave, message: "get all leave success" })
        } catch (error) {
            console.error(error.message)
            res.status(500).json({ message: "Error from LeaveRecordController" })
        }
    }
    approveLeave = async (req: Request, res: Response) => {
        try {
            const id: number = req.body.id
            await this.leaveService.approveLeave(id)
            res.status(200).json({ data: id, message: "approved" })
        } catch (error) {
            console.error(error.message)
            res.status(500).json({ message: "Error from LeaveRecordController" })
        }
    }
    declineLeave = async (req: Request, res: Response) => {
        try {
            const id: number = req.body.id
            await this.leaveService.declineLeave(id)
            res.status(200).json({ data: id, message: "declined" })
        } catch (error) {
            console.error(error.message)
            res.status(500).json({ message: "Error from LeaveRecordController" })
        }
    }
    isPayLeave = async (req: Request, res: Response) => {
        try {
            const id: number = req.body.id
            const payLeave: boolean = req.body.payLeave
            await this.leaveService.isPayLeave(id, payLeave)
            res.status(200).json({ data: id, message: "declined" })
        } catch (error) {
            console.error(error.message)
            res.status(500).json({ message: "Error from LeaveRecordController" })
        }
    }
    getCurrentMonthDepartmentStaffLeaveApplicationRecord = async(req: Request, res: Response) => {
        try{
            const departmentId = req.body.departmentId
            const startDay = req.body.startDay
            const endDay = req.body.endDay
            const departmentLeaveApplication = await this.leaveService.getCurrentMonthDepartmentStaffLeaveApplicationRecord(departmentId,startDay,endDay)
            res.status(200).json({data:departmentLeaveApplication})
        } catch (err) {
            console.error(err.message)
            res.status(500).json({message: "internal server error"})
        }
    }
}