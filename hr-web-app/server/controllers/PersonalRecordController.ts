import { PersonalRecordService } from "../service/PersonalData"
import { Request, Response } from "express"


export class PersonalRecordController {
    constructor(private personalRecordService: PersonalRecordService) { }

    CreatePersonalRecord = async (req: Request, res: Response) => {
        try {
            const data = req.body.employeeData
            if (!data) {
                res.status(400).json({data:{ message: "missing input" }})
                return
            }
            await this.personalRecordService.CreatePersonalRecord(data);
            res.json({data:{
                message: `input succsess`
            }})
        } catch (err) {
            console.error(err.message)
            res.status(500).json({data:{ message: "error" }})
        }
    }

    UpdatePersonalRecord = async (req: Request, res: Response) => {
        try {
            const data = req.body.data
            if (!data) {
                res.status(400).json({ message: "missing input" })
                return
            }
            await this.personalRecordService.UpdatePersonalRecord(data);
            res.json({
                message: `update succsess`
            })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "error" })
        }
    }

    getCount = async (req: Request, res: Response) => {
        try {
            const count= await this.personalRecordService.getcount();
            res.json({
                data:count
            })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    getUserProfile = async (req: Request, res: Response) => {
        try{
            const userId = parseInt(req.params.userId)
            const personalProfile = await this.personalRecordService.getUserProfile(userId);
            res.status(200).json({data:personalProfile})
        } catch(err) {
            console.error(err.message)
            res.status(500).json({ message: "in\d dternal server error"})
        }
    }
}