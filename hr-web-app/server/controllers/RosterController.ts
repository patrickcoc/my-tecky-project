import { Request, Response } from 'express'
import { RosterService } from '../service/RosterService'


export class RosterController {
    constructor(private rosterService: RosterService){}

    dailySchedule = async(req: Request, res: Response) => {
        try{
            const day = req.body.day
            const month = req.body.month
            const year = req.body.year
            const user_id = req.body.user_id
            const schedule = await this.rosterService.getDailyDepartmentRoster(day,month,year,user_id)
            res.status(200).json({data:schedule})
        } catch(e) {
            console.error(e.message)
            res.status(500).json({message:'internal server error'})
        }
    }

    currentMonthSchedule = async(req: Request, res: Response) => {
        try{
            const userId = parseInt(req.params.userId)
            const date = new Date()
            const currentMonth = date.getMonth() + 1
            const currentYear = date.getFullYear()
            const currentMonthSchedule = await this.rosterService.getPersonalMonthlySchedule(currentMonth,currentYear,userId)
            res.status(200).json({data:currentMonthSchedule})
        } catch(e) {
            console.error(e.message)
            res.status(500).json({message:'internal server error'})
        }
    }

    nexttMonthSchedule = async(req: Request, res: Response) => {
        try{
            const userId = parseInt(req.params.userId)
            const date = new Date()
            const currentMonth = date.getMonth() + 2
            let currentYear = date.getFullYear()
            const nextYear = currentMonth >= 12? currentYear + 1 :currentYear
            const currentMonthSchedule = await this.rosterService.getPersonalMonthlySchedule(currentMonth,nextYear,userId)
            res.status(200).json({data:currentMonthSchedule})
        } catch(e) {
            console.error(e.message)
            res.status(500).json({message:'internal server error'})
        }
    }

    createMonthSchedule = async(req: Request, res: Response) => {
        try{
            const data = req.body.data
            await this.rosterService.createNewMonthlySchedule(data)
            res.status(200).json({data:'success'})
        } catch(e) {
            console.error(e.message)
            res.status(500).json({message:'internal server error'})
        }
    }
}