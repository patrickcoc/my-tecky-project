import { Request, Response } from 'express'
import { SalaryService } from '../service/SalaryServer'

export class SalaryController {
    constructor(private SalaryService: SalaryService) { }

    getDepartmentList = async (req: Request, res: Response) => {
        try {
            const userId = parseInt(req.params.userId)
            const departmentnames = await this.SalaryService.getDepartmentList(userId)
            res.status(200).json({ data: departmentnames })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    getDepartmentStaffList = async (req: Request, res: Response) => {
        try {
            const departmentId = parseInt(req.params.departmentId)
            const departmentStaffList = await this.SalaryService.getDepartmentStaffList(departmentId)
            res.status(200).json({ data: departmentStaffList })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }
    addRemark = async (req: Request, res: Response) => {
        try {
            const staff_id = req.body.id
            const content = req.body.remark
            const check = req.body.checkBox
            const user_id = await this.SalaryService.getUserId(staff_id)
            if (user_id) {
                const update = await this.SalaryService.updateRemark(user_id.id, content, check)
                if (!update) {
                   await this.SalaryService.addRemark(user_id.id, content, check)         
                }
                res.status(200).json({ data: { message: "update success" } })
            }
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }

    getStaffData = async (req: Request, res: Response) => {
        try {
            const staff_id = req.body.user.id
            const date = req.body.date
            const departmentStaffList = await this.SalaryService.getStaffData(staff_id, date)
            res.status(200).json({ data: departmentStaffList })
        } catch (err) {
            console.error(err.message)
            res.status(500).json({ message: "internal server error" })
        }
    }
}