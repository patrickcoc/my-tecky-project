import { UserService } from "../service/UserService"
import express, { Request, Response } from "express"
import { checkPassword } from "../hash"
import jwtSimple from 'jwt-simple';
import jwt from '../jwt';
import { isAdmin } from "../guards";

export class UserController {
  constructor(private userService: UserService) { }

  toRouter(){

     const userRoutes = express.Router()

    userRoutes.post('/login', this.login)
    
    userRoutes.post('/create_ac',isAdmin, this.createUser)
    
    userRoutes.post('/get_personal_data',isAdmin, this.getUser)
    
    userRoutes.post('/delet_user',isAdmin, this.deletUser)


    return userRoutes
  }

  login = async (req: Request, res: Response) => {
    try {
      const user_input = req.body
      if (!user_input.username || !user_input.password) {
        res.status(400).json({data:{ message: "missing username/password" }})
        return
      }

      const user = await this.userService.login(user_input.username)
      const admin = await this.userService.checkAdmin(user_input.username)
      if (!user || !(await checkPassword(user_input.password, user.password))) {
        res.status(400).json({data:{ message: "invalid username/password" }})
        return
      }
      if (!admin) {
        return 
      }
     
      const payload = {
        id: user.id,
        username: user.staff_id,
        isAdmin: admin.admin
      };
      const token = jwtSimple.encode(payload, jwt.jwtSecret);

      res.status(200).json({  data: { user, token ,message: "login success"} })

    } catch (err) {
      console.error("err500", err.message)
      res.status(500).json({data:{ message: "server error" }})
    }
  }

  createUser = async (req: Request, res: Response) => {
    try {
      const user_ac = req.body
      const personal_data=req.body.data
   
      const user_id = await this.userService.createUser(user_ac,personal_data)
    
      await this.userService.CreatePersonalRecord(personal_data,user_id[0]);
    

      res.status(200).json({data:{message:"create success" }})
    } catch (err) {
      console.error("create 500", err.message)
      res.status(500).json({data:{ message:"internal server error" }})
    }
  }

  getUser = async (req: Request, res: Response) => {
    try {
      const id = req.body.select
      const user_data = await this.userService.getAllUserData(id)
 
      res.status(200).json( {data: user_data} )
    } catch (err) {
      console.error("create 500", err.message)
      res.status(500).json({ message: "internal server error" })
    }
  }
  deletUser = async (req: Request, res: Response) => {
    try {
      const data = req.body.user
     await this.userService.delUserPersonalData(data)
     await this.userService.delUserData(data)
      res.status(200).json( {message:"del success"} )
    } catch (err) {
      console.error("create 500", err.message)
      res.status(500).json({ message: "internal server error" })
    }
  }
}
