import { Bearer } from 'permit';
import jwtSimple from 'jwt-simple';
import express from 'express';
import jwt from './jwt';

import { JwtPayload } from './models';

const permit = new Bearer({
    query: "access_token"
})

export async function isLoggedIn(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction) {
    try {
        const token = permit.check(req);
        if (!token) {
            return res.status(401).json({ msg: "Permission Denied" });
        }
        const payload: JwtPayload = jwtSimple.decode(token, jwt.jwtSecret);
        req.jwtPayload = payload

        return  next()

    } catch (e) {
        return res.status(401).json({ msg: "Permission Denied" });
    }
}

export async function isAdmin(
    req: express.Request,
    res: express.Response,
    next: express.NextFunction) {
    try {
        const token = permit.check(req);
        if (!token) {
            return res.status(401).json({ msg: "Permission Denied" });
        }
        const payload: JwtPayload = jwtSimple.decode(token, jwt.jwtSecret);
 
        if (!payload.isAdmin) {
            return res.status(401).json({ msg: "Permission Denied" });
        }

        req.jwtPayload = payload
        return next()

    } catch (e) {
        return res.status(401).json({ msg: "Permission Denied" });
    }
}