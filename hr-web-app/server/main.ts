import express from 'express';
import Knex from 'knex';
// import { Request, Response } from 'express';
import cors from 'cors'
import path from 'path'

const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || 'development']);

const app = express();
app.use(cors())
app.use(express.urlencoded({ extended: true }));
app.use(express.json());

app.use(express.static("public"))

import { PersonalRecordService } from "./service/PersonalData";
import { PersonalRecordController } from "./controllers/PersonalRecordController";

const personalRecordService = new PersonalRecordService(knex)
export const personalRecordController = new PersonalRecordController(personalRecordService)
import { personalRecordRoutes } from "./routes/personalRecodRoutes"


import { GetDepartmentService } from "./service/Department";
import { DepartmentController } from "./controllers/DepartmentController";

const getDepartmentService = new GetDepartmentService(knex)
export const departmentController = new DepartmentController(getDepartmentService)
import { departmentRoutes } from "./routes/DepartmentRoutes"


import { UserService } from "./service/UserService";
import { UserController } from "./controllers/UserController";

export  const userService = new UserService(knex)
 const userController = new UserController(userService)
// import { userRoutes } from "./routes/UserRoutes"


import { AttendanceRecordService } from "./service/AttendanceRecord";
import { AttendanceRecordController } from "./controllers/AttendanceRecordController";

const attendanceRecordService = new AttendanceRecordService(knex)
export const attendanceRecordController = new AttendanceRecordController(attendanceRecordService)
import { attendanceRecordRoutes } from "./routes/AttendanceRoutes"

import { RosterService } from './service/RosterService'
import { RosterController } from './controllers/RosterController'

const rosterService = new RosterService(knex)
export const rosterController = new RosterController(rosterService)
import { rosterRoutes } from './routes/RosterRoutes';


import { AdminRosterService } from './service/AdminRosterService'
import { AdminRosterController } from './controllers/AdminRosterController'

const adminRosterService = new AdminRosterService(knex)
export const adminRosterController = new AdminRosterController(adminRosterService)
import { adminRosterRoutes } from './routes/AdminRosterRoutes'

import { CsvService } from './service/CSVService'
import { InserCsvController } from './controllers/InserCsvController'

const csvService = new CsvService(knex)
export const inserCsvController = new InserCsvController(csvService)
// import { InserCsvRoutes } from './routes/CsvRoutes'
// import { ReadCsv } from "./Text"


import { LeaveRecordService } from "./service/LeaveRecordService"
import { LeaveRecordController }from "./controllers/LeaveRecordController"

const LRS = new LeaveRecordService(knex)
export const leaveRecordController = new LeaveRecordController(LRS)
import { leaveRoutes } from "./routes/LeaveRoutes"

import { SalaryService } from './service/SalaryServer'
import { SalaryController } from './controllers/SalaryControllers'

const salaryService = new SalaryService(knex)
export const salaryController = new SalaryController(salaryService)
import { SalaryRoutes } from './routes/SalaryRoutes'


app.use(SalaryRoutes)
app.use(inserCsvController.toRouter())
app.use(attendanceRecordRoutes)
app.use(personalRecordRoutes)
app.use(departmentRoutes)
app.use(userController.toRouter())
app.use(rosterRoutes)
app.use(adminRosterRoutes)
app.use(leaveRoutes)

// app.use(express.static(path.join(__dirname, 'build')))

app.get('/*', function (req, res) {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

const PORT = 8080;
app.listen(PORT, () => {
  console.log(`Listening at http://localhost:${PORT}/`);
  if (process.env.CI) {
    process.exit(0);
  }
});
