import { Knex } from "knex";
const tableNameCompany = "company_list";
const tableNameDepartment = "department_list";
const tableNameUser = "users";


export async function up(knex: Knex): Promise<void> {

    await knex.schema.createTable(tableNameCompany, (table) => {
        table.increments();
        table.string("company_name").unique().notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable(tableNameDepartment, (table) => {
        table.increments();
        table.string("department_name").unique().notNullable();
        table.timestamps(false, true);
    })

    await knex.schema.createTable(tableNameUser, (table) => {
        table.increments();
        table.string("staff_id").unique().notNullable();
        table.string("password").notNullable();
        table.boolean("admin").notNullable();
        table.integer("company_id").notNullable();
        table.foreign("company_id").references(`${tableNameCompany}.id`);
        table.integer("department_id").notNullable();
        table.foreign("department_id").references(`${tableNameDepartment}.id`);
        table.timestamps(false, true);
      });
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tableNameCompany);
    await knex.schema.dropTable(tableNameDepartment);
    await knex.schema.dropTable(tableNameUser);
}

