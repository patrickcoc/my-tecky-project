import { Knex } from "knex";
const tableNamePersonalData = "personal_data";
const tableNameUser = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tableNamePersonalData, (table) => {
        table.increments();
        table.integer("users_id");
        table.foreign("users_id").references(`${tableNameUser}.id`);
        table.string("name").notNullable();
        table.string("IDcard").notNullable();
        table.string("address").notNullable();
        table.integer("phone").notNullable();
        table.string("email").notNullable();
        table.date("date_of_birth").notNullable();
        table.date("entry_date").notNullable();
        table.string("post");
        table.integer("salary");
        table.integer("leave_day");
        table.string("staff_id").unique;
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tableNamePersonalData);
}

