import { Knex } from "knex";
const tableNameAttendanceRecord = "attendance_record";
const tableNameUser = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tableNameAttendanceRecord, (table) => {
        table.increments();
        table.integer("users_id").notNullable();
        table.foreign("users_id").references(`${tableNameUser}.id`);
        table.string("attendance_date").notNullable();
        table.string("time").notNullable();
        table.string("in_out").notNullable();
        table.string('update_by')
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tableNameAttendanceRecord);
}
