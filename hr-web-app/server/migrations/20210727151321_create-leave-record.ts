import { Knex } from "knex";
const tableNameLeaveRecord = "leave_record";
const tableNameUser = "users";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tableNameLeaveRecord, (table) => {
        table.increments();
        table.integer("users_id").notNullable();
        table.foreign("users_id").references(`${tableNameUser}.id`);
        table.date("leave_date").notNullable();
        table.enum("is_all_day", ["1", "0.5"]).notNullable();
        table.boolean("pay_leave").notNullable();
        table.string("leave_type").notNullable();
        table.string("remark");
        table.enum("application_status", ["pending", "approved", "declined"]).notNullable().defaultTo("pending");
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tableNameLeaveRecord);
}

