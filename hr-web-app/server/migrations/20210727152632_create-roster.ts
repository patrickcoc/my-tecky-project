import { Knex } from "knex";
const tableNameRoster = "roster";
const tableNameUser = "users";
const tableNameSalary="salary_remark";

export async function up(knex: Knex): Promise<void> {
    await knex.schema.createTable(tableNameRoster, (table) => {
        table.increments();
        table.integer("users_id").notNullable();
        table.foreign("users_id").references(`${tableNameUser}.id`);
        table.timestamp("on_duty_date").notNullable();
        table.string("shift_type").notNullable();
        table.integer("year").notNullable();
        table.integer("month").notNullable();
        table.integer("day").notNullable();
        table.integer("version").notNullable()
        table.string('update_by')
        table.timestamps(false, true);
    })
    await knex.schema.createTable(tableNameSalary, (table) => {
        table.increments();
        table.integer("users_id").notNullable();
        table.foreign("users_id").references(`${tableNameUser}.id`);
        table.string('remark')
        table.boolean('checkbox')
        table.timestamps(false, true);
    })
}


export async function down(knex: Knex): Promise<void> {
    await knex.schema.dropTable(tableNameRoster);
    await knex.schema.dropTable(tableNameSalary);
}

