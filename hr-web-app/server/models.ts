export interface User {
    id: number
    company_id: number
    department_id: number
    staff_id: string
    password: string
    admin: boolean
}

export type JwtPayload = {
    id: number,
    username: string,
    isAdmin: boolean
};

declare global {
    namespace Express {
        interface Request {
            jwtPayload: JwtPayload
        }
    }
}

export interface AccountLevel {
    id: number
    accountLevel: string
}

export interface company {
    id: number
    companyName: string
}

export interface Department {
    id: number | string
    department_name: string
}

export interface PersonnelRecords {
    id: number
    users_id: number
    name: string
    IDcard: string
    address: string
    date_of_birth: string
    post: string
    email: string
    phone: number
    // department:string
    entry_date: string
    salary: number
    leave_day: number
    username: string
}

export interface LeaveRecord {
    id?: number
    users_id: number
    leave_type: string
    leave_date: Date
    is_all_day: string
    pay_leave: boolean
    remark: string
    application_status: string
}

export interface AttendanceRecord {
    attendance_date: Date
    time: string
    in_out: string
    users_id: number
    update_by?: string
}

export interface Roster {
    id: number
    users_id: number
    on_duty_date: Date
    shift_type: string
    year: number
    month: number
    day: number
    version: number
    update_by: Date
    created_at: Date
    updated_at: Date
}

export interface PersonalMonthlyRoster {
    [staff_id: string]: {
        [isoDate: string]: string;
    };
}

export interface Remark {
    users_id: number | string,
    remark: string,
    checkbox: boolean
}