import express from 'express'
import { isAdmin } from '../guards'
import { adminRosterController } from '../main'

export const adminRosterRoutes = express.Router()

adminRosterRoutes.get('/departmentList/:userId',isAdmin,adminRosterController.getDepartmentList)

adminRosterRoutes.get('/departmentStaffList/:departmentId',isAdmin,adminRosterController.getDepartmentStaffList)

adminRosterRoutes.post('/departmentStaffRoster',adminRosterController.getDepartmentStaffRoster)