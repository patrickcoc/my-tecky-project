import express from 'express'
import { isAdmin } from '../guards'
import { attendanceRecordController } from "../main"

export const attendanceRecordRoutes = express.Router()

// attendanceRecordRoutes.get('/get_department',departmentController.GetDepartmentList)
// for admin
attendanceRecordRoutes.post('/Qr_check',isAdmin,attendanceRecordController.postAttendanceRecord)
attendanceRecordRoutes.post('/get_attendance',isAdmin,attendanceRecordController.GetAttendanceRecord)
attendanceRecordRoutes.post('/del_attendance',isAdmin,attendanceRecordController.delAttendanceRecord)
attendanceRecordRoutes.post('/add_attendance',isAdmin,attendanceRecordController.addAttendanceRecord)
attendanceRecordRoutes.post('/get_month_attendance',isAdmin,attendanceRecordController.GetMonthAttendanceRecord)


//for users
attendanceRecordRoutes.post('/get_user_attendance',attendanceRecordController.GetUserAttendanceRecord)
attendanceRecordRoutes.post('/get_user_month_attendance',attendanceRecordController.GetMonthUserAttendanceRecord)
