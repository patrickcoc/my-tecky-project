import express from 'express'
import { isAdmin } from '../guards'
import { departmentController } from "../main"

export const departmentRoutes = express.Router()

departmentRoutes.get('/get_department',departmentController.GetDepartmentList)
departmentRoutes.post('/add_department',isAdmin,departmentController.addNewDepartment)