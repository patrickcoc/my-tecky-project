import express from 'express'
import { isLoggedIn, isAdmin } from '../guards'
import { leaveRecordController } from "../main"
export const leaveRoutes = express.Router()

leaveRoutes.get('/get_leave', isLoggedIn, leaveRecordController.getLeaveRecord)

leaveRoutes.post('/apply_leave', isLoggedIn, leaveRecordController.applyLeave)

leaveRoutes.get('/leave_detail/:id', isLoggedIn, leaveRecordController.getLeaveDetail)

leaveRoutes.get('/get_pending', isLoggedIn, leaveRecordController.getPendingLeave)

leaveRoutes.get('/get_allleave', isLoggedIn, leaveRecordController.getAllLeaveRecord)

leaveRoutes.post('/approve_leave', isLoggedIn, leaveRecordController.approveLeave)

leaveRoutes.post('/decline_leave', isLoggedIn, leaveRecordController.declineLeave)

leaveRoutes.post('/is_pay_leave', isLoggedIn, leaveRecordController.isPayLeave)

leaveRoutes.post('/departmentStaffMonthlyLeaveApplication', isAdmin, leaveRecordController.getCurrentMonthDepartmentStaffLeaveApplicationRecord)