import express from 'express'
import { rosterController } from '../main'

export const rosterRoutes = express.Router()

rosterRoutes.post('/dailySchedule',rosterController.dailySchedule)

rosterRoutes.get('/currentMonthSchedule/:userId',rosterController.currentMonthSchedule)

rosterRoutes.get('/nextMonthSchedule/:userId',rosterController.nexttMonthSchedule)

rosterRoutes.post('/monthSchedule',rosterController.createMonthSchedule)