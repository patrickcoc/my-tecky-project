import express from 'express'
import { isAdmin } from '../guards'
import { salaryController } from '../main'

export const SalaryRoutes = express.Router()

SalaryRoutes.get('/departmentList/:userId',isAdmin,salaryController.getDepartmentList)

SalaryRoutes.get('/departmentStaffList_getsalary/:departmentId',isAdmin,salaryController.getDepartmentStaffList)

SalaryRoutes.post('/calculator_salary',isAdmin,salaryController.getStaffData)

SalaryRoutes.post('/add_salary_remark',isAdmin,salaryController.addRemark)