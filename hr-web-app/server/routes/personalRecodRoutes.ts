import express from 'express'
import { isAdmin, isLoggedIn } from '../guards'
import { personalRecordController } from "../main"

export const personalRecordRoutes = express.Router()

personalRecordRoutes.post('/amdin_P_form',isAdmin,personalRecordController.CreatePersonalRecord)
personalRecordRoutes.get('/get_count',isAdmin,personalRecordController.getCount)
personalRecordRoutes.put('/update_data',personalRecordController.UpdatePersonalRecord)
personalRecordRoutes.get('/userProfile/:userId',isLoggedIn, personalRecordController.getUserProfile)