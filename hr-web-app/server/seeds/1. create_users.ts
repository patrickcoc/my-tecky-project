import { Knex } from "knex";
import { hashPassword } from "../hash";

export async function seed(knex: Knex): Promise<void> {
    try {
        await knex("salary_remark").del();
        await knex("leave_record").del();
        await knex("roster").del();
        await knex("attendance_record").del();
        await knex("personal_data").delete();
        await knex("users").delete();
        await knex("company_list").delete();
        await knex("department_list").delete();




        await knex("company_list").insert([
            { company_name: "TECKY" },
        ]); ``

        await knex("department_list").insert([
            { department_name: "Human Resources Department" },
            { department_name: "IT Management" },
            { department_name: "Application Department" }
        ]);


        let companyID = await knex("company_list")
            .select('*')
            .from('company_list')
            .where('company_name', '=', 'TECKY')
            .first()
        let hrDepartmentId = await knex("department_list")
            .select('*')
            .from('department_list')
            .where('department_name', '=', 'Human Resources Department')
            .first()
        let itDepartmentId = await knex("department_list")
            .select('*')
            .from('department_list')
            .where('department_name', '=', 'IT Management')
            .first()
        let appsDepartmentId = await knex("department_list")
            .select('*')
            .from('department_list')
            .where('department_name', '=', 'Application Department')
            .first()



        const hashedPassword = await hashPassword('62717171')

        await knex("users").insert([
            { staff_id: "admin", password: hashedPassword, admin: true, company_id: `${companyID.id}`, department_id: `${hrDepartmentId.id}` },
            { staff_id: "0000100", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000111", password: hashedPassword, admin: true, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000112", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000113", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000114", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000115", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000116", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000117", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000118", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000119", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000120", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${itDepartmentId.id}` },
            { staff_id: "0000121", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000122", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000123", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000124", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000125", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000126", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000127", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000128", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000129", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000130", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${appsDepartmentId.id}` },
            { staff_id: "0000131", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${hrDepartmentId.id}` },
            { staff_id: "0000132", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${hrDepartmentId.id}` },
            { staff_id: "0000133", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${hrDepartmentId.id}` },
            { staff_id: "0000134", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${hrDepartmentId.id}` },
            { staff_id: "0000135", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${hrDepartmentId.id}` },
            { staff_id: "0000136", password: hashedPassword, admin: false, company_id: `${companyID.id}`, department_id: `${hrDepartmentId.id}` },
        ]);


    }
    catch (err) {
        console.error(err);

    }
};
