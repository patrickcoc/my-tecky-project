import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
    try {


        let userID = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', 'admin')
            .first()

        let employeeID100 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000100')
            .first()
        let employeeID111 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000111')
            .first()
        let employeeID112 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000112')
            .first()
        let employeeID113 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000113')
            .first()
        let employeeID114 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000114')
            .first()
        let employeeID115 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000115')
            .first()
        let employeeID116 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000116')
            .first()
        let employeeID117 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000117')
            .first()
        let employeeID118 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000118')
            .first()
        let employeeID119 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000119')
            .first()
        let employeeID120 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000120')
            .first()
        let employeeID121 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000121')
            .first()
        let employeeID122 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000122')
            .first()
        let employeeID123 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000123')
            .first()
        let employeeID124 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000124')
            .first()
        let employeeID125 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000125')
            .first()
        let employeeID126 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000126')
            .first()
        let employeeID127 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000127')
            .first()
        let employeeID128 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000128')
            .first()
        let employeeID129 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000129')
            .first()
        let employeeID130 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000130')
            .first()
        let employeeID131 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000131')
            .first()
        let employeeID132 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000132')
            .first()
        let employeeID133 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000133')
            .first()
        let employeeID134 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000134')
            .first()
        let employeeID135 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000135')
            .first()
        let employeeID136 = await knex("users")
            .select('*')
            .from('users')
            .where('staff_id', '=', '0000136')
            .first()



        await knex("personal_data").insert([
            { users_id: `${userID.id}`, name: "admin", IDcard: "R1234567", address: "HK", phone: "23800000", email: "123@gg.mail", date_of_birth: "2000/01/01", entry_date: "2000/1/1", post: "admin", salary: "30000", leave_day: "368", staff_id: "admin" },
            { users_id: `${employeeID100.id}`, name: "Leo Lui", IDcard: "R2345678", address: "", phone: "66667777", email: "5555@gg.mail", date_of_birth: "2001/10/10", entry_date: "2000/1/1", post: "Architect", salary: "1000000", leave_day: "12", staff_id: "0000100" },
            { users_id: `${employeeID111.id}`, name: "Patrick Au", IDcard: "G0117241", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "51512121", email: "fun@gg.mail", date_of_birth: "1997/07/01", entry_date: "2000/2/2", post: "Architect", salary: "300000", leave_day: "12", staff_id: "0000111" },
            { users_id: `${employeeID112.id}`, name: "Edwin Ho", IDcard: "I3225679", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Manager", salary: "18000", leave_day: "12", staff_id: "0000112" },
            { users_id: `${employeeID113.id}`, name: "Harry Lam", IDcard: "R7777777", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "51512121", email: "fun@gg.mail", date_of_birth: "1997/07/01", entry_date: "2000/2/2", post: "Manager", salary: "300000", leave_day: "12", staff_id: "0000113" },
            { users_id: `${employeeID114.id}`, name: "Terry Chan", IDcard: "L5136108", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000114" },
            { users_id: `${employeeID115.id}`, name: "Bob Tang", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000115" },
            { users_id: `${employeeID116.id}`, name: "Javis Lui", IDcard: "E4825547", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000116" },
            { users_id: `${employeeID117.id}`, name: "Raymond Li", IDcard: "X6689243", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000117" },
            { users_id: `${employeeID118.id}`, name: "Jason Ho", IDcard: "R7348826", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000118" },
            { users_id: `${employeeID119.id}`, name: "Issac Wong", IDcard: "R7348826", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000119" },
            { users_id: `${employeeID120.id}`, name: "Tommy Sze", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000120" },
            { users_id: `${employeeID121.id}`, name: "Fat Kwan", IDcard: "K1232452", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000121" },
            { users_id: `${employeeID122.id}`, name: "Cherry Au", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000122" },
            { users_id: `${employeeID123.id}`, name: "Me Chan", IDcard: "K1232452", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000123" },
            { users_id: `${employeeID124.id}`, name: "Lily Chan", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000124" },
            { users_id: `${employeeID125.id}`, name: "Stephaine Li", IDcard: "K1232452", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000125" },
            { users_id: `${employeeID126.id}`, name: "Adele Ho", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000126" },
            { users_id: `${employeeID127.id}`, name: "Chris Ho", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000127" },
            { users_id: `${employeeID128.id}`, name: "Micheal Lee", IDcard: "K1232452", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000128" },
            { users_id: `${employeeID129.id}`, name: "Kim Lau", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000129" },
            { users_id: `${employeeID130.id}`, name: "Grace Lau", IDcard: "K1232452", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Developer", salary: "28000", leave_day: "12", staff_id: "0000130" },
            { users_id: `${employeeID131.id}`, name: "Alice Chan", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Manager", salary: "28000", leave_day: "12", staff_id: "0000131" },
            { users_id: `${employeeID132.id}`, name: "Emily Ng", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Officer", salary: "28000", leave_day: "12", staff_id: "0000132" },
            { users_id: `${employeeID133.id}`, name: "Angus Chan", IDcard: "K1232452", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Officer", salary: "28000", leave_day: "12", staff_id: "0000133" },
            { users_id: `${employeeID134.id}`, name: "John Li", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Officer", salary: "28000", leave_day: "12", staff_id: "0000134" },
            { users_id: `${employeeID135.id}`, name: "Timmy Tam", IDcard: "K1232452", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Officer", salary: "28000", leave_day: "12", staff_id: "0000135" },
            { users_id: `${employeeID136.id}`, name: "Alex Chiu", IDcard: "R3242312", address: "Rm 515-16, 5/F, One Midtown, 11 Hoi Shing Road, Tsuen Wan, Hong Kong", phone: "17772222", email: "sosad@gg.mail", date_of_birth: "1990/05/03", entry_date: "1999/2/2", post: "Assistant", salary: "28000", leave_day: "12", staff_id: "0000136" },

        ]);
    }
    catch (err) {
        console.error(err);

    }
};
