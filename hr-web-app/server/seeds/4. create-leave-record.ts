import { Knex } from "knex";

export async function seed(knex: Knex): Promise<void> {
        let adminID = await knex("users")
            .select("id")
            .from("users")
            .where('staff_id', '=', 'admin')
            .first()
        let employeeID100 = await knex("users")
            .select("id")
            .from("users")
            .where('staff_id', '=', '0000100')
            .first()
        let patrickID = await knex("users")
            .select("id")
            .from("users")
            .where('staff_id', '=', '0000111')
            .first()
        let employeeID112 = await knex("users")
            .select("id")
            .from("users")
            .where('staff_id', '=', '0000112')
            .first()
        let employeeID113 = await knex("users")
            .select("id")
            .from("users")
            .where('staff_id', '=', '0000113')
            .first()
        let employeeID114 = await knex("users")
            .select("id")
            .from("users")
            .where('staff_id', '=', '0000114')
            .first()
        let employeeID115 = await knex("users")
            .select("id")
            .from("users")
            .where('staff_id', '=', '0000115')
            .first()

        await knex("leave_record").insert([
            {
                users_id: `${adminID.id}`,
                leave_date: "2021/10/01",
                is_all_day: "1",
                pay_leave: true,
                leave_type: "sick leave",
                remark: "not feeling well",
                application_status: "pending"
            },
            {
                users_id: `${adminID.id}`,
                leave_date: "2021/8/27",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "casual leave",
                remark: "Casual leave due to the COVID-19.",
                application_status: "pending"
            },
            {
                users_id: `${adminID.id}`,
                leave_date: "2021/9/05",
                is_all_day: "0.5",
                pay_leave: false,
                leave_type: "others",
                remark: "Its my boss birthday!!",
                application_status: "approved"
            },
            {
                users_id: `${adminID.id}`,
                leave_date: "2021/9/09",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "sick leave",
                remark: "I am so sick sorry",
                application_status: "declined"
            },
            {
                users_id: `${adminID.id}`,
                leave_date: "2021/9/25",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "casual leave",
                remark: "Sep casual leave",
                application_status: "pending"
            },
            {
                users_id: `${employeeID100.id}`,
                leave_date: "2021/9/28",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "casual leave",
                remark: "Sep casual leave",
                application_status: "pending"
            },
            {
                users_id: `${patrickID.id}`,
                leave_date: "2021/10/1",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "others",
                remark: "I love 1/10 chiiina",
                application_status: "pending"
            },
            {
                users_id: `${patrickID.id}`,
                leave_date: "2021/11/11",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "sick leave",
                remark: "headache",
                application_status: "pending"
            },
            {
                users_id: `${patrickID.id}`,
                leave_date: "2021/12/22",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "annual leave",
                remark: "travelling",
                application_status: "approved"
            },
            {
                users_id: `${patrickID.id}`,
                leave_date: "2021/12/23",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "annual leave",
                remark: "travelling",
                application_status: "approved"
            },
            {
                users_id: `${employeeID100.id}`,
                leave_date: "2021/10/16",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "annual leave",
                remark: "Standard Annual Leave",
                application_status: "pending"
            },
            {
                users_id: `${employeeID112.id}`,
                leave_date: "2021/5/6",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "annual leave",
                remark: "Standard Annual Leave",
                application_status: "approved"
            },
            {
                users_id: `${employeeID113.id}`,
                leave_date: "2021/3/27",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "annual leave",
                remark: "Standard Annual Leave",
                application_status: "declined"
            },
            {
                users_id: `${employeeID114.id}`,
                leave_date: "2021/6/10",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "annual leave",
                remark: "Standard Annual Leave",
                application_status: "declined"
            },
            {
                users_id: `${employeeID115.id}`,
                leave_date: "2021/7/7",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "annual leave",
                remark: "Standard Annual Leave",
                application_status: "approved"
            },
            {
                users_id: `${adminID.id}`,
                leave_date: "2021/8/27",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "non-pay leave",
                remark: "tecky showcase please come",
                application_status: "approved"
            },
            {
                users_id: `${patrickID.id}`,
                leave_date: "2021/8/27",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "non-pay leave",
                remark: "tecky showcase please come",
                application_status: "approved"
            },
            {
                users_id: `${employeeID100.id}`,
                leave_date: "2021/8/27",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "non-pay leave",
                remark: "tecky showcase please come",
                application_status: "approved"
            },
            {
                users_id: `${employeeID112.id}`,
                leave_date: "2021/8/27",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "non-pay leave",
                remark: "tecky showcase please come",
                application_status: "approved"
            },
            {
                users_id: `${employeeID113.id}`,
                leave_date: "2021/8/27",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "non-pay leave",
                remark: "tecky showcase please come",
                application_status: "approved"
            },
            {
                users_id: `${employeeID114.id}`,
                leave_date: "2021/8/27",
                is_all_day: "1",
                pay_leave: false,
                leave_type: "non-pay leave",
                remark: "tecky showcase please come",
                application_status: "approved"
            },
            
        ])
        await knex("salary_remark").insert([
            {
            users_id:`${adminID.id}`,
            remark:"pay $10000 in 2021/7/30",
            checkbox:true
            }
        ])
}