import { Knex } from "knex";
import { Department } from "../models"
import { User } from "../models";
import tables from "../tables";

export class AdminRosterService {
    constructor(private knex: Knex) { }

    async getDepartmentList(userId: number) {
        const departmentName = await this.knex<Department>(tables.Department)
            .join('users', 'department_list.id', '=', 'users.department_id')
            .select('department_name', 'department_list.id')
            .distinct()
        return departmentName
    }

    async checkIsAdmin(userId: number) {
        const { admin } = await this.knex(tables.Users)
            .select('admin')
            .where('id', userId)
            .first()
        return admin
    }

    async getDepartmentStaffList(departmentId: number) {
        const departmentStaffList = await this.knex<User>(tables.Users)
            .select('users.staff_id', 'personal_data.name', 'personal_data.post')
            .join('department_list', 'department_list.id', '=', 'users.department_id')
            .join('personal_data', 'personal_data.users_id', '=', 'users.id')
            .where('department_id', departmentId)
        return departmentStaffList
    }

    async getDepartmentStaffRoster(department_id: number, start: string, end: string) {
        console.log('getDepartmentStaffRoster', { department_id, start, end })
        const departmentStaffRoster = await this.knex.transaction(async trx => {
            const staffList = await trx("users")
                .select('id', 'staff_id')
                .where('department_id', department_id)

            const data = {}



            for (const staff of staffList) {
                const staffId = staff.staff_id

                const userId = staff.id


                // const staffRosterList = await trx.raw(
                //     `select on_duty_date, shift_type from roster where users_id = ${userId} and month = ${month} and year = ${year} and version = (select MAX(roster.version) from roster where users_id = ${userId} and month = ${month} and year = ${year})`
                // )

                const dayList = await trx.raw(
                    `select 
                        on_duty_date, Max(version) as version
                    from roster
                    where users_id = ${userId}
                      and on_duty_date >= '${start}' 
                      and on_duty_date <= '${end}'
                      and version = (select MAX(a.version) from roster a where a.users_id = roster.users_id and a.on_duty_date = roster.on_duty_date) 
                    group by on_duty_date
                      `
                )




                data[staffId] = {}
                for (let row of dayList.rows) {
                    let date = new Date(row.on_duty_date).toISOString()
                    const result = await trx.raw(
                        `select 
                            on_duty_date, shift_type
                        from roster
                        where users_id = ${userId}
                          and on_duty_date = '${date}'
                          and version = ${row.version} 
                          `
                    )
                    const r = result.rows[0]
                    if (!r) {
                        continue;
                    }
                    data[staffId][date] = r.shift_type
                }


                // for (let row of result.rows) {

                //     let date = new Date(row.on_duty_date);
                //     let isoDate = date.toISOString()
                //     data[staffId][isoDate] = row.shift_type

                // }
                // const dayList = await trx.raw(
                //     `select day,MAX(roster.version) as version from roster where users_id = ${userId} and on_duty_date >= ${start} and on_duty_date <= ${end} group by day`
                // )


            //     for (let row of dayList.rows) {
            //         const result = await trx.raw(
            //             `select on_duty_date, shift_type from roster where users_id = ${userId} and on_duty_date >= ${start} and on_duty_date <= ${end} and version = ${row.version} and day = ${row.day}`
            //         )
            //         const r = result.rows[0]
            //         if (!r) {
            //             continue;
            //         }
            //         let date = new Date(r.on_duty_date);
            //         date.setHours(0, 0, 0, 0);
            //         let isoDate = new Date(r.on_duty_date).toISOString()
            //         data[staffId][isoDate] = r.shift_type
            //     }
            }

            return data

        })
        return departmentStaffRoster
    }
}