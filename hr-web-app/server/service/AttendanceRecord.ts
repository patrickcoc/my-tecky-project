import { Knex } from "knex";
import { AttendanceRecord, User } from "../models"
import tables from "../tables"
type data = {
    state: {
        date: Date
        time: string
        user: string
        type: string
        user_id: string
    }
}
export class AttendanceRecordService {
    constructor(private knex: Knex) { }

    async getAttendanceRecord(date: string) {
        const attendanceRecord =
            await this.knex<AttendanceRecord>(tables.AttendanceRecord)
                .join('users', 'users.id', 'attendance_record.users_id')
                .join('department_list', 'department_list.id', 'users.department_id')
                .select('*', 'attendance_record.id')
                .where('attendance_date', date)
        return attendanceRecord
    }

    async getUserAttendanceRecord(date: string, id: string) {
        const userAttendanceRecord =
            await this.knex<AttendanceRecord>(tables.AttendanceRecord)
                .join('users', 'users.id', 'attendance_record.users_id')
                .where('users_id', id)
                .where('attendance_date', date)
        return userAttendanceRecord
    }


    async getUserMonthAttendanceRecord(date: string, id: string) {
        const userMonthAttendanceRecord =
            await this.knex<AttendanceRecord>(tables.AttendanceRecord)
                .join('users', 'users.id', 'attendance_record.users_id')
                .where('users_id', id)
                .where('attendance_date', 'like', `${date}%`)
        return userMonthAttendanceRecord
    }


    async getMonthAttendanceRecord(date: string) {
        const userMonthAttendanceRecord =
            await this.knex<AttendanceRecord>(tables.AttendanceRecord)

                .join('users', 'users.id', 'attendance_record.users_id')
                .where('attendance_date', 'like', `${date}%`)

        return userMonthAttendanceRecord

    }
    async checkAttendance(data: data) {

        const newID = parseInt(data.state.user_id)

        const check =
            await this.knex<AttendanceRecord>(tables.AttendanceRecord)

                .where('users_id', newID)
                .where('attendance_date', data.state.date)
                .where('in_out', data.state.type)

        return check.length

    }
    async postAttendanceRecord(data: data) {

        const newID = parseInt(data.state.user_id)

        const newAttendanceRecord =
            await this.knex<AttendanceRecord>(tables.AttendanceRecord)
            
                .insert({
                    users_id: newID,
                    attendance_date: data.state.date,
                    time: data.state.time,
                    in_out: data.state.type
                })

        return newAttendanceRecord

    }
    async deleteAttendanceRecord(data: any) {
        const deleteAttendanceRecord =
            await this.knex<AttendanceRecord>(tables.AttendanceRecord)
                .where("users_id", data.users_id)
                .where('in_out', data.in_out)
                .where("attendance_date", data.attendance_date)
                .where('time', data.time)
                .delete()
        return deleteAttendanceRecord
    }

    async getUsers_id(data: any) {
        const usersID =
            await this.knex<User>(tables.Users)
                .where('staff_id', data.staff_id)
                .first()

        return usersID
    }
    async addAttendanceRecord(data: any, users_id: any) {
        const addAttendanceRecord =
            await this.knex<AttendanceRecord>(tables.AttendanceRecord)
                .insert({
                    users_id: users_id.id,
                    attendance_date: data.attendance_date,
                    time: data.time,
                    in_out: data.in_out,
                    update_by: data.update_by
                })
        return addAttendanceRecord
    }

}

