import { Knex } from "knex";
import { hashPassword } from "../hash";
import { PersonnelRecords, Department, User, company } from "../models"
import tables from "../tables"

// export type csvData={
//     Name: string,
//     IDcard: string,
//     Address: string,
//     Phone_Number: string,
//     Birth: string,
//     Email: string,
//     Department: string,
//     Post: string,
//     Salary: string,
//     Annual: string,
//     Entry: string
// }

export class CsvService {

    constructor(private knex: Knex) { }

    async getDepartmentID(data: any) {
        const getDepartmentID =
            await this.knex<Department>(tables.Department)
                .where('department_name', data.Department)
                .first()
        return getDepartmentID
    }


    async getCompanyID(data: any) {
        const getCompanyID =
            await this.knex<company>(tables.Company)
                .where('company_name', data.Company)
                .first()
        return getCompanyID
    }
    
    async getcount() {
        const numberOfCount =
            await this.knex<PersonnelRecords>(tables.PersonalData)
                .max('id')
        return numberOfCount[0].max
    }

    async CsvCreateUser(data: any, id: any,staff_id:any,company_id:any) {
        const number_id = parseInt(id)
        const password = data.IDcard.slice(1)
        const hashedPassword = await hashPassword(`${password}`)
        const date=new Date
        const yaer=date.getFullYear()
        const staffID =`${yaer}` + (company_id.toString()) + (id.toString()) +(staff_id.toString())
        const user_ac =
            await this.knex<User>(tables.Users)
                .insert({
                    staff_id: staffID,
                    password: hashedPassword,
                    company_id: company_id,
                    department_id: number_id,
                    admin: false
                })
                .returning('*')
               
        return user_ac
    }
    async csvInsert(data: any,user_ac:any) {

        const csvDataList =
            await this.knex<PersonnelRecords>(tables.PersonalData)
            .insert({
                name: data.Name,
                phone: data.Phone_Number,
                IDcard: data.IDcard,
                date_of_birth:data.Birth,
                address: data.Address,
                email: data.Email,
                post: data.Post,
                salary: data.Salary,
                leave_day: data.Annual,
                entry_date:data.Entry,
                users_id:user_ac[0].id
            })
        return csvDataList
    }

}