import { Knex } from "knex";
import { Department } from "../models"
import tables from "../tables"

export class GetDepartmentService {
    constructor(private knex: Knex) { }

    async GetDepartment() {
        const departmentList =
            await this.knex<Department>(tables.Department)
                .select('*')
                .from(`${tables.Department}`)
        return departmentList
    }


    async addNewDepartment(name: string) {
        const addNew =
            await this.knex<Department>(tables.Department)
                .insert({department_name: name})
        return addNew
    }
}