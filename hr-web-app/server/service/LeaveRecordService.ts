import { Knex } from "knex";
import { LeaveRecord } from "../models";

export class LeaveRecordService {
    constructor(private knex: Knex) { }

    async applyLeave(data: LeaveRecord) {
        const checkExistingLeave = await this.knex<LeaveRecord>("leave_record").select("*").where("leave_date", "=", data.leave_date).first()
        if (checkExistingLeave?.id === undefined) {
       
                await this.knex<LeaveRecord>("leave_record")
                    .insert({
                        users_id: data.users_id,
                        leave_type: data.leave_type,
                        leave_date: data.leave_date,
                        is_all_day: data.is_all_day,
                        pay_leave: false,
                        remark: data.remark,
                        application_status: "pending"
                    })
                    .returning("leave_date")
          
        } else {
          
        }
    }

    async getLeaveDetail(leaveId: number) {
        const leaveDetail =
            await this.knex<{
                staff_id: string,
                name: string,
                leave_date: string,
                leave_type: string,
                is_all_day: string,
                remark: string,
                application_status: string
            }[]>("users")
                .select(
                    "leave_record.users_id",
                    "personal_data.name",
                    "leave_record.leave_date",
                    "leave_record.leave_type",
                    "leave_record.is_all_day",
                    "leave_record.remark",
                    "leave_record.application_status"
                )
                .join("personal_data", "personal_data.users_id", "=", "users.id")
                .join("leave_record", "leave_record.users_id", "=", "personal_data.users_id")
                
                .where("leave_record.id", "=", leaveId).first()
                .orderBy("leave_record.leave_date", "desc")
        return leaveDetail
    }

    async getLeaveResult(id: number) {
        const leaveResult =
            await this.knex<LeaveRecord>("leave_record")
                .select("id", "leave_date", "leave_type", "application_status")
                .where("users_id", "=", id)
                .orderBy("leave_record", "desc")
        return leaveResult
    }

    async getAllPendingLeave() {
        const pendingLeave =
            await this.knex<{
                id: number,
                staff_id: string,
                name: string,
                leave_date: string,
                leave_type: string,
                is_all_day: string,
                application_status: string
            }[]>("users")
                .select(
                    "leave_record.id",
                    "users.staff_id",
                    "personal_data.name",
                    "leave_record.leave_date",
                    "leave_record.leave_type",
                    "is_all_day",
                    "application_status"
                )
                .join("leave_record", "leave_record.users_id", "=", "users.id")
                .join("personal_data", "personal_data.users_id", "=", "users.id")
                .where("application_status", "=", "pending")
                .orderBy("leave_record", "desc")
        return pendingLeave
    }

    async getAllLeaveResult() {
        const allLeaveResult =
            await this.knex<{
                id: number,
                staff_id: string,
                name: string,
                leave_date: string,
                leave_type: string,
                is_all_day: string,
                application_status: string
            }[]>("users")
                .select(
                    "leave_record.id",
                    "users.staff_id",
                    "personal_data.name",
                    "leave_record.leave_date",
                    "leave_record.leave_type",
                    "is_all_day",
                    "application_status"
                )
                .join("leave_record", "leave_record.users_id", "=", "users.id")
                .join("personal_data", "personal_data.users_id", "=", "users.id")
                .orderBy("leave_record", "desc")
        return allLeaveResult
    }

    async approveLeave(id: number) {
        await this.knex<LeaveRecord>("leave_record")
            .update(
                {
                    application_status: "approved"
                }
            )
            .where({ id })
    }

    async declineLeave(id: number) {
        await this.knex<LeaveRecord>("leave_record")
            .update(
                {
                    application_status: "declined"
                }
            )
            .where({ id })
    }

    async isPayLeave(id: number, pay_leave: boolean) {
        await this.knex<LeaveRecord>("leave_record")
            .update(
                { pay_leave }
            )
            .where({ id })
    }

    async getCurrentMonthDepartmentStaffLeaveApplicationRecord(departmentId: number, startDay: string, endDay: string) {
   
        const currentMonthDepartmentStaffLeaveApplicationRecord =
            await this.knex("leave_record")
                .select('leave_date', 'leave_type', 'personal_data.staff_id', 'name', 'application_status')
                .from('leave_record')
                .fullOuterJoin('users', 'leave_record.users_id', 'users.id')
                .innerJoin('personal_data', 'leave_record.users_id', 'personal_data.users_id')
                .where('department_id', departmentId)
                .where('leave_date', '>=', startDay)
                .where('leave_date', '<', endDay)
        return currentMonthDepartmentStaffLeaveApplicationRecord
    }
}