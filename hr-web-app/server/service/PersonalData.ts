import { Knex } from "knex";
import { PersonnelRecords, User } from "../models"
import tables from "../tables"

export class PersonalRecordService {
    constructor(private knex: Knex) { }

    async PersonalRecord(userId: number) {
        const personalRecord =
            await this.knex<PersonnelRecords>(tables.PersonalData)
                .where("userId", userId)
        return personalRecord
    }


    async CreatePersonalRecord(data: any) {

        const newPersonalRecord =
            await this.knex<PersonnelRecords>(tables.PersonalData)
                .insert({
                    name: data.name,
                    phone: data.phone,
                    IDcard: data.IDcard,
                    date_of_birth:data.date_of_birth,
                    address: data.address,
                    email: data.email,
                    post: data.post,
                    salary: data.salary,
                    leave_day: data.leave_day,
                    entry_date:data.entry_date,
                    users_id:data.users_id
                })
                .returning("id")

        return newPersonalRecord
    }

    async getcount() {
        const numberOfCount =
            await this.knex<PersonnelRecords>(tables.PersonalData)
                .max('id')
        return numberOfCount[0].max+1
    }
    async UpdatePersonalRecord(data: any) {

        const updatelRecord =
            await this.knex<PersonnelRecords>(tables.PersonalData)
            .where('id',data.id)
            .update({
                name: data.name,
                phone: data.phone,
                IDcard: data.IDcard,
                date_of_birth:data.date_of_birth,
                address: data.address,
                email: data.email,
                post: data.post,
                salary: data.salary,
                leave_day: data.leave_day,
                entry_date:data.entry_date
            })
        return updatelRecord
    }

    async getUserProfile(userId:number){
        const userProfile = await this.knex<User>(tables.Users)
                            .join("company_list","users.company_id","=","company_list.id")
                            .join("department_list","users.department_id","department_list.id")
                            .join("personal_data","users.id","personal_data.users_id")
                            .select('company_name','department_name',"name","post","leave_day","email","users.staff_id")
                            .where('users.id',userId)
                            .first()
        return userProfile
    }

}