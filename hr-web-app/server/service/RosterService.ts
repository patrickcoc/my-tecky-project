import { Knex } from "knex";
import { Roster } from "../models";
import tables from '../tables'

export class RosterService {
    constructor(private knex: Knex) { }

    async getDailyDepartmentRoster(day: number, month: number, year: number, user_id: number) {
        const dailySchedule = this.knex.transaction(async trx => {
            const department_id =
                await trx("users")
                    .select('department_id')
                    .where('id', user_id)
                    .first()

            const departmentId = department_id['department_id']

            const version =
                await trx("roster")
                    .max('version')
                    .where({
                        "day": day,
                        "month": month,
                        "year": year,
                    })

            const maxVersion = version['max'] === undefined ? 1 : version[0]['max']

            return await trx("users")
                .join('roster', 'users.id', 'roster.users_id')
                .join('personal_data', 'roster.users_id', 'personal_data.users_id')
                .where({
                    "roster.day": day,
                    "roster.month": month,
                    "roster.year": year,
                    "users.department_id": departmentId,
                    "version": maxVersion
                })
                .select('personal_data.staff_id', 'name', 'post', 'shift_type')

        })
        return dailySchedule
    }

    async getPersonalMonthlySchedule(month: number, year: number, users_id: number) {
        const version =
            await this.knex<Roster>(tables.Roster)
                .max('version')
                .where({
                    "month": month,
                    "year": year
                })



        const maxVersion = version['max'] === undefined ? 1 : version[0]['max']

        const monthlyPersonalSchedule =
            await this.knex<Roster>(tables.Roster)
                .where({
                    "users_id": users_id,
                    'version': maxVersion,
                    'month': month,
                    'year': year
                })
                .select('year', 'month', 'day', 'shift_type')
                .orderBy('day', 'asc')
        return monthlyPersonalSchedule
    }

    async createNewMonthlySchedule(data: any) {
        for (const staffId in data) {
            const staffData = data[staffId]
            for (const date in staffData) {
                this.knex.transaction(async trx => {
                    const localdate = new Date(date)

                    const year = localdate.getFullYear()
                    const month = localdate.getMonth() + 1
                    const day = localdate.getDate()

                    const userId = await trx("personal_data")
                        .select("users_id")
                        .where("staff_id", staffId)
                        .first()
                    const versionRow = await trx("roster")
                        .where({
                            "on_duty_date": date,
                            "users_id": userId['users_id'],
                            "year": year,
                            "month": month,
                            "day": day,
                        })
                        .max("version")
                        .first()
                        .returning("version")
                    const lastVersion = versionRow?.['max'] || 0
                    const version = lastVersion + 1




                    return await trx.insert({
                        "users_id": userId['users_id'],
                        "on_duty_date": date,
                        "shift_type": staffData[date],
                        "year": year,
                        "month": month,
                        "day": day,
                        "version": version,
                    }).into("roster")
                })
            }
        }

    }
}