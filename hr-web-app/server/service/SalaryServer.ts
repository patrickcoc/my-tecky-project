import { Knex } from "knex";
import { Department } from "../models"
import { User, Remark } from "../models";
import tables from "../tables";

export class SalaryService {
    constructor(private knex: Knex) { }

    async getDepartmentList(userId: number) {
        const departmentName = await this.knex<Department>(tables.Department)
            .join('users', 'department_list.id', '=', 'users.department_id')
            .select('department_name', 'department_list.id')
            .distinct()
        return departmentName
    }

    async checkIsAdmin(userId: number) {
        const { admin } = await this.knex(tables.Users)
            .select('admin')
            .where('id', userId)
            .first()
        return admin
    }

    async getDepartmentStaffList(departmentId: number) {
        const departmentStaffList = await this.knex<User>(tables.Users)
            .select('users.staff_id', 'personal_data.name', 'personal_data.salary', 'salary_remark.remark', 'salary_remark.checkbox')
            .join('department_list', 'department_list.id', '=', 'users.department_id')
            .join('personal_data', 'personal_data.users_id', '=', 'users.id')
            .leftJoin('salary_remark', 'salary_remark.users_id', '=', 'users.id')
            .where('department_id', departmentId)

        return departmentStaffList
    }

    async getStaffData(staff_id: string, date: string) {
        const staffData = await this.knex<User>(tables.Users)
            .select('users.staff_id', 'personal_data.name', 'personal_data.salary', 'attendance_record.attendance_date', 'attendance_record.in_out', 'attendance_record.time')
            .join('department_list', 'department_list.id', '=', 'users.department_id')
            .join('attendance_record', 'attendance_record.users_id', '=', 'users.id')
            .join('personal_data', 'personal_data.users_id', '=', 'users.id')
            .where('attendance_record.attendance_date', 'like', `${date}%`)
            .where('users.staff_id', staff_id)

        return staffData
    }
    async getUserId(staff_id: any) {
        const userData =
            await this.knex<User>(tables.Users)
                .where('staff_id',staff_id)
                .first()
        return userData

    }

    async updateRemark(userId: number, remarkContent: string, check: boolean) {
        const update=await this.knex<Remark>(tables.Salary_remark)
            .where('users_id', userId)
            .update({
                remark: remarkContent,
                checkbox: check
            })
        return update

    }
    async addRemark(userId: number, remarkContent: string, check: boolean) {
        await this.knex<Remark>(tables.Salary_remark)
            .insert({
                users_id: userId,
                remark: remarkContent,
                checkbox: check
            })

    }
}