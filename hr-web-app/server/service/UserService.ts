import { Knex } from 'knex'
import { hashPassword } from '../hash'
import { PersonnelRecords, User } from "../models"
import tables from "../tables"


export class UserService {
    constructor(private knex: Knex) { }

    async login(staff_id: string) {
        const user_data =
            await this.knex<User>(tables.Users)
                .select('id', 'password', 'staff_id', 'company_id', 'admin', 'department_id')
                .where('staff_id', staff_id)
                .first()
        return user_data
    }

    async checkAdmin(staff_id: string) {
        const user_data =
            await this.knex<User>(tables.Users)
                .select('staff_id', 'company_id', 'admin', 'department_id')
                .where('staff_id', staff_id)
                .first()
        return user_data
    }
    async createUser(data: any, personal_data: any) {
        const password = parseInt(personal_data.IDcard.slice(1))
        const hashedPassword = await hashPassword(`${password}`)

        const user_ac =
            await this.knex<User>(tables.Users)
                .insert({
                    staff_id: data.account.staff_id,
                    password: hashedPassword,
                    company_id: data.account.company_id,
                    department_id: data.account.department,
                    admin: data.account.admin
                })
                .returning("id")

        return user_ac
    }
    async CreatePersonalRecord(data: any, id: number) {
        const newPersonalRecord =
            await this.knex<PersonnelRecords>(tables.PersonalData)
                .insert({
                    name: data.name,
                    phone: data.phone,
                    IDcard: data.IDcard,
                    date_of_birth: data.date_of_birth,
                    address: data.address,
                    email: data.email,
                    post: data.post,
                    salary: data.salary,
                    leave_day: data.leave_day,
                    entry_date: data.entry_date,
                    users_id: id
                })
                .returning("id")

        return newPersonalRecord
    }
    async getAllUserData(id: any) {
        const allUserData =
            await this.knex<User>(tables.Users)
                .join('department_list', 'users.department_id', '=', 'department_list.id')
                .join('personal_data', 'personal_data.users_id', '=', 'users.id')
                .select('*', 'users.staff_id')
                .where('department_id', id)
        return allUserData

    }
    async delUserPersonalData(user: any) {
        const delUserPersonalData =
            await this.knex<PersonnelRecords>(tables.PersonalData)
                .where('id', user.id)
                .del()
        return delUserPersonalData

    }
    async delUserData(user: any) {
        const delUserData =
            await this.knex<User>(tables.Users)
                .where('staff_id', user.staff_id)
                .del()
        return delUserData

    }
}