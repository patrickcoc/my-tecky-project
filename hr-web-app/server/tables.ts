const tables = Object.freeze({
    Users: "users",
    Company: "company_list",
    Department: "department_list",

    //
    PersonalData: "personal_data",

    // Record
    AttendanceRecord: "attendance_record",
    LeaveRecord: "leave_record",

    // Roster
    Roster: "roster",
    Salary_remark: "salary_remark"
})

export default tables